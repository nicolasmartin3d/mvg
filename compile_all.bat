@setlocal enableextensions enabledelayedexpansion
@cd /d "%~dp0"
@echo off

set "vs_version=12"
set "vs_name=Visual Studio 12"
REM set 'cmake_toolkit=-T "v110_xp"'
set 'cmake_toolkit='

set configs[1]=msvc_v!vs_version!_static_x64_noQt
set prefixs[1]=../../../Install/msvc_v!vs_version!/static/Release/x64
set "cmake_generator[1]=-G!vs_name! Win64"
set opts_shared[1]=OFF

set configs[2]=msvc_v!vs_version!_static_x86_noQt
set prefixs[2]=../../../Install/msvc_v!vs_version!/static/Release/x86
set "cmake_generator[2]=-G!vs_name!"
set opts_shared[2]=OFF

set configs[3]=msvc_v!vs_version!_shared_x64_noQt
set prefixs[3]=../../../Install/msvc_v!vs_version!/shared/Release/x64
set "cmake_generator[3]=-G!vs_name! Win64"
set opts_shared[3]=ON

set configs[4]=msvc_v!vs_version!_shared_x86_noQt
set prefixs[4]=../../../Install/msvc_v!vs_version!/shared/Release/x86
set "cmake_generator[4]=-G!vs_name!"
set opts_shared[4]=ON

md ..\..\Build\mvg
cd ..\..\Build\mvg

REM for /L %%i in (1,1,4) do (
for /L %%i in (1,1,4) do (
	set c="!configs[%%i]!"
	rd /s /q !c!
	md !c!
	cd !c!
		
	set p="!prefixs[%%i]!"
	cmake "!cmake_generator[%%i]!" "!cmake_toolkit!" -DCMAKE_INSTALL_PREFIX=!p! ^
	 -DOpenCV_DIR=!p! -DOpenCV_STATIC=ON ^
	 -DOPENTHREADS_DIR=!p! ^
	 -DCeres_DIR=!p!/CMake ^
	 -DXTCLAP_INCLUDE_DIR=!p!/include ^
	 -DBUILD_TEST=ON -DBUILD_APPLICATIONS=ON -DBUILD_SHARED_LIBS=!opts_shared[%%i]! ../../../Source/mvg
	 
	cmake --build . --target ALL_BUILD --config Release -- /maxcpucount
	cmake --build . --target INSTALL --config Release -- /maxcpucount	
	
	cd ..
)

pause
