#!/usr/bin/tclsh

set liste {}
set k [catch {exec git ls-files . | grep -e {\.cpp} -e {\.h} -e {CMakeLists} -e {\.in} -e {\.tpp} } liste]
#set k [catch {exec git diff-index HEAD --name-only} liste]
#if {$k} {
    #puts "Erreur : $liste"
    #exit 1
#}

set k [catch { set cin [open /dev/tty] } infos]
if {$k} { exit 0 }
set cmd "uncrustify -q -c [pwd]/scripts/uncrustify.cfg --no-backup -l CPP "

foreach f $liste {
    if {[string match "*lapack*" $f]} { continue; }

    set fd [open $f r]
    set data [read  $fd]
    close $fd
    set line [lindex [split $data "\n"] 0]

    regexp {^[^.]*\.(.*)} $f -> ext

    #check the extension first !
    switch $ext {
        "cpp" -
        "c" -
        "hpp" -
        "h" -
        "h.in" -
        "tpp" {
            if {![string equal [string index $line 0] "/"]} {
                puts "Fist line of $f is not a comment. Source files must begin with a comment (and copyright)."
                exit 1
            }
            #puts "Running uncrustify on $f"
            set k [catch { eval exec $cmd $f } infos]

            #uncrustify sometimes hang on a line .. must not stop the commit
            #if ${k} { puts "Error while running uncrustify : $infos" ; exit 1 }

            #if {[file exists ${f}.orig]} {
                #set fd [open "${f}.orig" r]
                #set new_data [read $fd]
                #close $fd

                #if {![string equal $data $new_data]} {
                    #puts "Style not validated ... Do you want to merge the change automatically ? \[y/N\]"
                    #set ans [gets $cin]
                    #if {[string equal $ans "y"]} {
                        #set fd [open $f w]
                        #puts $fd $new_data
                        #close $fd
                    #} else {
                        #puts "Changes are stored in file ${f}.orig, you can see the changes by using diff -u $f ${f}.orig"
                    #}
                #}
            #}
        }
    }
}


set k [catch {exec git --no-pager diff} diff]
if {$k} {
    puts "Could not find differences using git diff"
    exit 1
}

if {![string equal $diff ""]} {
    puts "At least one file has been automatically modified, check it using git difftool"
    exit 1
}

exit 0
