#
# MVG - A library to solve multiple view geometry problems.
# Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
#
# This file is part of MVG.
#
# MVG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# MVG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MVG.  If not, see <http://www.gnu.org/licenses/>.
#

#
# File inspired by Ceres Solver (http://code.google.com/p/ceres-solver)
#

CMAKE_MINIMUM_REQUIRED(VERSION 2.8.0)
CMAKE_POLICY(VERSION 2.8)

IF (POLICY CMP0003)
  CMAKE_POLICY(SET CMP0003 NEW)
ENDIF()
IF (POLICY CMP0042)
  CMAKE_POLICY(SET CMP0042 NEW)
ENDIF()

PROJECT(MVG C CXX)

# Make CMake aware of the cmake folder for local FindXXX scripts.
LIST (APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")

SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
SET(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
SET(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
# Set postfixes for generated libraries based on buildtype.
SET(CMAKE_RELEASE_POSTFIX "")
SET(CMAKE_DEBUG_POSTFIX "-debug")

# Building shared library by default
OPTION(BUILD_SHARED_LIBS         "Build shared library instead of static ones" ON)
OPTION(BUILD_TEST                "Build various test" OFF)
OPTION(BUILD_APPLICATIONS        "Build applications distributed with the library" ON)
OPTION(BUILD_DEV_WARNINGS        "Enable all extra warnings (for development only)" OFF)
IF (MSVC)
  OPTION(MSVC_USE_STATIC_CRT     "MS Visual Studio: Use static C-Run Time Library in place of shared." OFF)
ENDIF (MSVC)

SET(MVG_AUTHOR "Nicolas Martin")
SET(MVG_EMAIL "nicolas.martin.3d@gmail.com")
SET(MVG_YEARS "2012-2015")
SET(MVG_PROJECT "MVG")
SET(MVG_DESCRIPTION "A library to solve multiple view geometry problems")
SET(MVG_VERSION_MAJOR 1)
SET(MVG_VERSION_MINOR 1)
SET(MVG_VERSION_REV 0)
SET(MVG_VERSION ${MVG_VERSION_MAJOR}.${MVG_VERSION_MINOR}.${MVG_VERSION_REV})
MESSAGE("-- MVG VERSION ${MVG_VERSION}.")

#define OS variables
IF (WIN32)
    SET (OS_WIN 1)
ENDIF()
IF (UNIX)
    SET (OS_UNIX 1)
ENDIF()
IF (APPLE)
    SET (OS_APPLE 1)
ENDIF()

# Required dependencies
# make the XX-DIR variable passed on the command line useable by cmake in the module
# configured by replacing backward slashes to forward slashes ...
IF (OpenCV_DIR)
    FILE(TO_CMAKE_PATH "${OpenCV_DIR}" OpenCV_DIR)
ENDIF (OpenCV_DIR)
FIND_PACKAGE(OpenCV REQUIRED)
# Calling find_package twice with specific components since OpenCV Config
# module does not conform to CMake doc about OPTIONAL_COMPONENTS
IF("${OpenCV_VERSION_MAJOR}" STREQUAL "3")
    FIND_PACKAGE(OpenCV REQUIRED core imgproc imgcodecs)
ELSE("${OpenCV_VERSION_MAJOR}" STREQUAL "3")
    FIND_PACKAGE(OpenCV REQUIRED core imgproc highgui)
ENDIF("${OpenCV_VERSION_MAJOR}" STREQUAL "3")
INCLUDE_DIRECTORIES(SYSTEM ${OpenCV_INCLUDE_DIRS})

# Because OpenCV does not explicitly call them even thought it links with its imported target ...
FIND_PACKAGE(Qt5Core QUIET)
FIND_PACKAGE(Qt5Gui QUIET)
FIND_PACKAGE(Qt5Widgets QUIET)
FIND_PACKAGE(Qt5Concurrent QUIET)
FIND_PACKAGE(Qt5Test QUIET)

# make the XX-DIR variable passed on the command line useable by cmake in the module
# configured by replacing backward slashes to forward slashes ...
IF (Ceres_DIR)
    FILE(TO_CMAKE_PATH "${Ceres_DIR}" Ceres_DIR)
ENDIF (Ceres_DIR)
FIND_PACKAGE(Ceres REQUIRED)
INCLUDE_DIRECTORIES(SYSTEM ${CERES_INCLUDE_DIRS})

# make the XX-DIR variable passed on the command line useable by cmake in the module
# configured by replacing backward slashes to forward slashes ...
IF (OPENTHREADS_DIR)
    FILE(TO_CMAKE_PATH "${OPENTHREADS_DIR}" OPENTHREADS_DIR)
ENDIF (OPENTHREADS_DIR)
IF (OPENTHREADS_DIR)
    FIND_PACKAGE(OpenThreads)
ENDIF (OPENTHREADS_DIR)
# On Linux, OpenThreads is not found if installed in lib64..
# Check with /usr/local/lib64
IF (NOT OPENTHREADS_FOUND)
    IF(EXISTS /usr/local/lib64)
        SET(OPENTHREADS_DIR /usr/local/lib64)
        FIND_PACKAGE(OpenThreads)
    ENDIF()
ENDIF()
# Check with /usr/lib64
IF (NOT OPENTHREADS_FOUND)
  IF(EXISTS /usr/lib64)
    SET(OPENTHREADS_DIR /usr/lib64)
    FIND_PACKAGE(OpenThreads)
  ENDIF()
ENDIF()
# Check with standard paths
IF (NOT OPENTHREADS_FOUND)
    FIND_PACKAGE(OpenThreads REQUIRED)
ENDIF()
INCLUDE_DIRECTORIES(SYSTEM ${OPENTHREADS_INCLUDE_DIR})


#xTCLAP: soon to be a module oriented cmake file
FIND_PATH(XTCLAP_INCLUDE_DIR
    NAMES xtclap/BoundConstraint.h)
IF (NOT XTCLAP_INCLUDE_DIR)
  MESSAGE("-- Did not find dependency Xtclap.")
ELSE (NOT XTCLAP_INCLUDE_DIR)
  MESSAGE("-- Found MVG dependency Xtclap in ${XTCLAP_INCLUDE_DIR}")
ENDIF (NOT XTCLAP_INCLUDE_DIR)
IF (XTCLAP_INCLUDE_DIR)
    INCLUDE_DIRECTORIES(SYSTEM ${XTCLAP_INCLUDE_DIR})
ELSE (XTCLAP_INCLUDE_DIR)
    SET (BUILD_APPLICATIONS OFF)
    SET (BUILD_TEST OFF)
ENDIF (XTCLAP_INCLUDE_DIR)

IF (BUILD_SHARED_LIBS)
  MESSAGE("-- Building MVG as a shared library.")
  SET (MVG_SHARED_LIBRARY TRUE)
ELSE (BUILD_SHARED_LIBS)
  MESSAGE("-- Building MVG as a static library.")
  SET (MVG_STATIC_LIBRARY TRUE)
ENDIF (BUILD_SHARED_LIBS)
ADD_DEFINITIONS(-DMVG_BUILDING)

# Change the default build type from Debug to Release, while still
# supporting overriding the build type.
#
# The CACHE STRING logic here and elsewhere is needed to force CMake
# to pay attention to the value of these variables.
IF (NOT CMAKE_BUILD_TYPE)
  SET(CMAKE_BUILD_TYPE Release CACHE STRING
    "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel."
    FORCE)
ELSE (NOT CMAKE_BUILD_TYPE)
  IF (CMAKE_BUILD_TYPE STREQUAL "Debug")
    MESSAGE("\n=================================================================================")
    MESSAGE("\n-- Build type: Debug. Performance will be terrible!")
    MESSAGE("-- Add -DCMAKE_BUILD_TYPE=Release to the CMake command line to get an optimized build.")
    MESSAGE("\n=================================================================================")
  ENDIF (CMAKE_BUILD_TYPE STREQUAL "Debug")
ENDIF (NOT CMAKE_BUILD_TYPE)

# Set the default MVG flags to an empty string.
SET (MVG_CXX_FLAGS)

IF (CMAKE_BUILD_TYPE STREQUAL "Release")
  IF (CMAKE_COMPILER_IS_GNUCXX)
    # Linux
    IF (CMAKE_SYSTEM_NAME MATCHES "Linux")
      IF (NOT GCC_VERSION VERSION_LESS 4.2)
        SET (MVG_CXX_FLAGS "${MVG_CXX_FLAGS} -march=native -mtune=native")
      ENDIF (NOT GCC_VERSION VERSION_LESS 4.2)
    ENDIF (CMAKE_SYSTEM_NAME MATCHES "Linux")
    # Mac OS X
    IF (CMAKE_SYSTEM_NAME MATCHES "Darwin")
      SET (MVG_CXX_FLAGS "${MVG_CXX_FLAGS} -msse3")
      # Use of -fast only applicable for Apple's GCC
      # Assume this is being used if GCC version < 4.3 on OSX
      EXECUTE_PROCESS(COMMAND ${CMAKE_CXX_COMPILER} ${CMAKE_CXX_COMPILER_ARG1} -dumpversion
        OUTPUT_VARIABLE GCC_VERSION
        OUTPUT_STRIP_TRAILING_WHITESPACE)
      IF (GCC_VERSION VERSION_LESS 4.3)
        SET (MVG_CXX_FLAGS "${MVG_CXX_FLAGS} -fast")
      ENDIF (GCC_VERSION VERSION_LESS 4.3)
    ENDIF (CMAKE_SYSTEM_NAME MATCHES "Darwin")
  ENDIF (CMAKE_COMPILER_IS_GNUCXX)
  IF (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    # Use of -O4 requires use of gold linker & LLVM-gold plugin, which might
    # well not be present / in use and without which files will compile, but
    # not link ('file not recognized') so explicitly check for support
    INCLUDE(CheckCXXCompilerFlag)
    CHECK_CXX_COMPILER_FLAG("-flto" HAVE_LTO_SUPPORT)
    IF (HAVE_LTO_SUPPORT)
      MESSAGE(STATUS "Enabling link-time optimization (-flto)")
      SET(MVG_CXX_FLAGS "${MVG_CXX_FLAGS} -flto")
    ELSE ()
      MESSAGE(STATUS "Compiler/linker does not support link-time optimization (-O4), disabling.")
    ENDIF (HAVE_LTO_SUPPORT)
  ENDIF ()
ENDIF (CMAKE_BUILD_TYPE STREQUAL "Release")

SET (CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} ${MVG_CXX_FLAGS}")

#stricter warnings
IF (UNIX)
    IF (BUILD_DEV_WARNINGS)
          IF (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
            SET(CMAKE_CXX_FLAGS
            "${CMAKE_CXX_FLAGS} -Werror -Weverything -Wno-exit-time-destructors -Wno-variadic-macros \
            -Wno-padded -Wno-c++11-long-long -Wno-padded -Wno-cast-align -Wno-unreachable-code-return \
            -Wno-gnu-zero-variadic-macro-arguments -Wno-missing-prototypes")
          ELSE (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
            SET(CMAKE_CXX_FLAGS
            "${CMAKE_CXX_FLAGS} -Werror -Wall -Wextra -Wno-pragmas -Wno-unknown-pragmas")
          ENDIF (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    ELSE (BUILD_DEV_WARNINGS)
          IF (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
            SET(CMAKE_CXX_FLAGS
            "${CMAKE_CXX_FLAGS} -Werror -Wall")
          ELSE (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
            SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror -Wall -Wno-pragmas")
          ENDIF (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    ENDIF (BUILD_DEV_WARNINGS)
ENDIF (UNIX)

#disable some pesky warnings under MSVC ..
IF (MSVC)
    # Disable warning about the insecurity of using "std::copy".
    ADD_DEFINITIONS("/wd4996")
    # Disable warning about non exported symbols while using STL
    # ADD_DEFINITIONS("/wd4251")
    # Enables use of exception handling
    ADD_DEFINITIONS("/EHsc")
	
	# From Ceres CMakeLists.txt
	# Update the C/CXX flags for MSVC to use either the static or shared
	# C-Run Time (CRT) library based on the user option: MSVC_USE_STATIC_CRT.
	LIST(APPEND C_CXX_FLAGS
		CMAKE_CXX_FLAGS
		CMAKE_CXX_FLAGS_DEBUG
		CMAKE_CXX_FLAGS_RELEASE
		CMAKE_CXX_FLAGS_MINSIZEREL
		CMAKE_CXX_FLAGS_RELWITHDEBINFO)

	FOREACH(FLAG_VAR ${C_CXX_FLAGS})
		IF (MSVC_USE_STATIC_CRT)
		  # Use static CRT.
		  IF (${FLAG_VAR} MATCHES "/MD")
			STRING(REGEX REPLACE "/MD" "/MT" ${FLAG_VAR} "${${FLAG_VAR}}")
		  ENDIF (${FLAG_VAR} MATCHES "/MD")
		ELSE (MSVC_USE_STATIC_CRT)
		  # Use shared, not static, CRT.
		  IF (${FLAG_VAR} MATCHES "/MT")
			STRING(REGEX REPLACE "/MT" "/MD" ${FLAG_VAR} "${${FLAG_VAR}}")
		  ENDIF (${FLAG_VAR} MATCHES "/MT")
		ENDIF (MSVC_USE_STATIC_CRT)
	ENDFOREACH(FLAG_VAR ${C_CXX_FLAGS})	
	
ENDIF (MSVC)

# config.h
CONFIGURE_FILE(
    "src/config.h.in"
    "${CMAKE_CURRENT_BINARY_DIR}/src/config.h")
INCLUDE_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR}/src)


INCLUDE(TestBigEndian)
TEST_BIG_ENDIAN(BIGENDIAN)
IF(${BIGENDIAN})
     ADD_DEFINITIONS(-DBIGENDIAN)
ELSE(${BIGENDIAN})
     ADD_DEFINITIONS(-DLITTLEENDIAN)
ENDIF(${BIGENDIAN})

ADD_SUBDIRECTORY(src)
# No warnings in CLAPACK code, this is not ours to maintain
IF (UNIX)
    FOREACH (file ${LAPACK_SOURCE_FILES})
        SET_PROPERTY(SOURCE ${file} APPEND PROPERTY COMPILE_FLAGS "-w")
    ENDFOREACH (file ${LAPACK_SOURCE_FILES})
ENDIF (UNIX)
# library generation
ADD_LIBRARY(mvg ${MVG_SOURCE_FILES})
TARGET_LINK_LIBRARIES(mvg ${OpenCV_LIBS} ${CERES_LIBRARIES} ${OPENTHREADS_LIBRARY})

# install target
INSTALL(FILES ${MVG_HEADER_FILES} DESTINATION include/mvg)
INSTALL(FILES "${CMAKE_CURRENT_BINARY_DIR}/src/config.h" DESTINATION "include/mvg")
INSTALL(TARGETS mvg
    EXPORT MVGExport
    RUNTIME DESTINATION bin
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib)


# Add an uninstall target to remove all installed files.
CONFIGURE_FILE("${CMAKE_SOURCE_DIR}/cmake/uninstall.cmake.in"
               "${CMAKE_BINARY_DIR}/cmake/uninstall.cmake"
               IMMEDIATE @ONLY)

ADD_CUSTOM_TARGET(uninstall
                  COMMAND ${CMAKE_COMMAND} -P ${CMAKE_BINARY_DIR}/cmake/uninstall.cmake)

# Set relative install paths, which are appended to CMAKE_INSTALL_PREFIX to
# generate the absolute install paths.
IF (WIN32)
  SET(RELATIVE_CMAKECONFIG_INSTALL_DIR CMake)
ELSE ()
  SET(RELATIVE_CMAKECONFIG_INSTALL_DIR share/mvg)
ENDIF ()

# This "exports" all targets which have been put into the export set
# "MVGExport". This means that CMake generates a file with the given
# filename, which can later on be loaded by projects using this package.
# This file contains ADD_LIBRARY(bar IMPORTED) statements for each target
# in the export set, so when loaded later on CMake will create "imported"
# library targets from these, which can be used in many ways in the same way
# as a normal library target created via a normal ADD_LIBRARY().
INSTALL(EXPORT MVGExport
    DESTINATION ${RELATIVE_CMAKECONFIG_INSTALL_DIR} FILE MVGTargets.cmake)

# Figure out the relative path from the installed Config.cmake file to the
# install prefix (which may be at runtime different from the chosen
# CMAKE_INSTALL_PREFIX if under Windows the package was installed anywhere)
# This relative path will be configured into the MVGConfig.cmake.
FILE(RELATIVE_PATH INSTALL_ROOT_REL_CONFIG_INSTALL_DIR
     ${CMAKE_INSTALL_PREFIX}/${RELATIVE_CMAKECONFIG_INSTALL_DIR}
     ${CMAKE_INSTALL_PREFIX})

# Create a MVGConfig.cmake file. <name>Config.cmake files are searched by
# FIND_PACKAGE() automatically. We configure that file so that we can put any
# information we want in it, e.g. version numbers, include directories, etc.
CONFIGURE_FILE("${CMAKE_SOURCE_DIR}/cmake/MVGConfig.cmake.in"
               "${CMAKE_CURRENT_BINARY_DIR}/MVGConfig.cmake" @ONLY)

# Additionally, when CMake has found a MVGConfig.cmake, it can check for a
# MVGConfigVersion.cmake in the same directory when figuring out the version
# of the package when a version has been specified in the FIND_PACKAGE() call,
# e.g. FIND_PACKAGE(MVG [1.4.2] REQUIRED). The version argument is optional.
CONFIGURE_FILE("${CMAKE_SOURCE_DIR}/cmake/MVGConfigVersion.cmake.in"
               "${CMAKE_CURRENT_BINARY_DIR}/MVGConfigVersion.cmake" @ONLY)

# Install these files into the same directory as the generated exports-file,
# we include the FindPackage scripts for libraries whose headers are included
# in the public API of MVG and should thus be present in MVG_INCLUDE_DIRS.
INSTALL(FILES "${CMAKE_CURRENT_BINARY_DIR}/MVGConfig.cmake"
              "${CMAKE_CURRENT_BINARY_DIR}/MVGConfigVersion.cmake"
        DESTINATION ${RELATIVE_CMAKECONFIG_INSTALL_DIR})

IF(BUILD_APPLICATIONS)
  # Compiling the applications that comes with it
  ADD_SUBDIRECTORY(applications)
ENDIF()

IF(BUILD_TEST)
  ENABLE_TESTING()
  ADD_SUBDIRECTORY(test)
ENDIF()
