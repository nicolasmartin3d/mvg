/*
 * License Agreement for MVG
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <opencv2/core/core.hpp>
#if CV_MAJOR_VERSION >= 3
#include <opencv2/core/utility.hpp>
#endif
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <fstream>
#include "../src/mvg.h"
#include "../src/recon.h"

using namespace cv;
using namespace std;

template <size_t n>
bool readPointFile(const string &path,
                   vector< Vec<double, n> > &pts,
                   vector<int> &ids)
{
    ifstream ifs(path.c_str());
    if (!ifs.is_open())
    {
        return false;
    }
    string str;
    while (getline(ifs, str))
    {
        int            id;
        Vec<double, n> vec;
        istringstream  iss(str);
        if (!(iss >> id)) { return false; }
        for (int i = 0; i < n; ++i)
        {
            if (!(iss >> vec[i])) { return false; }
        }
        pts.push_back(vec);
        ids.push_back(id);
    }

    return true;
}

void usage()
{
    cout <<
        "USAGE:\n\ttest_triangulate [-p pts2d1.txt pts2d2.txt pts3d.txt camConfig projConfig] [-m map.png camConfig projConfig [mask]]"
         << endl;
    exit(1);
}

int main(int   argc,
         char *argv[])
{
    if (argc < 2) { usage(); }

    vector<Mat>    imagePoints(2);
    vector<Camera> cameras(2);
    string         mode(argv[1]);

    string camConfigPath, projConfigPath;
    string maskPath;
    if (mode == "-p")
    {
        if (argc != 7) { usage(); }

        camConfigPath  = argv[5];
        projConfigPath = argv[6];
    }
    else if (mode == "-m")
    {
        if ((argc < 5) || (argc > 6)) { usage(); }

        camConfigPath  = argv[3];
        projConfigPath = argv[4];
        if (argc == 6) { maskPath = argv[5]; }
    }
    else
    {
        usage();
    }

    cameras[0].read(camConfigPath);
    cameras[1].read(projConfigPath);

    if (mode == "-p")
    {
        string pts3dPath, pts2d1Path, pts2d2Path;
        pts2d1Path = argv[2];
        pts2d2Path = argv[3];
        pts3dPath  = argv[4];

        vector<Vec3d> pts3d;
        vector<int>   ids3d;
        if (!readPointFile<3>(pts3dPath, pts3d, ids3d))
        {
            cerr << "Impossible de lire le fichier " << pts3dPath << endl;

            return 1;
        }

        vector<Vec2d> pts2d1, pts2d2;
        vector<int>   ids2d1, ids2d2;
        if (!readPointFile<2>(pts2d1Path, pts2d1, ids2d1))
        {
            cerr << "Impossible de lire le fichier " << pts2d1Path << endl;

            return 1;
        }
        if (!readPointFile<2>(pts2d2Path, pts2d2, ids2d2))
        {
            cerr << "Impossible de lire le fichier " << pts2d2Path << endl;

            return 1;
        }

        vector<int> ids(max(ids2d1.size(), ids2d2.size()));
        vector<int> tmp1, tmp2;
        tmp1 = ids2d1; tmp2 = ids2d2;
        sort(tmp1.begin(), tmp1.end());
        sort(tmp2.begin(), tmp2.end());
        vector<int>::iterator it =
            set_intersection(tmp1.begin(), tmp1.end(),
                             tmp2.begin(), tmp2.end(),
                             ids.begin());
        ids.resize(it - ids.begin());

        size_t nbPts = ids.size();
        imagePoints[0] = Mat(static_cast<int>(nbPts), 1, CV_64FC2);
        imagePoints[1] = Mat(static_cast<int>(nbPts), 1, CV_64FC2);

        for (size_t i = 0; i < ids2d1.size(); ++i)
        {
            vector<int>::iterator it = lower_bound(ids.begin(), ids.end(), ids2d1[i]);
            if ((it != ids.end()) && (*it == ids2d1[i]))
            {
                int pos = static_cast<int>(it - ids.begin());
                imagePoints[0].at<Point2d>(pos, 0) = Point2d(pts2d1[i][0], pts2d1[i][1]);
            }
        }

        for (size_t i = 0; i < ids2d2.size(); ++i)
        {
            vector<int>::iterator it = lower_bound(ids.begin(), ids.end(), ids2d2[i]);
            if ((it != ids.end()) && (*it == ids2d2[i]))
            {
                int pos = static_cast<int>(it - ids.begin());
                imagePoints[1].at<Point2d>(pos, 0) = Point2d(pts2d2[i][0], pts2d2[i][1]);
            }
        }

        Mat epts3d;
        cout << "========================================" << endl;
        cout << "========== triangulating matches ========" << endl;
        double time = static_cast<double>(cv::getTickCount());
        triangulatePoints(imagePoints[0], imagePoints[1], cameras[0], cameras[1], epts3d);
        time = 1000 * ((double)cv::getTickCount() - time) / cv::getTickFrequency();
        cout << "Took " << time << " ms" << endl;

        {
            MatConstIterator_<Point3d> it = epts3d.begin<Point3d>();
            for (int i = 0; i < epts3d.rows; ++i, ++it)
            {
                if (norm(Mat(pts3d[i]), Mat(*it)) > 0.1)
                {
                    cerr << i << " " << *it << " -> " << pts3d[i][0]
                         << " " << pts3d[i][1] << " " << pts3d[i][2] << " "
                         << norm(Mat(pts3d[i]), Mat(*it)) << endl;

                    return 1;
                }
            }
        }
    }
    else if (string(argv[1]) == "-m")
    {
        string mapPath;
        mapPath = argv[2];

        Mat map;
        map = imread(mapPath, -1);
        if (map.empty())
        {
            cout << "Impossible de lire l'image " << mapPath << endl;

            return 1;
        }

        /*
         *   Size cam1Size = cameras[0].imageSize;
         *   Size cam2Size = cameras[1].imageSize;
         *
         *   for (int yc=0; yc<cam1Size.height; ++yc) {
         *    const Vec3w *matchPtr = map.ptr<Vec3w>(yc);
         *    for (int xc=0; xc<cam1Size.width; ++xc)  {
         *        //trouve la correpondance dans le proj
         *        int xv = (matchPtr[xc])[2], yv = (matchPtr[xc])[1];
         *        double xp = static_cast<double>(shortToPos(xv, cam2Size.width));
         *        double yp = static_cast<double>(shortToPos(yv, cam2Size.height));
         *
         *        imagePoints[0].push_back(Point2d(xc+0.5, yc+0.5));
         *        imagePoints[1].push_back(Point2d(xp+0.5, yp+0.5));
         *    }
         *   }
         *
         *   {
         *    Mat pts1, pts2;
         *    matchMapToPoints(map, cam1Size, cam2Size, pts1, pts2);
         *    {
         *        const Point2d *p1 = imagePoints[1].ptr<Point2d>();
         *        const Point2d *p2 = pts2.ptr<Point2d>();
         *        for (int yc=0; yc<cam1Size.height*cam1Size.width; ++yc) {
         *            if (std::abs(p1[yc].x-p2[yc].x > 0.01 ||
         *                         std::abs(p1[yc].y-p2[yc].y > 0.01)))
         *            {
         *                cout << "====== " << yc << endl;
         *                cout << p1[yc].x << " " << p2[yc].x << endl;
         *                cout << p1[yc].y << " " << p2[yc].y << endl;
         *            }
         *        }
         *    }
         *    {
         *        const Point2d *p1 = imagePoints[0].ptr<Point2d>();
         *        const Point2d *p2 = pts1.ptr<Point2d>();
         *        for (int yc=0; yc<cam1Size.height*cam1Size.width; ++yc) {
         *            if (std::abs(p1[yc].x-p2[yc].x > 0.01 ||
         *                         std::abs(p1[yc].y-p2[yc].y > 0.01)))
         *            {
         *                cout << "====== " << yc << endl;
         *                cout << p1[yc].x << " " << p2[yc].x << endl;
         *                cout << p1[yc].y << " " << p2[yc].y << endl;
         *            }
         *        }
         *    }
         *   }
         */

        Mat       mask = imread(maskPath, -1);
        Ptr<Mesh> mesh = reconstructMesh(cameras[0], cameras[1], map, Mat(), mask);
        mesh->computeNormals();
        mesh->clip(-numeric_limits<double>::max(), -numeric_limits<double>::max(), 0,
                   numeric_limits<double>::max(), numeric_limits<double>::max(), 20);
        mesh->write("test.ply");

        Mesh tmp;
        tmp.read("test.ply");

        if (mesh->vertices.size() != tmp.vertices.size())
        {
            cerr << "# vertices differ " << mesh->vertices.size() << " vs "
                 <<  tmp.vertices.size() << endl;

            return 1;
        }
        if (mesh->triangles.size() != tmp.triangles.size())
        {
            cerr << "# faces differ " << mesh->triangles.size() << " vs "
                 << tmp.triangles.size() << endl;

            return 1;
        }
        for (size_t i = 0; i < mesh->vertices.size(); ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                if (std::abs<double>(mesh->vertices[i][j] - tmp.vertices[i][j]) > 0.5)
                {
                    cerr << i << " " << j << " " << mesh->vertices[i][j] << " "
                         << tmp.vertices[i][j] << endl;

                    return 1;
                }
            }
        }
        for (size_t i = 0; i < mesh->triangles.size(); ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                if (std::abs<int>(mesh->triangles[i][j] - tmp.triangles[i][j]) > 0.5)
                {
                    cerr << i << " " << j << " " << mesh->triangles[i][j] << " "
                         << tmp.triangles[i][j] << endl;

                    return 1;
                }
            }
        }
    }

    return 0;
}
