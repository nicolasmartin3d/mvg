/*
 * License Agreement for MVG
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <xtclap/CmdLine.h>

#include "../src/fundam.h"
#include "../src/mvg.h"

using namespace cv;
using namespace std;
using namespace TCLAP;

bool wellDistributed(const Mat &pts,
                     Size       size,
                     int        steps)
{
    RNG         rng;
    vector<int> histo(steps * steps, 0);
    int         count = pts.rows * pts.cols;

    double         xstep = size.width / static_cast<double>(steps);
    double         ystep = size.height / static_cast<double>(steps);
    const Point2d *p     = pts.ptr<Point2d>();
    for (int i = 0; i < count; ++i)
    {
        int x = static_cast<int>(p[i].x / xstep);
        int y = static_cast<int>(p[i].y / ystep);
        histo[y * steps + x]++;
    }

    /*//s'assure que pas plus de 20% des cases n'est vide
     *   int nbVide = 0;
     *   for (int i=0; i<steps*steps; ++i)
     *    if (histo[i] == 0) nbVide++;
     *   if (nbVide > steps*steps*0.2) return false;
     *
     *   cout << "nbVide = " << nbVide <<  " " << steps*steps*0.2 << endl;*/

    int tot = 0;
    for (int i = 0; i < steps * steps; ++i)
    {
        tot += histo[i];
    }
    assert(tot == count);

    // choisis 50% des cases au hasard et vérifie qu'elles ne sont pas vides
    /*vector<int> pos(steps*steps);
     *   for (int i=0; i<steps*steps; ++i) pos[i] = i;
     *   for (int i=0; i<steps*steps*2; ++i) {
     *    int a = random()%(steps*steps);
     *    int b = random()%(steps*steps);
     *    swap(pos[a], pos[b]);
     *   }
     *
     *   int nbPos = static_cast<int>(0.5*steps*steps);
     *   for (int i=0; i<nbPos; ++i) {
     *    cout << pos[i] << " " << histo[pos[i]] << endl;
     *    if (histo[pos[i]] == 0) return false;
     *   }*/

    int nbPos = static_cast<int>(0.5 * steps * steps);
    for (int i = 0; i < nbPos; ++i)
    {
        int pos = rng() % (steps * steps);
        if (histo[pos] == 0) { return false; }
    }

    // cerr << ">>>OUI" << endl;

    // s'assure qu'aucune paire de points n'est à une distance de 1 pixel
    for (int i = 0; i < count; ++i)
    {
        for (int j = 0; j < i; ++j)
        {
            double dx = p[i].x - p[j].x;
            double dy = p[i].y - p[j].y;
            cout << fabs(dx) << " " << fabs(dy) << endl;
            if ((fabs(dx) < 10) && (fabs(dy) < 10)) { return false; }
        }
    }

    cerr << "YO!" << endl;

    return true;

#if 0
    nth_element(histo.begin(), histo.begin() + count / 2, histo.end());
    // pas exactement la vraie médiane, mais ça fera ..
    double ideal = count / static_cast<double>(steps * steps);

    cout << "ideal is " << ideal << endl;
    cout << "median is " << histo[count / 2] << endl;
    cout << "distrib is " << endl;
    for (int i = 0; i < steps * steps; ++i)
    {
        cout << histo[i] << " ";
    }
    cout << endl;

    return fabs(histo[count / 2] - ideal) < 0.1 * count;

#endif
}

bool flipCoin(RNG &rng) { return rng.uniform(0, 2) == 0; }

void add_noise(double noiseStdDev,
               RNG   &rng,
               Mat   &p1s,
               Mat   &p2s)
{
    if (noiseStdDev < numeric_limits<double>::epsilon())
    {
        return;
    }

    Mat n1 = p1s.clone(), n2 = p2s.clone();
    randn(n1, Scalar::all(0.), Scalar::all(noiseStdDev));
    randn(n2, Scalar::all(0.), Scalar::all(noiseStdDev));

    p1s += n1;
    p2s += n2;
}

void add_outliers(vector<bool> &outls,
                  int           nOutls,
                  double        threshold,
                  RNG          &rng,
                  const Mat    &F,
                  Mat          &p1s,
                  Mat          &p2s)
{
    double *_p1s = p1s.ptr<double>(), *_p2s = p2s.ptr<double>();
    // const double *_F = F.ptr<double>();
    const Mat tF = F.t();
    // const double *_tF = tF.ptr<double>();
    size_t nPoints = outls.size();
    // threshold ^2 *8 to be sure !
    const double outlThreshold = threshold * threshold * 12;

    const double minOutlDistance = 20.0, maxOutlDistance = 40.0;
    fill(outls.begin(), outls.end(), false);
    for (int j = 0; j < nOutls;)
    {
        int id = rng.uniform(0, static_cast<int>(nPoints));
        if (outls[id]) { continue; }

        bool outlier = false;
        for (int k = 0; k < 10; ++k)
        {
            double x1, y1, x2, y2;
            do
            {
                x1 = _p1s[id * 2 + 0]; y1 = _p1s[id * 2 + 1];
                x2 = _p2s[id * 2 + 0]; y2 = _p2s[id * 2 + 1];
                if (flipCoin(rng))
                {
                    x1 += rng.uniform(minOutlDistance, maxOutlDistance) *
                          (rng.uniform(0, 2) * 2 - 1);
                }
                if (flipCoin(rng))
                {
                    x2 += rng.uniform(minOutlDistance, maxOutlDistance) *
                          (rng.uniform(0, 2) * 2 - 1);
                }
                if (flipCoin(rng))
                {
                    y1 += rng.uniform(minOutlDistance, maxOutlDistance) *
                          (rng.uniform(0, 2) * 2 - 1);
                }
                if (flipCoin(rng))
                {
                    y2 += rng.uniform(minOutlDistance, maxOutlDistance) *
                          (rng.uniform(0, 2) * 2 - 1);
                }
            }
            while (x1 < 0 || x1 > 1000 || y1 < 0 || y1 > 1000 ||
                   x2 < 0 || x2 > 1000 || y2 < 0 || y2 > 1000);

            Point2d p1(x1, y1), p2(x2, y2);
            if (max(squaredDistanceToEpipolarLine(tF, p2, p1),
                    squaredDistanceToEpipolarLine(F, p1, p2))
                > outlThreshold)
            {
                _p1s[id * 2 + 0] = x1; _p1s[id * 2 + 1] = y1;
                _p2s[id * 2 + 0] = x2; _p2s[id * 2 + 1] = y2;
                outlier          = true;
                break;
            }
        }
        if (!outlier) { continue; }

        outls[id] = true;
        ++j;
    }
}

bool runSceneTest(int    nIters,
                  int    method,
                  int    nPoints,
                  int    nOutls,
                  double noiseStdDev,
                  double threshold)
{
    RNG rng(0x12345678);

    double _K[9];
    Mat    K(3, 3, CV_64F, _K), iK, itK;

    double _v[3], _c[3], _F[9];
    Mat    v(3, 1, CV_64F, _v);
    Mat    c(3, 1, CV_64F, _c), t, tX;
    Mat    R, tR;
    Mat    M1(3, 4, CV_64F), M2(3, 4, CV_64F),
    M2s3x3 = M2(Range::all(), Range(0, 3)),
    M2s1x3 = M2(Range::all(), Range(3, 4));
    Mat iM1 = Mat::eye(4, 4, CV_64F), iM1s = iM1(Range(0, 3), Range(0, 3));
    Mat F   = Mat(3, 3, CV_64F, _F), eF;

    Size           camSize(1000, 1000);
    vector<double> _p3ds(3 * nPoints), _p1s(2 * nPoints), _p2s(2 * nPoints);
    Mat            p3ds = Mat(nPoints, 3, CV_64F, _p3ds.data()),
                   p1s  = Mat(nPoints, 1, CV_64FC2, _p1s.data()),
                   p2s  = Mat(nPoints, 1, CV_64FC2, _p2s.data());
    vector<bool> outls(nPoints), mask;
    Mat          meanPts, stddevPts;
    Mat          op1s, op2s;

    for (int i = 0; i < nIters;)
    {
        double fx = rng.uniform(1500, 2500),
               ar = rng.uniform(0.95, 1.05),
               cx = rng.uniform(1000 / 2. - 75, 1000 / 2. + 75),
               cy = rng.uniform(1000 / 2. - 75, 1000 / 2. + 75);

        K             = Mat::eye(3, 3, CV_64F);
        _K[0 * 3 + 0] = fx;
        _K[1 * 3 + 1] = fx * ar;
        _K[0 * 3 + 2] = cx;
        _K[1 * 3 + 2] = cy;

        iK  = K.inv();
        itK = iK.t();

        // generate the scene
        M1 = K * Mat::eye(3, 4, CV_64F);
        iK.copyTo(iM1s);

        R                    = Mat::eye(3, 3, CV_64F);
        _v[0]                = _v[1] = 0; _v[2] = rng.uniform(-CV_PI / 8., CV_PI / 8.);
        Rodrigues(v, tR); R *= tR;
        _v[0]                = _v[2] = 0; _v[1] = rng.uniform(-CV_PI / 8., CV_PI / 8.);
        Rodrigues(v, tR); R *= tR;
        _v[1]                = _v[2] = 0; _v[0] = rng.uniform(-CV_PI / 8., CV_PI / 8.);
        Rodrigues(v, tR); R *= tR;

        randu(c, Scalar::all(-30), Scalar::all(30));
        t = R * -c;

        R.copyTo(M2s3x3);
        t.copyTo(M2s1x3);
        M2 = K * M2;

        // generate the 3D points by checking if the reprojection are inside the
        // image
        double _p3d[4], _p1[4], _p2[3];
        Mat    p3d(4, 1, CV_64F, _p3d),
        p1(4, 1, CV_64F, _p1),
        p2(3, 1, CV_64F, _p2);
        bool found = false;

        for (int iters = 0, j = 0; j < nPoints && iters < nPoints * 2; ++iters)
        {
            const int maxDispIters = 10;

            _p1[0] = rng.uniform(0., 1000.);
            _p1[1] = rng.uniform(0., 1000.);
            _p1[2] = 1;

            int d;
            for (d = 0; d < maxDispIters; ++d)
            {
                _p1[3] = 1. / rng.uniform(30., 80.);

                p3d = iM1 * p1; p3d /= _p3d[3];

                p2 = M2 * p3d; p2 /= _p2[2];

                if ((_p2[0] >= 0) && (_p2[0] <= 1000) &&
                    (_p2[1] >= 0) && (_p2[1] <= 1000))
                {
                    break;
                }
            }

            if (d < maxDispIters)
            {
                for (int k = 0; k < 3; ++k)
                {
                    _p3ds[j * 3 + k] = _p3d[k];
                }
                for (int k = 0; k < 2; ++k)
                {
                    _p1s[j * 2 + k] = _p1[k];
                    _p2s[j * 2 + k] = _p2[k];
                }
                ++j;
            }

            if (j == nPoints) { found = true; }
        }

        if (!found) { continue; }

        // ignore degenerate cases (points too close to each other)
        meanStdDev(p1s, meanPts, stddevPts);
        if ((stddevPts.at<double>(0, 0) < 200.) ||
            (stddevPts.at<double>(0, 1) < 200.))
        {
            continue;
        }
        meanStdDev(p2s, meanPts, stddevPts);
        if ((stddevPts.at<double>(0, 0) < 200.) ||
            (stddevPts.at<double>(0, 1) < 200.))
        {
            continue;
        }

        // find the real F = K^-T . [t]_X . R . K^-1
        skewMatrix(t, tX);
        F  = itK * tX * R * iK;
        F /= F.at<double>(2, 2);

        add_noise(noiseStdDev, rng, p1s, p2s);

        add_outliers(outls, nOutls, threshold, rng, F, p1s, p2s);

        int ret = findFundamentalMatrix(p1s, p2s, eF, &mask,
                                        method, threshold, 0.99, camSize, camSize);
        if (((nPoints == 7) && (ret != 1) && (ret != 3)) ||
            ((nPoints > 7) && (ret != 1)))
        {
            cerr << "\t"
                 << (nPoints == 7
                ? "# of solutions is not 1 nor 3 : " : "# of solution is not 1 : ")
                 << ret << endl;

            return false;
        }

        /*
         *   cout << F << endl;
         *   cout << eF << endl;
         *   getchar();
         */

        found = false;
        for (int j = 0; j < ret; ++j)
        {
            Mat eFi = eF.rowRange(j * 3, (j + 1) * 3);
            if (abs(eFi.at<double>(2, 2)) < numeric_limits<double>::epsilon())
            {
                continue;
            }

            eFi /= eFi.at<double>(2, 2);

            if (noiseStdDev < numeric_limits<double>::epsilon())
            {
                if (norm(F, eFi) > 10E-6)
                {
                    continue;
                }
            }
            else
            {
                /*
                 * we should use the true distance to decide if the solution is
                 *    good enough, but just looking at the classification of
                 *    outliers gives us an hint. if all points are well classified
                 *    then we get the best LS estimate possible, no further
                 *    improvement possible.
                 *    so if there is noise, the best test is to look at outlier
                 *    classification, which is done just below
                 */
                /*cout << norm(F, eFi) << " " <<
                 *    distanceBetweenFundamentalMatrices(camSize, F, eFi) << endl;*/
            }

            found = true;
            break;
        }

        if (!found)
        {
            cerr << "\t" << "solution is not valid : " << eF <<
                " vs " << F << endl;

            return false;
        }

        int mismatch = 0;
        for (int j = 0; j < nPoints; ++j)
        {
            /*
             * mask[j] = 1 si inlier, et outls[j] = 0 si inlier
             */
            if (mask[j] == outls[j])
            {
                cerr << "\t" << "outlier "
                     << (mask[j] ? "not detected : " : " detected : ")
                     << j << " with points : p=[" << _p1s[j * 2 + 0]
                     << "," << _p1s[j * 2 + 1]
                     << "] q=[" << _p2s[j * 2 + 0] << "," << _p2s[j * 2 + 1]
                     << "] and F=" << F << endl;

                /*
                 * tolerance of 10 inliers rejected (sigma is underestimated by
                 * LMedS method so there is a tendancy to reject more points
                 * than needed, this is because we take a conservative 2.5*sigma
                 * whereas the tail should be captured by an infinite*sigma or
                 * at least 10*sigma) but no tolerance if an outlier is missed
                 */
                if ((method == FM_LMEDS) && (mask[j] == 0))
                {
                    ++mismatch;
                }
                else
                {
                    return false;
                }

                /*
                 * in fact we should use something different than a fixed
                 * tolerance : we should evaluate by how much the point was
                 * tagged outlier, and decide if it is because of the
                 * underestimation of sigma, or because of an error of the
                 * algorithm
                 */
                if (mismatch > 10)
                {
                    return false;
                }
            }
        }

        cout << p1s << endl;
        cout << p2s << endl;
        cout << endl;

        ++i;
    }

    return true;
}

bool runRandTest(int    nIters,
                 int    method,
                 int    nPoints,
                 int    nOutls,
                 double noiseStdDev,
                 double threshold)
{
    RNG rng(0x12345678);

    double         _F[9];
    vector<double> _p1s(nPoints * 2), _p2s(nPoints * 2);
    Mat            F(3, 3, CV_64F, _F),
    p1s(nPoints, 1, CV_64FC2, _p1s.data()),
    p2s(nPoints, 1, CV_64FC2, _p2s.data());

    double _v[3], _p[3];
    Mat    v(1, 3, CV_64F, _v),
    pT(1, 3, CV_64F, _p);

    Size         camSize(1000, 1000);
    Mat          eF;
    Mat          meanPts, stddevPts;
    vector<bool> outls(nPoints), mask;

    for (int i = 0; i < nIters;)
    {
        randu(F, Scalar::all(-1), Scalar::all(1));
        // generate a fundamental from exact 7 points
        Mat pts1(7, 1, CV_64FC2), pts2(7, 1, CV_64FC2);
        randu(pts1, Scalar(0., 0.), Scalar(1000., 1000.));
        randu(pts2, Scalar(0., 0.), Scalar(1000., 1000.));
        findExactFundamentalMatrix(pts1, pts2, F);

        if (abs(F.at<double>(2, 2)) < numeric_limits<double>::epsilon())
        {
            continue;
        }
        F /= F.at<double>(2, 2);

        bool found = false;
        for (int j = 0, k = 0; j < nPoints && k < 1000; ++k)
        {
            double x2 = rng.uniform(0., 1000.), y2 = rng.uniform(0., 1000.);
            double x1, y1;
            int    t;

            _p[0] = x2; _p[1] = y2; _p[2] = 1;
            v     = pT * F;
            for (t = 0; t < 1000; ++t)
            {
                x1 = rng.uniform(0., 1000.);
                y1 = -(_v[0] * x1 + _v[2]) / _v[1];

                if ((y1 >= 0) && (y1 < 1000))
                {
                    break;
                }
            }

            if (t != 1000)
            {
                _p1s[j * 2 + 0] = x1; _p1s[j * 2 + 1] = y1;
                _p2s[j * 2 + 0] = x2; _p2s[j * 2 + 1] = y2;
                ++j;
                /*
                 * Mat p1 = (Mat_<double>(3, 1) << x1, y1, 1);
                 * Mat p2 = (Mat_<double>(3, 1) << x2, y2, 1);
                 * cout << p2.t()*F*p1 << endl;
                 */
            }

            if (j == nPoints) { found = true; }
        }

        if (!found) { continue; }

        // ignore degenerate cases (points too close to each other)
        // ********* TODO **************
        // we need a better test for this : an histogram by example
#if 1
        meanStdDev(p1s, meanPts, stddevPts);
        if ((stddevPts.at<double>(0, 0) < 200.) ||
            (stddevPts.at<double>(0, 1) < 200.))
        {
            continue;
        }
        meanStdDev(p2s, meanPts, stddevPts);
        if ((stddevPts.at<double>(0, 0) < 200.) ||
            (stddevPts.at<double>(0, 1) < 200.))
        {
            continue;
        }
#else
        if (!wellDistributed(p1s, camSize,
                             static_cast<int>(sqrt(p1s.rows * p1s.cols) * 0.6 + 0.5)))
        {
            continue;
        }
        if (!wellDistributed(p2s, camSize,
                             static_cast<int>(sqrt(p2s.rows * p2s.cols) * 0.6 + 0.5)))
        {
            continue;
        }
        cerr << "GO !!" << endl;
#endif

        add_noise(noiseStdDev, rng, p1s, p2s);

        add_outliers(outls, nOutls, threshold, rng, F, p1s, p2s);

        cout << i << endl;
        int ret = findFundamentalMatrix(p1s, p2s, eF, &mask,
                                        method, threshold, 0.99, camSize, camSize);
        if (((nPoints == 7) && (ret != 1) && (ret != 3)) ||
            ((nPoints > 7) && (ret != 1)))
        {
            cerr << "\t"
                 << (nPoints == 7
                ? "# of solutions is not 1 nor 3 : " : "# of solution is not 1 : ")
                 << ret << endl;

            return false;
        }

        /*
         * cout << F << endl;
         * cout << eF << endl;
         * getchar();
         */

        found = false;
        for (int j = 0; j < ret; ++j)
        {
            Mat eFi = eF.rowRange(j * 3, (j + 1) * 3);
            if (abs(eFi.at<double>(2, 2)) < numeric_limits<double>::epsilon())
            {
                continue;
            }

            eFi /= eFi.at<double>(2, 2);

            if (noiseStdDev < numeric_limits<double>::epsilon())
            {
                if (norm(F, eFi) > 10E-6)
                {
                    continue;
                }
            }
            else
            {
                /*cout << norm(F, eFi) << " " <<
                 *    distanceBetweenFundamentalMatrices(camSize, F, eFi) << endl;*/
            }

            found = true;
            break;
        }

        if (!found)
        {
            cerr << "\t" << "solution is not valid : " << eF <<
                " vs " << F << endl;

            return false;
        }

        int mismatch = 0;
        for (int j = 0; j < nPoints; ++j)
        {
            /*
             * mask[j] = 1 si inlier, et outls[j] = 0 si inlier
             */
            if (mask[j] == outls[j])
            {
                cerr << "\t" << "outlier "
                     << (mask[j] ? "not detected : " : " detected : ")
                     << j << " with points : p=[" << _p1s[j * 2 + 0]
                     << "," << _p1s[j * 2 + 1]
                     << "] q=[" << _p2s[j * 2 + 0] << "," << _p2s[j * 2 + 1]
                     << "] and F=" << F << endl;

                /*
                 * tolerance of 10 inliers rejected (sigma is underestimated by
                 * LMedS method so there is a tendancy to reject more points
                 * than needed, this is because we take a conservative 2.5*sigma
                 * whereas the tail should be captured by an infinite*sigma or
                 * at least 10*sigma) but no tolerance if an outlier is missed
                 */
                if ((method == FM_LMEDS) && (mask[j] == 0))
                {
                    ++mismatch;
                }
                else
                {
                    cout << p1s << endl;
                    cout << p2s << endl;
                    cout << F << endl;

                    return false;
                }

                /*
                 * in fact we should use something different than a fixed
                 * tolerance : we should evaluate by how much the point was
                 * tagged outlier, and decide if it is because of the
                 * underestimation of sigma, or because of an error of the
                 * algorithm
                 */
                if (mismatch > 10)
                {
                    return false;
                }
            }
        }

        /*for (int j = 0; j < nPoints; ++j) {
         *    [>
         * mask[j] = 1 si inlier, et outls[j] = 0 si inlier
         *     <]
         *    if (mask[j] == outls[j]) {
         *        cerr << "\t" << "outlier " <<
         *            (mask[j] ? "not detected : " : " detected : ") << j <<
         *            " with points : p=[" << _p1s[j*2+0] << "," << _p1s[j*2+1] <<
         *            "] q=[" << _p2s[j*2+0] << "," << _p2s[j*2+1] <<
         *            "] and F=" << F << endl;
         *        return false;
         *    }
         *   }*/
        ++i;
    }

    return true;
}

int main(int   argc,
         char *argv[])
{

    CmdLine cmd("Test program for fundamental estimation");

    PositiveConstraint<int>    ctr1;
    BoundConstraint<int>       ctr2(0, 3);
    PositiveConstraint<double> ctr3;
    BoundConstraint<int>       ctr4(7);

    ValueArg<int> numItersArg("i", "num-iters",
                              "Number of samples to try before acceptation",
                              false, 100, &ctr1, cmd);

    SwitchArg randomArg("r", "random",
                        "Use random points to generate the homography matrix",
                        true);

    SwitchArg sceneArg("s", "scene",
                       "Use an artificial scene to generate the homography matrix",
                       false);
    cmd.xorAdd(randomArg, sceneArg);

    ValueArg<int> methodArg("m", "method",
                            "Method used to compute the homography (0 = 7 points, 1 = 8points, 2 = RANSAC, 3 = LMEds)",
                            false, 2, &ctr2, cmd);

    ValueArg<int> numOutliersArg("o", "num-outliers",
                                 "Number of outliers to add",
                                 false, 50, &ctr1, cmd);

    ValueArg<int> numPointsArg("p", "num-points",
                               "Number of points to generate",
                               false, 50, &ctr4, cmd);

    ValueArg<double> noiseStdDevArg("n", "noise-dev",
                                    "Standard deviation of gaussian noise to add to each point",
                                    false, 0, &ctr3, cmd);

    ValueArg<double> thresholdArg("t", "threshold",
                                  "Threshold used to determine if a point is an outlier",
                                  false, 3, &ctr3, cmd);

    cmd.parse(argc, argv);

    int  nIters = numItersArg.getValue();
    bool random = true;
    if (randomArg.isSet()) { random = true; }
    else if (sceneArg.isSet())
    {
        random = false;
    }
    int    method      = methodArg.getValue();
    int    nOutls      = numOutliersArg.getValue();
    int    nPoints     = numPointsArg.getValue();
    double noiseStdDev = noiseStdDevArg.getValue();
    double threshold   = thresholdArg.getValue();

    cerr << "Running " << (random ? "random" : "scene")
         << " test with method "
         << (method == FM_7POINT ? "7 points"
        : method == FM_8POINT ? "8 points"
        : method == FM_RANSAC ? "RANSAC" : "LMeds")
         << " threshold = " << threshold
         << " and " << nPoints << " points with "
         << nOutls << " outliers and noise of dev " << noiseStdDev
         << endl;

    if (random)
    {
        if (!runRandTest(nIters, method, nPoints, nOutls, noiseStdDev, threshold))
        {
            return 1;
        }
    }
    else
    {
        if (!runSceneTest(nIters, method, nPoints, nOutls, noiseStdDev,
                          threshold))
        {
            return 1;
        }
    }

    return 0;
}
