/*
 * License Agreement for MVG
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <sys/time.h>

#include "util.hpp"

using namespace std;
using namespace Eigen;

namespace REDSVD {

    const float SVD_EPS = 0.0001f;

    double Util::getSec()
    {
        timeval tv;
        gettimeofday(&tv, NULL);

        return tv.tv_sec + (double)tv.tv_usec * 1e-6;
    }

    void Util::sampleTwoGaussian(float &f1,
                                 float &f2)
    {
        float v1  = (float)(rand() + 1.f) / ((float)RAND_MAX + 2.f);
        float v2  = (float)(rand() + 1.f) / ((float)RAND_MAX + 2.f);
        float len = sqrt(-2.f * log(v1));
        f1 = len * cos(2.f * M_PI * v2);
        f2 = len * sin(2.f * M_PI * v2);
    }

    void Util::sampleGaussianMat(MatrixXf &mat)
    {
        for (int i = 0; i < mat.rows(); ++i)
        {
            int j = 0;
            for (; j + 1 < mat.cols(); j += 2)
            {
                float f1, f2;
                sampleTwoGaussian(f1, f2);
                mat(i, j)     = f1;
                mat(i, j + 1) = f2;
            }
            for (; j < mat.cols(); j++)
            {
                float f1, f2;
                sampleTwoGaussian(f1, f2);
                mat(i, j) = f1;
            }
        }
    }

    void Util::processGramSchmidt(MatrixXf &mat)
    {
        for (int i = 0; i < mat.cols(); ++i)
        {
            for (int j = 0; j < i; ++j)
            {
                float r = mat.col(i).dot(mat.col(j));
                mat.col(i) -= r * mat.col(j);
            }
            float norm = mat.col(i).norm();
            if (norm < SVD_EPS)
            {
                for (int k = i; k < mat.cols(); ++k)
                {
                    mat.col(k).setZero();
                }

                return;
            }
            mat.col(i) *= (1.f / norm);
        }
    }

    void Util::convertFV2Mat(const vector<fv_t> &fvs,
                             REDSVD::SMatrixXf  &A)
    {
        int    maxID      = 0;
        size_t nonZeroNum = 0;
        for (size_t i = 0; i < fvs.size(); ++i)
        {
            const fv_t &fv(fvs[i]);
            for (size_t j = 0; j < fv.size(); ++j)
            {
                maxID = max(fv[j].first + 1, maxID);
            }
            nonZeroNum += fv.size();
        }
        A.resize(fvs.size(), maxID);
        A.reserve(nonZeroNum);
        for (size_t i = 0; i < fvs.size(); ++i)
        {
            A.startVec(i);
            const fv_t &fv(fvs[i]);
            for (size_t j = 0; j < fv.size(); ++j)
            {
                A.insertBack(i, fv[j].first) = fv[j].second;
            }
        }
        A.finalize();
    }

}
