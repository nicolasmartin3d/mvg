/*
 * License Agreement for MVG
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <xtclap/CmdLine.h>

#include "../src/resect.h"
#include "../src/mvg.h"

using namespace cv;
using namespace std;
using namespace TCLAP;

bool flipCoin(RNG &rng) { return rng.uniform(0, 2) == 0; }

// from OpenCV
void exact_homography(const vector<Point2d> &p1s,
                      const vector<Point2d> &p2s,
                      Mat                   &H)
{
    Mat    X(8, 1, CV_64F, H.ptr<double>());
    double a[8][8], b[8];
    Mat    A(8, 8, CV_64F, a), B(8, 1, CV_64F, b);

    // *INDENT-OFF*
    for( int i = 0; i < 4; ++i )
    {
        a[i][0] = a[i+4][3] = p1s[i].x;
        a[i][1] = a[i+4][4] = p1s[i].y;
        a[i][2] = a[i+4][5] = 1;
        a[i][3] = a[i][4] = a[i][5] =
        a[i+4][0] = a[i+4][1] = a[i+4][2] = 0;
        a[i][6] = -p1s[i].x*p2s[i].x;
        a[i][7] = -p1s[i].y*p2s[i].x;
        a[i+4][6] = -p1s[i].x*p2s[i].y;
        a[i+4][7] = -p1s[i].y*p2s[i].y;
        b[i] = p2s[i].x;
        b[i+4] = p2s[i].y;
    }
    // *INDENT-ON*

    solve(A, B, X, DECOMP_SVD);
    H.at<double>(2, 2) = 1.;
}

void add_noise(double noiseStdDev,
               double noise3DStdDev,
               RNG   &rng,
               Mat   &p1s,
               Mat   &p2s)
{
    if (noiseStdDev < numeric_limits<double>::epsilon())
    {
        return;
    }

    Mat n1 = p1s.clone(), n2 = p2s.clone();
    randn(n1, Scalar::all(0.), Scalar::all(noise3DStdDev));
    randn(n2, Scalar::all(0.), Scalar::all(noiseStdDev));

    p1s += n1;
    p2s += n2;
}

void add_outliers(vector<bool> &outls,
                  int           nOutls,
                  double        threshold,
                  RNG          &rng,
                  const Mat    &M,
                  Mat          &p1s,
                  Mat          &p2s)
{
    double *_p1s    = p1s.ptr<double>(), *_p2s = p2s.ptr<double>();
    size_t  nPoints = outls.size();
    // threshold ^2 *8 to be sure !
    const double outlThreshold = threshold * threshold * 12;

    const double minOutlDistance   = 20.0, maxOutlDistance = 40.0;
    const double min3DOutlDistance = 5.0, max3DOutlDistance = 10.;
    fill(outls.begin(), outls.end(), false);
    for (int j = 0; j < nOutls;)
    {
        int id = rng.uniform(0, static_cast<int>(nPoints));
        if (outls[id]) { continue; }

        bool outlier = false;
        for (int k = 0; k < 10; ++k)
        {
            double x1, y1, z1, x2, y2;
            do
            {
                x1 = _p1s[id * 3 + 0]; y1 = _p1s[id * 3 + 1]; z1 = _p1s[id * 3 + 2];
                x2 = _p2s[id * 2 + 0]; y2 = _p2s[id * 2 + 1];
                if (flipCoin(rng))
                {
                    x1 += rng.uniform(min3DOutlDistance, max3DOutlDistance) *
                          (rng.uniform(0, 2) * 2 - 1);
                }
                if (flipCoin(rng))
                {
                    y1 += rng.uniform(min3DOutlDistance, max3DOutlDistance) *
                          (rng.uniform(0, 2) * 2 - 1);
                }
                if (flipCoin(rng))
                {
                    z1 += rng.uniform(min3DOutlDistance, max3DOutlDistance) *
                          (rng.uniform(0, 2) * 2 - 1);
                }
                if (flipCoin(rng))
                {
                    x2 += rng.uniform(minOutlDistance, maxOutlDistance) *
                          (rng.uniform(0, 2) * 2 - 1);
                }
                if (flipCoin(rng))
                {
                    y2 += rng.uniform(minOutlDistance, maxOutlDistance) *
                          (rng.uniform(0, 2) * 2 - 1);
                }
            }
            while (x1 < -30 || x1 > 30 || y1 < -30 || y1 > 30 || z1 < 30 || z1 > 80 ||
                   x2 < 0 || x2 > 1000 || y2 < 0 || y2 > 1000);

            Point3d p1(x1, y1, z1);
            Point2d p2(x2, y2);
            if (squaredDistanceToCameraProjection(M, p1, p2)
                > outlThreshold)
            {
                _p1s[id * 3 + 0] = x1; _p1s[id * 3 + 1] = y1; _p1s[id * 3 + 2] = z1;
                _p2s[id * 2 + 0] = x2; _p2s[id * 2 + 1] = y2;
                outlier          = true;
                break;
            }
        }
        if (!outlier) { continue; }

        outls[id] = true;
        ++j;
    }
}

bool runSceneTest(int    nIters,
                  int    method,
                  int    nPoints,
                  int    nOutls,
                  double noiseStdDev,
                  double noise3DStdDev,
                  double threshold)
{
    RNG rng(0x12345678);

    double _K[9];
    Mat    K(3, 3, CV_64F, _K);

    double _v[3], _c[3];
    Mat    v(3, 1, CV_64F, _v);
    Mat    c(3, 1, CV_64F, _c), t, tX;
    Mat    R, tR;
    Mat    M(3, 4, CV_64F),
    Ms3x3 = M(Range::all(), Range(0, 3)),
    Ms1x3 = M(Range::all(), Range(3, 4));
    Mat eM;

    Size           camSize(1000, 1000);
    vector<double> _p3ds(3 * nPoints), _ps(2 * nPoints);
    Mat            p3ds = Mat(nPoints, 1, CV_64FC3, _p3ds.data()),
                   ps   = Mat(nPoints, 1, CV_64FC2, _ps.data());
    vector<bool> outls(nPoints), mask;
    Mat          meanPts, stddevPts;

    for (int i = 0; i < nIters;)
    {
        double fx = rng.uniform(1500, 2500),
               ar = rng.uniform(0.95, 1.05),
               cx = rng.uniform(1000 / 2. - 75, 1000 / 2. + 75),
               cy = rng.uniform(1000 / 2. - 75, 1000 / 2. + 75);

        K             = Mat::eye(3, 3, CV_64F);
        _K[0 * 3 + 0] = fx;
        _K[1 * 3 + 1] = fx * ar;
        _K[0 * 3 + 2] = cx;
        _K[1 * 3 + 2] = cy;

        // generate the scene
        R                    = Mat::eye(3, 3, CV_64F);
        _v[0]                = _v[1] = 0; _v[2] = rng.uniform(-CV_PI / 8., CV_PI / 8.);
        Rodrigues(v, tR); R *= tR;
        _v[0]                = _v[2] = 0; _v[1] = rng.uniform(-CV_PI / 8., CV_PI / 8.);
        Rodrigues(v, tR); R *= tR;
        _v[1]                = _v[2] = 0; _v[0] = rng.uniform(-CV_PI / 8., CV_PI / 8.);
        Rodrigues(v, tR); R *= tR;

        randu(c, Scalar::all(-30), Scalar::all(30));
        t = R * -c;

        R.copyTo(Ms3x3);
        t.copyTo(Ms1x3);
        M = K * M;

        // generate the 3D points and checks that its projection is within the
        // image
        double _p3d[4], _p[3];
        Mat    p3d(4, 1, CV_64F, _p3d),
        p(3, 1, CV_64F, _p);
        bool found = false;

        for (int iters = 0, j = 0; j < nPoints && iters < nPoints * 2; ++iters)
        {
            _p3d[0] = rng.uniform(-20., 20.);
            _p3d[1] = rng.uniform(-20., 20.);
            _p3d[2] = rng.uniform(30., 80.);
            _p3d[3] = 1;

            p = M * p3d; p /= _p[2];

            if ((_p[0] < 0) || (_p[0] > 1000) ||
                (_p[1] < 0) || (_p[1] > 1000))
            {
                continue;
            }

            for (int k = 0; k < 3; ++k)
            {
                _p3ds[j * 3 + k] = _p3d[k];
            }
            for (int k = 0; k < 2; ++k)
            {
                _ps[j * 2 + k] = _p[k];
            }
            ++j;

            if (j == nPoints) { found = true; }
        }

        if (!found) { continue; }

        // ignore degenerate cases (points too close to each other)
        meanStdDev(ps, meanPts, stddevPts);
        if ((stddevPts.at<double>(0, 0) < 200.) ||
            (stddevPts.at<double>(0, 1) < 200.))
        {
            continue;
        }
        meanStdDev(p3ds, meanPts, stddevPts);
        if ((stddevPts.at<double>(0, 0) < 8.) ||
            (stddevPts.at<double>(0, 1) < 8.) ||
            (stddevPts.at<double>(0, 2) < 8.))
        {
            continue;
        }

        add_noise(noiseStdDev, noise3DStdDev, rng, p3ds, ps);

        add_outliers(outls, nOutls, threshold, rng, M, p3ds, ps);

        int ret = findResectionMatrix(p3ds, ps, eM, &mask,
                                      method, threshold, 0.99, camSize);

        if (ret != 1)
        {
            cerr << "\t"
                 << "# of solution is not 1 : "
                 << ret << endl;

            return false;
        }

        found = true;
        Mat eK, eR, et;
        if (!decompositionKRT(eM, eK, eR, et))
        {
            cerr << "\t" << "decomposition KRT failed" << endl;
            cout << eM << endl;

            return false;
        }
        if (noiseStdDev < numeric_limits<double>::epsilon())
        {
            if ((norm(K, eK) > 10E-6) || (norm(R, eR) > 10E-6) ||
                (norm(t, et) >  10e-6))
            {
                found = false;
            }
        }
        else
        {
            /*
             * we should use the true distance to decide if the solution is
             *    good enough, but just looking at the classification of
             *    outliers gives us an hint. if all points are well classified
             *    then we get the best LS estimate possible, no further
             *    improvement possible.
             *    so if there is noise, the best test is to look at outlier
             *    classification, which is done just below
             */
            /*cout << norm(H, eHi) << " "
             *   << endl;*/
        }

        if (!found)
        {
            cout << K << endl;
            cout << eK << endl;
            cout << norm(K, eK) << endl;
            cout << R << endl;
            cout << eR << endl;
            cout << norm(R, eR) << endl;
            cout << t << endl;
            cout << et << endl;
            cout << norm(t, et) << endl;
            cerr << "\t" << "solution is not valid : " << eM <<
                " vs " << M << endl;

            return false;
        }

        int mismatch = 0;
        for (int j = 0; j < nPoints; ++j)
        {
            /*
             * mask[j] = 1 si inlier, et outls[j] = 0 si inlier
             */
            if (mask[j] == outls[j])
            {
                cerr << "\t" << "outlier "
                     << (mask[j] ? "not detected : " : " detected : ")
                     << j << " with points : p3d=[" << _p3ds[j * 3 + 0] << ","
                     << _p3ds[j * 3 + 1] << "," << _p3ds[j * 3 + 2]
                     << "] p=[" << _ps[j * 2 + 0] << "," << _ps[j * 2 + 1]
                     << "] and M=" << M << endl;

                /*
                 * tolerance of 10 inliers rejected (sigma is underestimated by
                 * LMedS method so there is a tendancy to reject more points
                 * than needed, this is because we take a conservative 2.5*sigma
                 * whereas the tail should be captured by an infinite*sigma or
                 * at least 10*sigma) but no tolerance if an outlier is missed
                 */
                if ((method == RM_LMEDS) && (mask[j] == 0))
                {
                    ++mismatch;
                }
                else
                {
                    return false;
                }

                /*
                 * in fact we should use something different than a fixed
                 * tolerance : we should evaluate by how much the point was
                 * tagged outlier, and decide if it is because of the
                 * underestimation of sigma, or because of an error of the
                 * algorithm
                 */
                if (mismatch > 10)
                {
                    return false;
                }
            }
        }
        ++i;
    }

    return true;
}

bool runRandTest(int    nIters,
                 int    method,
                 int    nPoints,
                 int    nOutls,
                 double noiseStdDev,
                 double noise3DStdDev,
                 double threshold)
{
    RNG rng(0x12345678);

    double         _M[12], _K[9], _R[9], _t[3];
    vector<double> _p3ds(nPoints * 3), _p1s(nPoints * 2);
    Mat            M(3, 4, CV_64F, _M),
    Ms3x3 = M(Range::all(), Range(0, 3)),
    Ms1x3 = M(Range::all(), Range(3, 4)),
    K(3, 3, CV_64F, _K),
    R(3, 3, CV_64F, _R),
    t(3, 1, CV_64F, _t),
    p3ds(nPoints, 1, CV_64FC3, _p3ds.data()),
    p1s(nPoints, 1, CV_64FC2, _p1s.data());
    Mat iM      = Mat::eye(4, 4, CV_64F),
        iM3x4   = iM(Range(0, 3), Range::all());
    double *_iM = iM.ptr<double>();

    double _v[3], _p[3];
    Mat    v(3, 1, CV_64F, _v),
    p(3, 1, CV_64F, _p);

    Size         camSize(1000, 1000);
    Mat          eM;
    Mat          meanPts, stddevPts;
    vector<bool> outls(nPoints), mask;

    for (int i = 0; i < nIters;)
    {
        // generate a camera from exact 6 points
        Mat pts1(6, 1, CV_64FC3), pts2(6, 1, CV_64FC2);
        randu(pts1, Scalar(-20., -20., 30.), Scalar(20., 20., 80.));
        randu(pts2, Scalar(0., 0.), Scalar(1000., 1000.));
        findExactResectionMatrix(pts1, pts2, M);

        if (!decompositionKRT(M, K, R, t))
        {
            continue;
        }

        M.copyTo(iM3x4);
        iM = iM.inv();

        // tests sur K, R, t générés ?
        /*if (abs(M.at<double>(2, 2)) < numeric_limits<double>::epsilon())
         *    continue;
         *   H /= H.at<double>(2, 2);*/

        // generate the 3D points by deprojecting an image point back
        // and checking that its coordinates are within a cube of
        // "normal" coordinates
        double _p3d[4], _p1[4];
        Mat    p3d(4, 1, CV_64F, _p3d),
        p1(4, 1, CV_64F, _p1);
        bool found = false;

        for (int iters = 0, j = 0; j < nPoints && iters < nPoints * 2; ++iters)
        {
            const int maxDepthIters = 10;

            double x, y, z;
            x      = _p1[0] = rng.uniform(0., 1000.);
            y      = _p1[1] = rng.uniform(0., 1000.);
            _p1[2] = 1;

            int d;
            for (d = 0; d < maxDepthIters; ++d)
            {
                z = rng.uniform(30., 80.);

                // after deprojection _p3d[2] = z !
                _p1[3] = (_iM[2 * 4 + 0] * x + _iM[2 * 4 + 1] * y + _iM[2 * 4 + 2] - z *
                          (_iM[3 * 4 + 0] * x + _iM[3 * 4 + 1] * y + _iM[3 * 4 + 2])) /
                         (_iM[3 * 4 + 3] * z - _iM[2 * 4 + 3]);

                p3d = iM * p1; p3d /= _p3d[3];

                if ((_p3d[0] >= -20) && (_p3d[0] <= 20) &&
                    (_p3d[1] >= -20) && (_p3d[1] <= 20))
                {
                    break;
                }
            }

            if (d < maxDepthIters)
            {
                for (int k = 0; k < 3; ++k)
                {
                    _p3ds[j * 3 + k] = _p3d[k];
                }
                for (int k = 0; k < 2; ++k)
                {
                    _p1s[j * 2 + k] = _p1[k];
                }
                ++j;
            }

            if (j == nPoints) { found = true; }
        }

        if (!found) { continue; }

        // ignore degenerate cases (points too close to each other)
        meanStdDev(p1s, meanPts, stddevPts);
        if ((stddevPts.at<double>(0, 0) < 200.) ||
            (stddevPts.at<double>(0, 1) < 200.))
        {
            continue;
        }
        meanStdDev(p3ds, meanPts, stddevPts);
        if ((stddevPts.at<double>(0, 0) < 8.) ||
            (stddevPts.at<double>(0, 1) < 8.) ||
            (stddevPts.at<double>(0, 2) < 8.))
        {
            continue;
        }

        add_noise(noiseStdDev, noise3DStdDev, rng, p3ds, p1s);

        add_outliers(outls, nOutls, threshold, rng, M, p3ds, p1s);

        int ret = findResectionMatrix(p3ds, p1s, eM, &mask,
                                      method, threshold, 0.99, camSize);

        if (ret != 1)
        {
            cerr << "\t"
                 << "# of solution is not 1 : "
                 << ret << endl;

            return false;
        }

        found = true;
        Mat eK, eR, et;
        if (!decompositionKRT(eM, eK, eR, et))
        {
            cerr << "\t" << "decomposition KRT failed" << endl;
            cerr << eM << endl;

            return false;
        }
        if (noiseStdDev < numeric_limits<double>::epsilon())
        {
            if ((norm(K, eK) > 10E-6) || (norm(R, eR) > 10E-6) ||
                (norm(t, et) > 10e-6))
            {
                found = false;
            }
        }
        else
        {
            /*
             * we should use the true distance to decide if the solution is
             *    good enough, but just looking at the classification of
             *    outliers gives us an hint. if all points are well classified
             *    then we get the best LS estimate possible, no further
             *    improvement possible.
             *    so if there is noise, the best test is to look at outlier
             *    classification, which is done just below
             */
            /*cout << norm(H, eHi) << " "
             *   << endl;*/
        }

        if (!found)
        {
            cout << K << endl;
            cout << eK << endl;
            cout << norm(K, eK) << endl;
            cout << R << endl;
            cout << eR << endl;
            cout << norm(R, eR) << endl;
            cout << t << endl;
            cout << et << endl;
            cout << norm(t, et) << endl;
            cout << p1s << endl;
            cout << p3ds << endl;
            cerr << "\t" << "solution is not valid : " << eM <<
                " vs " << M << endl;

            return false;
        }

        for (int j = 0; j < nPoints; ++j)
        {
            /*
             * mask[j] = 1 si inlier, et outls[j] = 0 si inlier
             */
            if (mask[j] == outls[j])
            {
                cerr << "\t" << "outlier "
                     << (mask[j] ? "not detected : " : " detected : ")
                     << j << " with points : p3d=[" << _p3ds[j * 3 + 0]
                     << "," << _p3ds[j * 3 + 1] << "," << _p3ds[j * 3 + 2]
                     << "] p=[" << _p1s[j * 2 + 0] << "," << _p1s[j * 2 + 1]
                     << "] and M=" << M << endl;

                return false;
            }
        }
        ++i;
    }

    return true;
}

int main(int   argc,
         char *argv[])
{

    CmdLine cmd("Test program for fundamental estimation");

    PositiveConstraint<int>    ctr1;
    BoundConstraint<int>       ctr2(2, 3);
    PositiveConstraint<double> ctr3;
    BoundConstraint<int>       ctr4(4);

    ValueArg<int> numItersArg("i", "num-iters",
                              "Number of samples to try before acceptation",
                              false, 100, &ctr1, cmd);

    SwitchArg randomArg("r", "random",
                        "Use random points to generate the homography matrix",
                        true);

    SwitchArg sceneArg("s", "scene",
                       "Use an artificial scene to generate the homography matrix",
                       false);
    cmd.xorAdd(randomArg, sceneArg);

    ValueArg<int> methodArg("m", "method",
                            "Method used to compute the homography (0 = 7 points, 1 = 8points, 2 = RANSAC, 3 = LMEds)",
                            false, 2, &ctr2, cmd);

    ValueArg<int> numOutliersArg("o", "num-outliers",
                                 "Number of outliers to add",
                                 false, 50, &ctr1, cmd);

    ValueArg<int> numPointsArg("p", "num-points",
                               "Number of points to generate",
                               false, 50, &ctr4, cmd);

    ValueArg<double> noiseStdDevArg("n", "noise-dev",
                                    "Standard deviation of gaussian noise to add to each 2D point",
                                    false, 0, &ctr3, cmd);

    ValueArg<double> noise3DStdDevArg("z", "noise-dev",
                                      "Standard deviation of gaussian noise to add to each 3D point",
                                      false, 0, &ctr3, cmd);

    ValueArg<double> thresholdArg("t", "threshold",
                                  "Threshold used to determine if a point is an outlier",
                                  false, 3, &ctr3, cmd);

    cmd.parse(argc, argv);

    int  nIters = numItersArg.getValue();
    bool random = true;
    if (randomArg.isSet()) { random = true; }
    else if (sceneArg.isSet())
    {
        random = false;
    }
    int    method        = methodArg.getValue();
    int    nOutls        = numOutliersArg.getValue();
    int    nPoints       = numPointsArg.getValue();
    double noiseStdDev   = noiseStdDevArg.getValue();
    double noise3DStdDev = noise3DStdDevArg.getValue();
    double threshold     = thresholdArg.getValue();

    cerr << "Running " << (random ? "random" : "scene")
         << " test with method "
         << (method == RM_RANSAC ? "RANSAC" : "LMeds")
         << " threshold = " << threshold
         << " and " << nPoints << " points with "
         << nOutls << " outliers and noise of dev " << noiseStdDev
         << " and " << noise3DStdDev << " for 3D points "
         << endl;

    if (random)
    {
        if (!runRandTest(nIters, method, nPoints, nOutls, noiseStdDev,
                         noise3DStdDev, threshold))
        {
            return 1;
        }
    }
    else
    {
        if (!runSceneTest(nIters, method, nPoints, nOutls, noiseStdDev,
                          noise3DStdDev, threshold))
        {
            return 1;
        }
    }

    return 0;
}
