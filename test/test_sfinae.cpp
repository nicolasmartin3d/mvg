/*
 * License Agreement for MVG
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include "../src/logger.h"
#include "../src/sfinae.h"

typedef char ( &yes)[1];
typedef char ( &no)[2];

template <typename B, typename D>
struct Host
{
    operator B *() const;
    operator D *();
};

template<typename T, typename U>
struct is_same
{
    static const bool value = false;
};

template<typename T>
struct is_same<T, T>  // specialization
{
    static const bool value = true;
};

template <typename B, typename D>
struct is_base_of
{
    template <typename T>
    static yes check(D *, T);
    static no  check(B *,
                     int);

    static const bool value =
        (sizeof(check(Host<B, D>(), int())) == sizeof(yes)) ||
        is_same<B, D>::value;
};

class Base
{ };
class Derived : private Base
{ };

int main(int   argc,
         char *argv[])
{
    (void)argc;
    (void)argv;

    if (IsBaseOf<Base, Derived>::value &&
        !IsBaseOf<Derived, Base>::value)
    {
        logInfo() << "OK1";
    }
    else
    {
        logWarning() << "IsBaseOf not working";
    }

    if (IsBaseOf<Base, Base>::value)
    {
        logInfo() << "OK2";
    }
    else
    {
        logWarning() << "IsBaseOf not working";
    }

    if (is_base_of<Base, Derived>::value &&
        !is_base_of<Derived, Base>::value)
    {
        logInfo() << "OK3";
    }
    else
    {
        logWarning() << "is_base_of not working";
    }

    if (is_base_of<Base, Base>::value)
    {
        logInfo() << "OK4";
    }
    else
    {
        logWarning() << "is_base_of not working";
    }

}
