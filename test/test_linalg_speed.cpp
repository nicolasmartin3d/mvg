/*
 * License Agreement for MVG
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// From http://nghiaho.com/?p=954
#include "redsvd.hpp"

#include <cstdio>
#include <iostream>
#include <sys/time.h>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <armadillo>

#include "../mvg/mvg.h"

using namespace std;

int ITERATIONS = 1000;

const string svd_file  = "svd.html";
const string mul_file  = "multiply.html";
const string add_file  = "addition.html";
const string inv_file  = "invert.html";
const string tran_file = "transpose.html";

struct Results
{
    double svd;
    double mul;
    double add;
    double inv;
    double tran;
};

double TimeDiff(timeval t1,
                timeval t2);
void RunOpenCV(int                    size,
               const vector <double> &values,
               Results               &results);
void RunArmadillo(int                    size,
                  const vector <double> &values,
                  Results               &results);
void RunEigen(int                    size,
              const vector <double> &values,
              Results               &results);
void RunLSVD(int                    size,
             const vector <double> &values,
             Results               &results);
void RunRedSVD(int                    size,
               const vector <double> &values,
               Results               &results);
void HTMLOutput(ofstream                        &os,
                const vector < vector<double> > &results,
                const vector <int>              &labels);

int main()
{
    int start = 4;
    int end   = 1024;

    vector < vector<double> > svd_results;
    vector < vector<double> > mul_results;
    vector < vector<double> > add_results;
    vector < vector<double> > inv_results;
    vector < vector<double> > tran_results;
    vector <int>              labels;

    for (int i = start; i < end; i = i * 2)
    {
        labels.push_back(i);

        if (i >= 256)
        {
            ITERATIONS = 1;
        }
        else if (i >= 64)
        {
            ITERATIONS = 100;
        }
        else
        {
            ITERATIONS = 1000;
        }

        vector <double> values;

        // Fill with random values
        for (int j = 0; j < i; j++)
        {
            for (int k = 0; k < i; k++)
            {
                double v = rand() / (1.0 + RAND_MAX);
                values.push_back(v);
            }
        }

        Results r1, r2, r3, r4, r5;

        RunOpenCV(i, values, r1);
        RunArmadillo(i, values, r2);
        RunEigen(i, values, r3);
        RunLSVD(i, values, r4);
        RunRedSVD(i, values, r5);

        vector <double> svd, mul, add, inv, tran;

        svd.push_back(r1.svd / ITERATIONS);
        svd.push_back(r2.svd / ITERATIONS);
        svd.push_back(r3.svd / ITERATIONS);
        svd.push_back(r4.svd / ITERATIONS);
        svd.push_back(r5.svd / ITERATIONS);
        svd_results.push_back(svd);

        mul.push_back(r1.mul / ITERATIONS);
        mul.push_back(r2.mul / ITERATIONS);
        mul.push_back(r3.mul / ITERATIONS);
        mul_results.push_back(mul);

        add.push_back(r1.add / ITERATIONS);
        add.push_back(r2.add / ITERATIONS);
        add.push_back(r3.add / ITERATIONS);
        add_results.push_back(add);

        inv.push_back(r1.inv / ITERATIONS);
        inv.push_back(r2.inv / ITERATIONS);
        inv.push_back(r3.inv / ITERATIONS);
        inv_results.push_back(inv);

        tran.push_back(r1.tran / ITERATIONS);
        tran.push_back(r2.tran / ITERATIONS);
        tran.push_back(r3.tran / ITERATIONS);
        tran_results.push_back(tran);

        cout << endl;
    }

    // Output HTML table
    ofstream output_svd(svd_file.c_str());
    ofstream output_mul(mul_file.c_str());
    ofstream output_add(add_file.c_str());
    ofstream output_inv(inv_file.c_str());
    ofstream output_tran(tran_file.c_str());

    HTMLOutput(output_svd, svd_results, labels);
    HTMLOutput(output_mul, mul_results, labels);
    HTMLOutput(output_add, add_results, labels);
    HTMLOutput(output_inv, inv_results, labels);
    HTMLOutput(output_tran, tran_results, labels);

    return 0;
}

void HTMLOutput(ofstream                        &os,
                const vector < vector<double> > &results,
                const vector <int>              &labels)
{
    os.setf(ios::fixed, ios::floatfield);   // floatfield set to fixed

    os.precision(5);

    os << "<h2>Raw data</h2>" << endl;
    os << "<table border=1>" << endl;

    if (results[0].size() == 3)
    {
        os <<
            "<tr><td>Results in ms</td><td>OpenCV</td><td>Armadillo</td><td>Eigen</td></tr>"
           << endl;
    }
    else if (results[0].size() == 5)
    {
        os <<
            "<tr><td>Results in ms</td><td>OpenCV</td><td>Armadillo</td><td>Eigen</td><td>LSVD</td><td>RedSVD</td></tr>"
           << endl;
    }

    for (size_t i = 0; i < results.size(); i++)
    {
        os << "<tr><td>" << labels[i] << "x" << labels[i] << "</td>";

        double best = *min_element(results[i].begin(), results[i].end());

        for (size_t j = 0; j < results[i].size(); j++)
        {
            if (results[i][j] == best)
            {
                os << "<td><font color=red>" << results[i][j] << "</font></td>";
            }
            else
            {
                os << "<td>" << results[i][j] << "</td>";
            }
        }

        os << "</tr>" << endl;
    }

    os << "</table>" << endl;
    os << "<h2>Normalised</h2>" << endl;
    os << "<table border=1>" << endl;
    if (results[0].size() == 3)
    {
        os <<
            "<tr><td>Speed up over slowest</td><td>OpenCV</td><td>Armadillo</td><td>Eigen</td></tr>"
           << endl;
    }
    else if (results[0].size() == 5)
    {
        os <<
            "<tr><td>Speed up over slowest</td><td>OpenCV</td><td>Armadillo</td><td>Eigen</td><td>LSVD</td><td>RedSVD</td></tr>"
           << endl;
    }

    os.precision(2);

    for (size_t i = 0; i < results.size(); i++)
    {
        os << "<tr><td>" << labels[i] << "x" << labels[i] << "</td>";

        double m    = *max_element(results[i].begin(), results[i].end());
        double best = *min_element(results[i].begin(), results[i].end());

        for (size_t j = 0; j < results[i].size(); j++)
        {
            if (results[i][j] == best)
            {
                os << "<td><font color=red>" << (m / results[i][j]) << "x</font></td>";
            }
            else
            {
                os << "<td>" << (m / results[i][j]) << "x</td>";
            }
        }

        os << "</tr>" << endl;
    }

    os << "</table>" << endl;
}

double TimeDiff(timeval t1,
                timeval t2)
{
    double t;
    t  = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
    t += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms

    return t;
}

void RunLSVD(int                    size,
             const vector <double> &values,
             Results               &results)
{
    cv::Mat A(size, size, CV_64F);

    timeval t1, t2;
    double  d;

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            A.at<double>(i, j) = values[i * size + j];
        }
    }

    LSVD lsvd;
    gettimeofday(&t1, NULL);
    for (int i = 0; i < ITERATIONS; i++)
    {
        lsvd(A);
    }
    gettimeofday(&t2, NULL);
    d = TimeDiff(t1, t2);
    cout << "LSVD: " << size << "x" << size << " - " << d << endl;
    results.svd = d;
}

void RunOpenCV(int                    size,
               const vector <double> &values,
               Results               &results)
{
    cv::Mat A(size, size, CV_64F);
    cv::Mat B(size, size, CV_64F);
    cv::Mat C(size, size, CV_64F);

    timeval t1, t2;
    double  d;

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            A.at<double>(i, j) = values[i * size + j];
            B.at<double>(i, j) = values[i * size + j];
        }
    }

    cv::SVD svd;

    gettimeofday(&t1, NULL);
    for (int i = 0; i < ITERATIONS; i++)
    {
        svd(A);
    }
    gettimeofday(&t2, NULL);
    d = TimeDiff(t1, t2);
    cout << "OpenCV SVD: " << size << "x" << size << " - " << d << endl;
    results.svd = d;

    gettimeofday(&t1, NULL);
    for (int i = 0; i < ITERATIONS; i++)
    {
        C = A + B;
    }
    gettimeofday(&t2, NULL);
    d = TimeDiff(t1, t2);
    cout << "OpenCV addition: " << size << "x" << size << " - " <<
        TimeDiff(t1, t2) << endl;
    results.add = d;

    gettimeofday(&t1, NULL);
    for (int i = 0; i < ITERATIONS; i++)
    {
        C = A * B;
    }
    gettimeofday(&t2, NULL);
    d = TimeDiff(t1, t2);
    cout << "OpenCV multiply: " << size << "x" << size << " - " <<
        TimeDiff(t1, t2) << endl;
    results.mul = d;

    gettimeofday(&t1, NULL);
    for (int i = 0; i < ITERATIONS; i++)
    {
        C = A.inv();
    }
    gettimeofday(&t2, NULL);
    d = TimeDiff(t1, t2);
    cout << "OpenCV invert: " << size << "x" << size << " - " << TimeDiff(t1, t2) << endl;
    results.inv = d;

    gettimeofday(&t1, NULL);
    for (int i = 0; i < ITERATIONS; i++)
    {
        C = A.t();
    }
    gettimeofday(&t2, NULL);
    d = TimeDiff(t1, t2);
    cout << "OpenCV transpose: " << size << "x" << size << " - " <<
        TimeDiff(t1, t2) << endl;
    results.tran = d;
}

void RunArmadillo(int                    size,
                  const vector <double> &values,
                  Results               &results)
{
    arma::mat A(size, size), U, V;
    arma::mat B(size, size), C(size, size);
    arma::vec S;

    timeval t1, t2;
    double  d;

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            A(i, j) = values[i * size + j];
            B(i, j) = values[i * size + j];
        }
    }

    gettimeofday(&t1, NULL);
    for (int i = 0; i < ITERATIONS; i++)
    {
        arma::svd(U, S, V, A);
    }
    gettimeofday(&t2, NULL);
    d           = TimeDiff(t1, t2);
    results.svd = d;
    cout << "Armadillo SVD: " << size << "x" << size << " - " << TimeDiff(t1, t2) << endl;

    gettimeofday(&t1, NULL);
    for (int i = 0; i < ITERATIONS; i++)
    {
        C = A + B;
    }
    gettimeofday(&t2, NULL);
    d           = TimeDiff(t1, t2);
    results.add = d;
    cout << "Armadillo addition: " << size << "x" << size << " - " <<
        TimeDiff(t1, t2) << endl;

    gettimeofday(&t1, NULL);
    for (int i = 0; i < ITERATIONS; i++)
    {
        C = A * B;
    }
    gettimeofday(&t2, NULL);
    d           = TimeDiff(t1, t2);
    results.mul = d;
    cout << "Armadillo multiply: " << size << "x" << size << " - " <<
        TimeDiff(t1, t2) << endl;

    gettimeofday(&t1, NULL);
    for (int i = 0; i < ITERATIONS; i++)
    {
        C = arma::inv(A);
    }
    gettimeofday(&t2, NULL);
    d           = TimeDiff(t1, t2);
    results.inv = d;
    cout << "Armadillo invert: " << size << "x" << size << " - " <<
        TimeDiff(t1, t2) << endl;

    gettimeofday(&t1, NULL);
    for (int i = 0; i < ITERATIONS; i++)
    {
        C = arma::trans(A);
    }
    gettimeofday(&t2, NULL);
    d            = TimeDiff(t1, t2);
    results.tran = d;
    cout << "Armadillo transpose: " << size << "x" << size << " - " <<
        TimeDiff(t1, t2) << endl;
}

void RunRedSVD(int                    size,
               const vector <double> &values,
               Results               &results)
{
    Eigen::MatrixXf A(size, size), U, V;

    timeval t1, t2;
    double  d;

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            A(i, j) = values[i * size + j];
        }
    }

    gettimeofday(&t1, NULL);
    for (int i = 0; i < ITERATIONS; i++)
    {
        REDSVD::RedSVD svd(A);
    }
    gettimeofday(&t2, NULL);
    d           = TimeDiff(t1, t2);
    results.svd = d;
    cout << "RedSVD: " << size << "x" << size << " - " << TimeDiff(t1, t2) << endl;
}

void RunEigen(int                    size,
              const vector <double> &values,
              Results               &results)
{
    Eigen::MatrixXd A(size, size), U, V;
    Eigen::MatrixXd B(size, size);
    Eigen::MatrixXd C(size, size);

    arma::vec S;

    timeval t1, t2;
    double  d;

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            A(i, j) = values[i * size + j];
        }
    }

    gettimeofday(&t1, NULL);
    for (int i = 0; i < ITERATIONS; i++)
    {
        Eigen::JacobiSVD<Eigen::MatrixXd> svd(A,
                                              Eigen::ComputeThinU || Eigen::ComputeThinV);
    }
    gettimeofday(&t2, NULL);
    d           = TimeDiff(t1, t2);
    results.svd = d;
    cout << "Eigen SVD: " << size << "x" << size << " - " << TimeDiff(t1, t2) << endl;

    gettimeofday(&t1, NULL);
    for (int i = 0; i < ITERATIONS; i++)
    {
        C = A + B;
    }
    gettimeofday(&t2, NULL);
    d           = TimeDiff(t1, t2);
    results.add = d;
    cout << "Eigen addition: " << size << "x" << size << " - " <<
        TimeDiff(t1, t2) << endl;

    gettimeofday(&t1, NULL);
    for (int i = 0; i < ITERATIONS; i++)
    {
        C = A * B;
    }
    gettimeofday(&t2, NULL);
    d           = TimeDiff(t1, t2);
    results.mul = d;
    cout << "Eigen multiply: " << size << "x" << size << " - " <<
        TimeDiff(t1, t2) << endl;

    gettimeofday(&t1, NULL);
    for (int i = 0; i < ITERATIONS; i++)
    {
        C = A.inverse();
    }
    gettimeofday(&t2, NULL);
    d           = TimeDiff(t1, t2);
    results.inv = d;
    cout << "Eigen invert: " << size << "x" << size << " - " << TimeDiff(t1, t2) << endl;

    gettimeofday(&t1, NULL);
    for (int i = 0; i < ITERATIONS; i++)
    {
        C = A.transpose();
    }
    gettimeofday(&t2, NULL);
    d            = TimeDiff(t1, t2);
    results.tran = d;
    cout << "Eigen transpose: " << size << "x" << size << " - " <<
        TimeDiff(t1, t2) << endl;
}
