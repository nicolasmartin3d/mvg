/*
 * License Agreement for MVG
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <opencv2/core/core.hpp>
#if CV_MAJOR_VERSION >= 3
#include <opencv2/core/utility.hpp>
#endif
#include <iostream>
#include "../src/lsvd.h"

using namespace std;
using namespace cv;

int main(int   argc,
         char *argv[])
{
    (void)argc;
    (void)argv;

    Mat    A;
    LSVD   lsvd;
    SVD    svd;
    double time;

    for (int i = 3; i < 10; i++)
    {
        A.create(i, i, CV_64F);
        randu(A, 0, 1);
        time = static_cast<double>(getTickCount());
        for (int k = 0; k < 10; ++k)
        {
            svd(A);
        }
        time = 1000 * (static_cast<double>(getTickCount()) - time) / getTickFrequency();
        cout << "SVD size " << i << " took " << time << " ms" << endl;
        time = static_cast<double>(getTickCount());
        for (int k = 0; k < 10; ++k)
        {
            lsvd(A);
        }
        time = 1000 * (static_cast<double>(getTickCount()) - time) / getTickFrequency();
        cout << "LSVD size " << i << " took " << time << " ms" << endl;
    }

    for (int i = 10; i < 200; i += 10)
    {
        A.create(i, i, CV_64F);
        randu(A, 0, 1);
        time = static_cast<double>(getTickCount());
        for (int k = 0; k < 10; ++k)
        {
            svd(A);
        }
        time = 1000 * (static_cast<double>(getTickCount()) - time) / getTickFrequency();
        cout << "SVD size " << i << " took " << time << " ms" << endl;
        time = static_cast<double>(getTickCount());
        for (int k = 0; k < 10; ++k)
        {
            lsvd(A);
        }
        time = 1000 * (static_cast<double>(getTickCount()) - time) / getTickFrequency();
        cout << "LSVD size " << i << " took " << time << " ms" << endl;
    }
}
