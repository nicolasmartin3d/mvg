/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The file is inspired by :
 * the class QDebug from Qt Copyright (C) 2012 Nokia Corporation and/or its subsidiary(-ies).
 * the class ezATAProgressBar of Remik Ziemlinksi (C) 2011, 2012
 */

#include <iostream>
#include <stdio.h>
#include <string>
#include <stdexcept>
#include <opencv2/core/core.hpp>

#include "logger.h"

using namespace std;
using namespace cv;

Logger::VerbosityLevel Logger::verbosityThreshold = Logger::L_INFO;

Logger::~Logger()
{ }

const string & Logger::messageString(Logger::VerbosityLevel level)
{
    static const string messageStrings[] = {
        "", "(error)", "(warning)", "(info)", "(debug)"
    };

    return messageStrings[level];
}

struct StreamLogger::Private
{
    Private(Logger::VerbosityLevel level_,
            bool                   endline_,
            ostream               &stream_);
    ~Private();

    Logger::VerbosityLevel level;
    bool                   endline;
    ostream               &stream;
    bool                   space;
};

StreamLogger::Private::Private(Logger::VerbosityLevel level_,
                               bool                   endline_,
                               ostream               &stream_) :
    level(level_),
    endline(endline_),
    stream(stream_),
    space(true)
{ }

StreamLogger::Private::~Private()
{ }

StreamLogger::StreamLogger(VerbosityLevel level,
                           bool           header,
                           bool           endline,
                           ostream       &stream)
{
    m_privImpl = new Private(level, endline, stream);
    if (header) { (*this) << messageString(level).c_str(); }
}

StreamLogger::StreamLogger(const StreamLogger &l) :
    m_privImpl(new Private(*l.m_privImpl))
{ }

StreamLogger & StreamLogger::operator=(const StreamLogger &l)
{
    if (this != &l)
    {
        delete m_privImpl;
        m_privImpl = new Private(*l.m_privImpl);
    }

    return *this;
}

StreamLogger::StreamLogger(Logger::   VerbosityLevel,
                           const char *) :
    m_privImpl(0)
{ }

StreamLogger::~StreamLogger()
{
    if (m_privImpl->endline && (m_privImpl->level != L_ERROR) &&
        (m_privImpl->level <= verbosityThreshold))
    {
        m_privImpl->stream << std::endl;
    }
    delete m_privImpl;
}

StreamLogger & StreamLogger::space()
{
    m_privImpl->space = true;
    m_privImpl->stream << ' ';

    return *this;
}

StreamLogger & StreamLogger::noSpace()
{
    m_privImpl->space = false;

    return *this;
}

StreamLogger & StreamLogger::maybeSpace()
{
    if (m_privImpl->space) { m_privImpl->stream << ' '; }

    return *this;
}

Logger::VerbosityLevel StreamLogger::level() const
{
    return m_privImpl->level;
}

std::ostream & StreamLogger::stream() const
{
    return m_privImpl->stream;
}

bool StreamLogger::needsSpace() const
{
    return m_privImpl->space;
}

bool StreamLogger::needsEndline() const
{
    return m_privImpl->endline;
}

StreamLogger & StreamLogger::operator<<(bool b)
{
    if (m_privImpl->level <= verbosityThreshold)
    {
        m_privImpl->stream << (b ? "true" : "false");

        return maybeSpace();
    }

    return *this;
}

StreamLogger & StreamLogger::operator<<(const string &t)
{
    if (m_privImpl->level <= verbosityThreshold)
    {
        m_privImpl->stream << t;

        return maybeSpace();
    }

    return *this;
}

StreamLogger & StreamLogger::operator<<(StreamLogger & (*m)(StreamLogger &))
{
    return (*m)(*this);
}

StreamLogger & StreamLogger::operator<<(std::ostream & (*m)(std::ostream &))
{
    if (m_privImpl->level <= verbosityThreshold)
    {
        m_privImpl->stream << m;

        return maybeSpace();
    }

    return *this;
}

StreamLogger & space(StreamLogger &os)
{
    return os.space();
}

StreamLogger & nospace(StreamLogger &os)
{
    return os.noSpace();
}

static const int millisecondsInSeconds = 1000;
static const int millisecondsInMinutes = 60 * millisecondsInSeconds;
static const int millisecondsInHours   = 60 * millisecondsInMinutes;
static const int millisecondsInDays    = 24 * millisecondsInHours;

/*
 *    taken from libcommonc++ : TimeSpan
 */
static std::string millisecondsToString(double ms)
{
    double v   = std::min(std::max(ms, 0.0), static_cast<double>(INT_MAX));
    int    tmp = cvRound(v);

    int days = tmp / millisecondsInDays;
    tmp = tmp % millisecondsInDays;

    int hours = tmp / millisecondsInHours;
    tmp = tmp % millisecondsInHours;

    int mins = tmp / millisecondsInMinutes;
    tmp = tmp % millisecondsInMinutes;

    int secs = tmp / millisecondsInSeconds;

    char        buf[8];
    std::string out;

    if (days)
    {
        sprintf(buf, "%dd ", days);
        out += buf;
    }

    if (hours >= 1)
    {
        sprintf(buf, "%dh ", hours);
        out += buf;
    }

    if (mins >= 1)
    {
        sprintf(buf, "%dm ", mins);
        out += buf;
    }

    if (secs >= 1)
    {
        sprintf(buf, "%ds", secs);
        out += buf;
    }

    if (out.empty())
    {
        out = "0s";
    }

    return out;
}

static void showProgressBar(size_t   maxWidth,
                            double   pct,
                            double   dt,
                            double   eta,
                            ostream &stream)
{
    char pctstr[5];
    sprintf(pctstr, "%3d%%", static_cast<int>(100 * pct));

    // Compute how many tics we can display.
    size_t nticsMax = std::max(maxWidth - 27, static_cast<size_t>(0));
    size_t ntics    = static_cast<size_t>(nticsMax * pct);

    string out(pctstr);
    out.append(" [");
    out.append(ntics, '#');
    out.append(nticsMax - ntics, ' ');
    out.append("] ");
    out.append((pct < 1.0) ? "ETA " : "in ");

    if (pct >= 1.0)
    {
        string tstr = millisecondsToString(dt);
        out.append(tstr);

        // Pad end with spaces to overwrite previous string that may have been longer.
        if (out.size() < maxWidth)
        {
            out.append(maxWidth - out.size(), ' ');
        }

        out.append("\n");
    }
    else
    {
        if (eta > 7 * millisecondsInDays)
        {
            out.append("> 1 week");
        }
        else
        {
            string tstr = millisecondsToString(eta);
            out.append(tstr);
        }

        // Pad end with spaces to overwrite previous string that may have been longer.
        if (out.size() < maxWidth)
        {
            out.append(maxWidth - out.size(), ' ');
        }

        out.append("\r");
    }

    stream << out;
    stream.flush();
}

struct ProgressLogger::Private
{
    Private(ostream &stream_=cout);
    ~Private();

    static const int maxWidth = 80;
    int              currentValue;
    unsigned int     maxValue;
    double           startTime;
    double           lastTime;
    ostream         &stream;
    double           lastPct, lastEta;
    Mutex            mutex;
};

ProgressLogger::Private::Private(ostream &stream_) :
    stream(stream_)
{ }

ProgressLogger::Private::~Private()
{ }

ProgressLogger::ProgressLogger(unsigned int maxValue,
                               bool         newline,
                               ostream     &stream)
{
    m_privImpl = new Private(stream);
    setMaxValue(maxValue);
    if (newline) { stream << endl; }
    showProgressBar(m_privImpl->maxWidth, 0., 10E10, 10E10, stream);
}

ProgressLogger::ProgressLogger(const ProgressLogger &l) :
    m_privImpl(new Private(*l.m_privImpl))
{ }

ProgressLogger & ProgressLogger::operator=(const ProgressLogger &l)
{
    if (this != &l)
    {
        delete m_privImpl;
        m_privImpl = new Private(*l.m_privImpl);
    }

    return *this;
}

ProgressLogger::~ProgressLogger()
{
    if (m_privImpl->currentValue != static_cast<int>(m_privImpl->maxValue))
    {
        m_privImpl->currentValue = static_cast<int>(m_privImpl->maxValue);
        m_privImpl->lastTime     = static_cast<double>(getTickCount());
        double pct = m_privImpl->currentValue / static_cast<double>(m_privImpl->maxValue);
        double dt  =
            (1000 * (m_privImpl->lastTime - m_privImpl->startTime) / getTickFrequency());
        double eta = dt * (1.0 - pct) / pct;
        showProgressBar(m_privImpl->maxWidth, pct, dt, eta, m_privImpl->stream);
    }
    delete m_privImpl;
}

void ProgressLogger::setMaxValue(unsigned int maxValue_)
{
    m_privImpl->maxValue     = maxValue_;
    m_privImpl->currentValue = 0;
    m_privImpl->startTime    = static_cast<double>(getTickCount());
    m_privImpl->lastTime     = m_privImpl->startTime;
    m_privImpl->lastPct      = 0;
}

// One-line refreshing progress bar inspired by wget that shows ETA (time remaining).
// 90% [##################################################     ] ETA 12d 23h 56s
ProgressLogger & ProgressLogger::operator++()
{
    // atomic swap and add
    int currentValue = CV_XADD(&m_privImpl->currentValue, 1) + 1;
    if (currentValue > static_cast<int>(m_privImpl->maxValue))
    {
        currentValue = static_cast<int>(m_privImpl->maxValue);
    }

    double lastTime = static_cast<double>(getTickCount());
    double dt       = (1000 * (lastTime - m_privImpl->startTime) / getTickFrequency());

    if (currentValue == static_cast<int>(m_privImpl->maxValue))
    {
        AutoLock lock(m_privImpl->mutex);
        showProgressBar(m_privImpl->maxWidth, 1.0, dt, 0., m_privImpl->stream);

        return *this;
    }

    double pct = currentValue / static_cast<double>(m_privImpl->maxValue);
    if (fabs(pct - m_privImpl->lastPct) > 1E-2)
    {
        double   eta = dt * (1.0 - pct) / pct;
        AutoLock lock(m_privImpl->mutex);
        showProgressBar(m_privImpl->maxWidth, pct, dt, eta, m_privImpl->stream);
        m_privImpl->lastTime = lastTime;
        m_privImpl->lastPct  = pct;
        m_privImpl->lastEta  = eta;

        return *this;
    }

    return *this;
}

ProgressLogger & ProgressLogger::operator++(int)
{
    this->operator++();

    return *this;
}

const char * LoggerException::what() const throw()
{
    return "";
}

struct ErrorLogger::Private
{
    Private(int           line_,
            const string &file_,
            const string &function_);
    ~Private();

    int    line;
    string file;
    string function;
};

ErrorLogger::Private::Private(int           line_,
                              const string &file_,
                              const string &function_) :
    line(line_),
    file(file_),
    function(function_)
{ }

ErrorLogger::Private::~Private()
{ }

ErrorLogger::ErrorLogger(int           line,
                         const string &file,
                         const string &function,
                         bool          header,
                         bool          endline,
                         ostream      &stream) :
    StreamLogger(Logger::L_ERROR, header, endline, stream)
{
    m_privImpl = new Private(line, file, function);
    (*this) << "in file " << file
            << " at line " << line << " "
            << (function.empty() ? "" : "in function ")
            << (function.empty() ? "" : function)
            << " with message [";
}

ErrorLogger::ErrorLogger(const ErrorLogger &l) :
    StreamLogger(Logger::L_ERROR),
    m_privImpl(new Private(*l.m_privImpl))
{ }

ErrorLogger & ErrorLogger::operator=(const ErrorLogger &l)
{
    if (this != &l)
    {
        delete m_privImpl;
        m_privImpl = new Private(*l.m_privImpl);
    }

    return *this;
}

ErrorLogger::ErrorLogger(int,
                         const string &,
                         const string &,
                         const char   *) :
    StreamLogger(Logger::L_ERROR)
{ }

#if defined(_MSC_VER) && _MSC_VER >= 1400
#pragma warning(push)
#pragma warning(disable:4722)
#endif
ErrorLogger::~ErrorLogger()
{
    (*this) << "]" << endl;
    // exit(1);
    throw LoggerException();
}
#if defined(_MSC_VER) && _MSC_VER >= 1400
#pragma warning(pop)
#endif
