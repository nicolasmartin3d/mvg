/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONCURRENCY_H
#define CONCURRENCY_H

#include <OpenThreads/Mutex>
#include <OpenThreads/Condition>
#include <list>
#include <queue>
#include <stack>
#include "export.h"

struct MVG_DECLSPEC ParallelForBody
{
    virtual ~ParallelForBody();
    virtual void operator()(size_t start,
                            size_t end) const = 0;
};

// An implementation of parallel_for that only depends on OpenThread
MVG_DECLSPEC void parallel_for(size_t                 start,
                               size_t                 end,
                               const ParallelForBody &body);

template <class T, class Container, class Adapter>
struct AdapterIsNotFull;
template <class T, class Container, class Adapter>
struct AdapterIsNotEmpty;

template <class T, class Container = std::list<T>,
          class Adapter            = std::queue<T, Container> >
class ConcurrentAdapter
{

    typedef typename Container::value_type value_type;
    typedef typename Container::size_type  size_type;
    typedef Container                      container_type;

    typedef OpenThreads::Mutex mutex_type;

    public:

        ConcurrentAdapter();
        ConcurrentAdapter(const ConcurrentAdapter &sq);
        ~ConcurrentAdapter();
        ConcurrentAdapter & operator=(const ConcurrentAdapter &sq);

        unsigned int capacity() const;
        void set_capacity(unsigned int capacity);

        void push(const value_type &item);
        bool try_push(const value_type &item);
        // timeout in us
        bool timeout_push(const value_type &item,
                          unsigned long int timeout);

        void pop(value_type &item);
        bool try_pop(value_type &item);
        // timeout in us
        bool timeout_pop(value_type       &item,
                         unsigned long int timeout);

        size_type size() const;
        void clear();
        bool empty() const;
        bool full() const;

        void swap(ConcurrentAdapter &sq);

    private:
        Adapter                    m_queue;
        mutable OpenThreads::Mutex m_mutex;
        OpenThreads::Condition     m_conditionFull, m_conditionEmpty;
        unsigned int               m_capacity;

        friend struct AdapterIsNotFull<T, Container, Adapter>;
        friend struct AdapterIsNotEmpty<T, Container, Adapter>;
};

template <class T, class Container = std::list<T> > class ConcurrentQueue :
    public ConcurrentAdapter<T, Container, std::queue<T, Container> >
{ };
template <class T, class Container = std::list<T> > class ConcurrentStack :
    public ConcurrentAdapter<T, Container, std::stack<T, Container> >
{ };

#include "concurrency.tpp"

#endif // CONCURRENCY_H

