/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MODEL_EST_H_
#define MODEL_EST_H_

#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include "export.h"

// from OpenCV
class MVG_DECLSPEC ModelEstimator
{
    public:
        ModelEstimator(int      modelPoints,
                       cv::Size modelSize,
                       int      maxBasicSolutions);
        virtual ~ModelEstimator();

        // a Kernel that can be used to estimate a Least Square model
        // returns nothing as there can only be one LS model
        virtual int runLSKernel(const cv::Mat &m1,
                                const cv::Mat &m2,
                                cv::Mat       &model) = 0;

        virtual int runKernel(const cv::Mat &m1,
                              const cv::Mat &m2,
                              cv::Mat       &model) = 0;

        virtual bool runLMeDS(const cv::Mat     &m1,
                              const cv::Mat     &m2,
                              cv::Mat           &model,
                              std::vector<bool> &mask,
                              double             confidence=0.99,
                              int                maxIters=200);

        virtual bool runRANSAC(const cv::Mat     &m1,
                               const cv::Mat     &m2,
                               cv::Mat           &model,
                               std::vector<bool> &mask,
                               double             threshold=3,
                               double             confidence=0.99,
                               int                maxIters=200);

        virtual bool runOptimization(const cv::Mat     &m1,
                                     const cv::Mat     &m2,
                                     cv::Mat           &model,
                                     std::vector<bool> &mask) = 0;

        virtual void setSeed(int64 seed);

    protected:
        virtual void computeReprojError(const cv::Mat       &m1,
                                        const cv::Mat       &m2,
                                        const cv::Mat       &model,
                                        std::vector<double> &errors) = 0;

        virtual int findInliers(const cv::Mat       &m1,
                                const cv::Mat       &m2,
                                const cv::Mat       &model,
                                std::vector<double> &errors,
                                std::vector<bool>   &mask,
                                double               threshold);

        virtual bool getSubset(const cv::Mat &m1,
                               const cv::Mat &m2,
                               cv::Mat       &ms1,
                               cv::Mat       &ms2,
                               int            maxAttempts=1000);

        virtual bool checkSubset(const cv::Mat &ms,
                                 int            count);

        virtual int compressPoints(cv::Mat                 &m1,
                                   cv::Mat                 &m2,
                                   const std::vector<bool> &mask);

        bool optimize() const;
        void setOptimize(bool optimize);

        cv::RNG& rng();
        int modelPoints() const;
        int maxBasicSolutions() const;
        cv::Size modelSize() const;

    private:
        struct Private;
        Private *m_privImpl;

        ModelEstimator(const ModelEstimator &) { }
        void operator=(const ModelEstimator &) { }
};

#endif // MODEL_EST_H_

