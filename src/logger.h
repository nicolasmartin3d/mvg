/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOGGER_H
#define LOGGER_H

#include <iostream>
#include <sstream>
#include <string>
#include <opencv2/core/core.hpp>
#if CV_MAJOR_VERSION >= 3
#include <opencv2/core/utility.hpp>
#endif
#include "export.h"

class MVG_DECLSPEC Logger
{
    public:
        virtual ~Logger();
        enum VerbosityLevel {
            L_NONE,
            L_ERROR,
            L_WARNING,
            L_INFO,
            L_DEBUG
        };
        static VerbosityLevel verbosityThreshold;

    protected:
        static const std::string & messageString(VerbosityLevel level);
};

class MVG_DECLSPEC StreamLogger : public Logger
{
    public:
        StreamLogger(VerbosityLevel level,
                     bool           header=true,
                     bool           endline=true,
                     std::ostream  &stream=std::cout);
        StreamLogger(const StreamLogger &);
        StreamLogger & operator=(const StreamLogger &);
        virtual ~StreamLogger();

        StreamLogger & space();
        StreamLogger & noSpace();
        StreamLogger & maybeSpace();

        template <class T>
        StreamLogger & operator<<(const T &t)
        {
            if (level() <= verbosityThreshold)
            {
                stream() << t;

                return maybeSpace();
            }

            return *this;
        }

        template <class T>
        StreamLogger & operator<<(const std::vector<T> &v)
        {
            if (level() <= verbosityThreshold)
            {
                stream() << "[ ";
                for (size_t i = 0; i < v.size() - 1; ++i)
                {
                    stream() << v[i] << ", ";
                }
                stream() << v[v.size() - 1];
                stream() << " ]";

                return maybeSpace();
            }

            return *this;
        }

        StreamLogger & operator<<(bool b);
        StreamLogger & operator<<(const std::string &t);
        StreamLogger & operator<<(StreamLogger & (*m)(StreamLogger &));
        StreamLogger & operator<<(std::ostream & (*m)(std::ostream &));

    protected:
        VerbosityLevel level() const;
        std::ostream & stream() const;
        bool needsSpace() const;
        bool needsEndline() const;

    private:
        struct Private;
        Private *m_privImpl;

        // prevents some nasty errors when using logInfo("some std::string") or logDebug("some other
        // std::string")
        StreamLogger(VerbosityLevel level,
                     const char    *dummy);
};

MVG_DECLSPEC StreamLogger & space(StreamLogger &os);
MVG_DECLSPEC StreamLogger & nospace(StreamLogger &os);

class MVG_DECLSPEC ProgressLogger
{
    public:
        ProgressLogger(unsigned int  maxValue=100,
                       bool          newline=false,
                       std::ostream &stream=std::cout);
        ProgressLogger(const ProgressLogger &);
        ProgressLogger & operator=(const ProgressLogger &);
        ~ProgressLogger();

        void setMaxValue(unsigned int maxValue_);
        ProgressLogger & operator++();
        ProgressLogger & operator++(int);

    private:
        struct Private;
        Private *m_privImpl;
};

struct MVG_DECLSPEC LoggerException : public std::exception
{
    virtual const char * what() const throw();
};

class MVG_DECLSPEC ErrorLogger : public StreamLogger
{
    public:
        ErrorLogger(int                line,
                    const std::string &file,
                    const std::string &function,
                    bool               header=true,
                    bool               endline=true,
                    std::ostream      &stream=std::cout);
        ErrorLogger(const ErrorLogger &);
        ErrorLogger & operator=(const ErrorLogger &);

        ~ErrorLogger();

    private:
        struct Private;
        Private *m_privImpl;

        // prevents some nasty errors when using logError("some std::string");
        ErrorLogger(int                line,
                    const std::string &file,
                    const std::string &function,
                    const char        *dummy);
};

#if defined DEBUG || !defined NDEBUG
#define logDebug(...) if (Logger::L_DEBUG > Logger::verbosityThreshold) { } \
    else StreamLogger(Logger::L_DEBUG, ## __VA_ARGS__)
#else
#define logDebug(...) if (true) { } \
    else StreamLogger(Logger::L_DEBUG, ## __VA_ARGS__)
#endif
#define logInfo(...) if (Logger::L_INFO > Logger::verbosityThreshold) { } \
    else StreamLogger(Logger::L_INFO, ## __VA_ARGS__)
#define logWarning(...) if (Logger::L_WARNING > Logger::verbosityThreshold) { } \
    else StreamLogger(Logger::L_WARNING, ## __VA_ARGS__)
#define logProgress ProgressLogger

#ifdef __GNUC__
#define logError(...) ErrorLogger(__LINE__, __FILE__, __func__, ## __VA_ARGS__)
#else
#define logError(...) ErrorLogger(__LINE__, __FILE__, "", ## __VA_ARGS__)
#endif

#endif // LOGGER_H
