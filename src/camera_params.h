/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CAMERA_PARAMETERS_H
#define CAMERA_PARAMETERS_H

#include <opencv2/core/core.hpp>
#include <iostream>

#include "export.h"

class MVG_DECLSPEC CameraGeometricParameters
{
    public:
        CameraGeometricParameters();
        CameraGeometricParameters(const CameraGeometricParameters &);
        CameraGeometricParameters & operator=(const CameraGeometricParameters &);

        CameraGeometricParameters(const cv::Mat &M,
                                  const cv::Mat &coeffs,
                                  cv::Size       imageSize);

        CameraGeometricParameters(const cv::Mat &K,
                                  const cv::Mat &R,
                                  const cv::Mat &t,
                                  const cv::Mat &coeffs,
                                  cv::Size       imageSize);

        ~CameraGeometricParameters();

        const cv::Mat & K() const;
        void setIntrinsicMatrix(const cv::Mat &K);

        const cv::Mat & R() const;
        void setRotationMatrix(const cv::Mat &R);

        const cv::Mat & t() const;
        void setTranslationVector(const cv::Mat &t);

        void setExtrinsicMatrix(const cv::Mat &Rt);

        const cv::Mat & M() const;
        void setMatrix(const cv::Mat &M);

        cv::Size imageSize() const;
        void setImageSize(const cv::Size &imageSize);

        double k1();
        double k2();
        double p1();
        double p2();
        const cv::Mat & distorsionCoefficients() const;
        void setDistorsionCoefficients(const cv::Mat &coeffs);

        bool read(const cv::FileStorage &fs);
        bool read(const std::string &path);

        bool write(cv::FileStorage &fs);
        bool write(const std::string &path);

        bool empty() const;

    private:
        struct Private;
        Private *m_privImpl;
};
MVG_DECLSPEC std::ostream & operator<<(std::ostream                    &os,
                                       const CameraGeometricParameters &c);

#endif

