/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <limits>
#include <ceres/ceres.h>

#include "lsvd.h"
#include "similarity.h"
#include "mvg.h"

using namespace std;
using namespace cv;
using namespace ceres;

struct SimilarityEstimator::Private
{
    Private(bool rigid_);
    ~Private();

    bool rigid;
};

SimilarityEstimator::Private::Private(bool rigid_) :
    rigid(rigid_)
{ }

SimilarityEstimator::Private::~Private()
{ }

/*
 * This file is largely inspired by OpenCV's version of algorithms for
 * fundamental estimation
 */

SimilarityEstimator::SimilarityEstimator(int  modelPoints,
                                         bool rigid) :
    // size is flipped because it is width, height not rows, cols
    ModelEstimator(modelPoints, cv::Size(4, 3), 1)
{
    assert(modelPoints == 3);
    m_privImpl = new Private(rigid);
}

SimilarityEstimator::~SimilarityEstimator()
{
    delete m_privImpl;
}

void SimilarityEstimator::initialize(const Mat & /*m1*/,
                                     const Mat & /*m2*/)
{ }

void SimilarityEstimator::initialize(const Mat & /*m1*/,
                                     Size /*size*/)
{ }

/*virtual*/
int SimilarityEstimator::runLSKernel(const Mat &m1,
                                     const Mat &m2,
                                     Mat       &model)
{
    const Point3f *p1 = m1.ptr<Point3f>();
    const Point3f *p2 = m2.ptr<Point3f>();
    assert((m1.cols == 1 || m1.rows == 1) &&
           (m1.cols * m1.rows == m2.rows * m2.cols));

    int count = m1.rows * m1.cols;
    // int count = m1.rows;

    Point3f c1(0, 0, 0), c2(0, 0, 0);
    for (int i = 0; i < count; ++i)
    {
        c1 += p1[i];
        c2 += p2[i];
    }
    c1.x /= count; c1.y /= count; c1.z /= count;
    c2.x /= count; c2.y /= count; c2.z /= count;

    Mat      v1(1, count, CV_64FC3);
    Mat      v2(1, count, CV_64FC3);
    Point3d *pv1 = v1.ptr<Point3d>();
    Point3d *pv2 = v2.ptr<Point3d>();
    for (int i = 0; i < count; ++i)
    {
        pv1[i] = p1[i] - c1;
        pv2[i] = p2[i] - c2;
    }

    double *pd1 = v1.ptr<double>();
    double *pd2 = v2.ptr<double>();
    Mat     H(3, 3, CV_64F, Scalar::all(0));
    for (int i = 0; i < count; ++i)
    {
        H += Mat(3, 1, CV_64F, pd1 + 3 * i) * Mat(1, 3, CV_64F, pd2 + 3 * i);
    }

    Mat U, W, Vt;
    SVD::compute(H, W, U, Vt, SVD::MODIFY_A);

    double _W2[3 * 3];
    Mat    W2(3, 3, CV_64F, _W2);
    W2             = Scalar::all(0);
    _W2[0 * 3 + 0] = 1;
    _W2[1 * 3 + 1] = 1;
    _W2[2 * 3 + 2] = determinant(U * Vt);
    Mat R = Vt.t() * W2 * U.t();

    double s;
    if (!m_privImpl->rigid)
    {
        double _v;
        Mat    v(1, 1, CV_64F, &_v);
        double n1 = 0, n2 = 0;
        for (int i = 0; i < count; ++i)
        {
            v   = Mat(1, 3, CV_64F, pd2 + 3 * i) * R * Mat(3, 1, CV_64F, pd1 + 3 * i);
            n1 += _v;
            n2 += pd1[3 * i + 0] * pd1[3 * i +  0] +
                  pd1[3 * i + 1] * pd1[3 * i + 1] + pd1[3 * i + 2] * pd1[3 * i + 2];
        }
        s = n1 / n2;
    }
    else { s = 1.; }

    Mat Rs = R * s;
    // Mat t = Mat(3, 1, CV_64F, _c2)-Rs*Mat(3, 1, CV_64F, _c1);
    Mat t = (Mat_<double>(3, 1) << c2.x, c2.y, c2.z) -
            Rs * (Mat_<double>(3, 1) << c1.x, c1.y, c1.z);

    // cout << R << " " << determinant(R) << endl;
    // cout << t << endl;
    // cout << s << endl;

    model.create(3, 4, CV_64F);
    Mat mA3x3 = model(Range(0, 3), Range(0, 3));
    Mat mA3x1 = model(Range(0, 3), Range(3, 4));
    R.copyTo(mA3x3);
    model *= s;
    t.copyTo(mA3x1);

    return 1;
}

/*virtual*/
int SimilarityEstimator::runKernel(const Mat &m1,
                                   const Mat &m2,
                                   Mat       &model)
{
    return runLSKernel(m1, m2, model);
}

/*virtual*/
void SimilarityEstimator::computeReprojError(const Mat      &m1,
                                             const Mat      &m2,
                                             const Mat       & /*model*/,
                                             vector<double> &errors)
{
    size_t count = static_cast<size_t>(m1.rows * m1.cols);
    // int count = m1.rows;
    const Point3f *p1 = m1.ptr<Point3f>();
    const Point3f *p2 = m2.ptr<Point3f>();

    for (size_t i = 0; i < count; i++)
    {
        // errors[i] = squaredDistanceToCameraProjection(M, p1[i], p2[i]);
        double n = norm(p1[i] - p2[i]);
        errors[i] = n * n;
        // cout << errors[i] << " ";
    }
    // cout << endl;

    /*
     *   int count = m1.rows*m1.cols;
     *   //int count = m1.rows;
     *   const Point3f* from = m1.ptr<Point3f>();
     *   const Point3f* to   = m2.ptr<Point3f>();
     *   //const Point3d* from = reinterpret_cast<const Point3d*>(m1->data.ptr);
     *   //const Point3d* to   = reinterpret_cast<const Point3d*>(m2->data.ptr);
     *   //const double* F = model.data.db;
     *   const double* F = model.ptr<double>();
     *   //float* err = error.data.fl;
     *
     *   for(int i = 0; i < count; i++ )
     *   {
     *    const Point3f& f = from[i];
     *    const Point3f& t = to[i];
     *
     *    double a = F[0]*f.x + F[1]*f.y + F[ 2]*f.z + F[ 3] - t.x;
     *    double b = F[4]*f.x + F[5]*f.y + F[ 6]*f.z + F[ 7] - t.y;
     *    double c = F[8]*f.x + F[9]*f.y + F[10]*f.z + F[11] - t.z;
     *
     *    errors[i] = (double)std::sqrt(a*a + b*b + c*c);
     *   }
     */
}

int SimilarityEstimator::compressPoints(Mat                &m1,
                                        Mat                &m2,
                                        const vector<bool> &mask)
{
    assert((m1.cols == 1 || m1.rows == 1) &&
           (m1.cols * m1.rows == m2.rows * m2.cols));

    Point3f *m1ptr = m1.ptr<Point3f>();
    Point3f *m2ptr = m2.ptr<Point3f>();
    size_t   count = static_cast<size_t>(m1.cols * m1.rows);
    // int count = m1.rows;

    int j = 0;
    for (size_t i = 0; i < count; ++i)
    {
        if (mask[i])
        {
            m1ptr[j] = m1ptr[i];
            m2ptr[j] = m2ptr[i];
            ++j;
        }
    }

    if (m1.cols == 1)
    {
        m1.rows = m2.rows = j;
    }
    else
    {
        m1.cols = m2.cols = j;
    }

    return j;
}

bool SimilarityEstimator::getSubset(const Mat &m1,
                                    const Mat &m2,
                                    Mat       &ms1,
                                    Mat       &ms2,
                                    int        maxAttempts)
{
    vector<int>    idx(static_cast<size_t>(modelPoints()));
    const Point3f *m1ptr  = m1.ptr<Point3f>();
    const Point3f *m2ptr  = m2.ptr<Point3f>();
    Point3f       *ms1ptr = ms1.ptr<Point3f>();
    Point3f       *ms2ptr = ms2.ptr<Point3f>();
    size_t         count  = static_cast<size_t>(m1.cols * m1.rows);
    // int count = m1.rows;

    size_t i;
    int    iters;
    // assert( CV_IS_MAT_CONT(m1->type & m2->type) && (elemSize % sizeof(int) == 0) );
    for (iters = 0, i = 0; i < static_cast<size_t>(modelPoints()) && iters < maxAttempts;)
    {
        idx[i] = static_cast<int>(rng() (static_cast<unsigned int>(count)));

        vector<int>::iterator idxE = idx.begin() + static_cast<int>(i);
        if (find(idx.begin(), idxE, idx[i]) != idxE) { continue; }

        ms1ptr[i] = m1ptr[idx[i]];
        ms2ptr[i] = m2ptr[idx[i]];

        if (!check3DSubset(ms1, static_cast<int>(i + 1)) ||
            !check3DSubset(ms2, static_cast<int>(i + 1)))
        {
            iters++;
            continue;
        }

        i++;
    }

    return i == static_cast<size_t>(modelPoints()) && iters < maxAttempts;
}

bool SimilarityEstimator::check3DSubset(const Mat &ms,
                                        int        count)
{
    // CV_Assert( CV_MAT_TYPE(ms1->type) == CV_64FC3 );

    int j, k, i = count - 1;
    // const Point3d* ptr = reinterpret_cast<const Point3d*>(ms1->data.ptr);
    const Point3f *ptr = ms.ptr<Point3f>();

    // check that the i-th selected point does not belong
    // to a line connecting some previously selected points

    for (j = 0; j < i; ++j)
    {
        // Point3d d1 = ptr[j] - ptr[i];
        Point3f d1 = ptr[j] - ptr[i];
        double  n1 = norm(d1);

        for (k = 0; k < j; ++k)
        {
            // Point3d d2 = ptr[k] - ptr[i];
            Point3f d2 = ptr[k] - ptr[i];
            double  n  = norm(d2) * n1;

            if (fabs(d1.dot(d2) / n) > 0.996)
            {
                break;
            }
        }
        if (k < j)
        {
            break;
        }
    }

    return j == i;
}

int findSimilarityTransform(const Mat    &points1,
                            const Mat    &points2,
                            Mat          &mmatrix,
                            vector<bool> *mask,
                            int           method,
                            double        param1,
                            double        param2,
                            Size          size)
{
    int    result = 0;
    size_t count;

    double _M[3 * 4];
    Mat    M(3, 4, CV_64F, _M);

    Mat m1 = points1.clone(), m2 = points2.clone();
    assert(m1.rows * m1.cols == m2.rows * m2.cols);

    count = static_cast<size_t>(MAX(m1.cols, m1.rows));

    if (count < 3)
    {
        return 0;
    }

    vector<bool>        tempMask(count, true);
    SimilarityEstimator estimator(3);

    if (size != Size()) { estimator.initialize(points1, size); }
    else { estimator.initialize(points1, points2); }

    if (count == 3)
    {
        result = estimator.runKernel(m1, m2, M);
    }
    else
    {
        if (param1 <= 0)
        {
            param1 = 3;
        }
        if ((param2 < DBL_EPSILON) || (param2 > 1 - DBL_EPSILON))
        {
            param2 = 0.99;
        }

        if (((method & ~3) == ST_LMEDS) && (count >= 2 * 3 + 1))
        {
            result = estimator.runLMeDS(m1, m2, M, tempMask, param2);
        }
        else
        {
            result = estimator.runRANSAC(m1, m2, M, tempMask, param1, param2);
        }

        if (result <= 0)
        {
            return 0;
        }
    }

    if (result) { M.copyTo(mmatrix); }

    if (mask)
    {
        mask->resize(count);
        copy(tempMask.begin(), tempMask.end(), mask->begin());
    }

    return result;
}

int findRigidTransform(const Mat    &points1,
                       const Mat    &points2,
                       Mat          &mmatrix,
                       vector<bool> *mask,
                       int           method,
                       double        param1,
                       double        param2,
                       Size          size)
{
    int    result = 0;
    size_t count;

    double _M[3 * 4];
    Mat    M(3, 4, CV_64F, _M);

    Mat m1 = points1.clone(), m2 = points2.clone();
    assert(m1.rows * m1.cols == m2.rows * m2.cols);

    count = static_cast<size_t>(MAX(m1.cols, m1.rows));

    if (count < 3)
    {
        return 0;
    }

    vector<bool>        tempMask(count, true);
    SimilarityEstimator estimator(3, true);

    if (size != Size()) { estimator.initialize(points1, size); }
    else { estimator.initialize(points1, points2); }

    if (count == 3)
    {
        result = estimator.runKernel(m1, m2, M);
    }
    else
    {
        if (param1 <= 0)
        {
            param1 = 3;
        }
        if ((param2 < DBL_EPSILON) || (param2 > 1 - DBL_EPSILON))
        {
            param2 = 0.99;
        }

        if (((method & ~3) == ST_LMEDS) && (count >= 2 * 3 + 1))
        {
            result = estimator.runLMeDS(m1, m2, M, tempMask, param2);
        }
        else
        {
            result = estimator.runRANSAC(m1, m2, M, tempMask, param1, param2);
        }

        if (result <= 0)
        {
            return 0;
        }
    }

    if (result) { M.copyTo(mmatrix); }

    if (mask)
    {
        mask->resize(count);
        copy(tempMask.begin(), tempMask.end(), mask->begin());
    }

    return result;
}

struct SimilarityNonLinearEstimator
{
    SimilarityNonLinearEstimator(double x_,
                                 double y_) : x(x_),
        y(y_) { }

    template <typename T> bool operator()(const T *const M,
                                          const T *const P,
                                          T             *residuals) const
    {

        T p[2] = { T(x), T(y) };

        residuals[0] = ceres::sqrt(similarityGeometricDistance(M, P, p));

        return true;
    }

    double x, y;
};

/*virtual*/
bool SimilarityEstimator::runOptimization(const Mat    & /*m1*/,
                                          const Mat    & /*m2*/,
                                          Mat          & /*model*/,
                                          vector<bool> & /*mask*/)
{
#if 0
    int    count = m1.rows * m1.cols;
    double sx0   = T1.at<double>(0, 0), ox0 = T1.at<double>(0, 3),
           sy0   = T1.at<double>(1, 1), oy0 = T1.at<double>(1, 3),
           sz0   = T1.at<double>(2, 2), oz0 = T1.at<double>(2, 3);
    double sx1   = T2.at<double>(0, 0), ox1 = T2.at<double>(0, 2),
           sy1   = T2.at<double>(1, 1), oy1 = T2.at<double>(1, 2);

    double paramsP[3 * 4];
    Mat    P(3, 4, CV_64F, paramsP);
    P = T2 * model * T1.inv();

    Mat            mn1(count, 1, CV_64FC3), mn2(count, 1, CV_64FC2);
    const Point3d *p1      = m1.ptr<Point3d>();
    const Point2d *p2      = m2.ptr<Point2d>();
    double        *paramsX = mn1.ptr<double>();
    Point2d       *pn2     = mn2.ptr<Point2d>();

    Problem problem;
    for (int i = 0; i < count; ++i)
    {
        if (mask[i])
        {
            paramsX[i * 3]     = p1[i].x * sx0 + ox0;
            paramsX[i * 3 + 1] = p1[i].y * sy0 + oy0;
            paramsX[i * 3 + 2] = p1[i].z * sz0 + oz0;

            pn2[i].x = p2[i].x * sx1 + ox1;
            pn2[i].y = p2[i].y * sy1 + oy1;

            CostFunction *cost_function = new AutoDiffCostFunction<
                SimilarityNonLinearEstimator, 1, 12, 3>(
                new SimilarityNonLinearEstimator(pn2[i].x, pn2[i].y));

            /*
             *   Mat tX = (Mat_<double>(4,1) << paramsX[i*3+0],
             *        paramsX[i*3+1], paramsX[i*3+2], 1);
             *   Mat tp0 = M1*tX; tp0 /= tp0.at<double>(2,0);
             *   Mat tp1 = M2*tX; tp1 /= tp1.at<double>(2,0);
             *   Mat tp = (Mat_<double>(2,1) << pn1[i].x, pn1[i].y);
             *   Mat tq = (Mat_<double>(2,1) << pn2[i].x, pn2[i].y);
             *   cout << tX << tp0 << tp << tp1 << tq << endl;
             */

            problem.AddResidualBlock(cost_function, new HuberLoss(1),
                                     paramsP, paramsX + i * 3);
        }
    }

    Solver::Options options;
    options.linear_solver_type = ceres::SPARSE_SCHUR;
    options.ordering_type      = ceres::SCHUR;
    // options.minimizer_progress_to_stdout = true;
    // options.num_linear_solver_threads = 2;
    // options.num_threads = 2;

    Solver::Summary summary;
    Solve(options, &problem, &summary);
    cout << summary.FullReport() << "\n";

    model = T2.inv() * P * T1;

    return true;

#endif

    return true;
}

/* End of file. */

