/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MESH_H
#define MESH_H

#include <set>
#include <vector>
#include <opencv2/core/core.hpp>
#include "export.h"

class MVG_DECLSPEC Mesh
{
    public:
        Mesh();
        Mesh(const Mesh &);
        Mesh & operator=(const Mesh &);
        ~Mesh();

        typedef cv::Vec<unsigned int, 3> Vec3u;
        void computeNormals(bool recompute=false);
        void reverseNormals();

        // in the following, properties associated to a vertex correspond
        // to color, normal and texture coordinates.

        // clips the mesh to the bounds specified
        // every vertex (and associated props) that is outside
        // the bounds will be removed
        // any face containing at least one clipped vertex will be removed
        void clip(double xmin,
                  double ymin,
                  double zmin,
                  double xmax,
                  double ymax,
                  double zmax);

        // removes vertices that do not belong to any face
        // every vertex (and associated props) will be removed
        void removeUnusedVertices();

        // removes every vertex (and associated props) whose id is present
        // in the given set
        void removeVertices(const std::set<int> &ids);

        bool write(const std::string &path,
                   bool               ascii=false) const;
        bool read(const std::string &path);

    private:
        void remapIndices(const std::vector<int> &indices);

        bool writeAscii(FILE *f) const;
        bool readAscii(FILE *f);
        bool writeBinary(FILE *f) const;
        bool readBinary(FILE *f);

    public:
        std::vector<Vec3u> & triangles();
        const std::vector<Vec3u> & triangles() const;

        std::vector<cv::Vec2d> & texCoords();
        const std::vector<cv::Vec2d> & texCoords() const;

        std::vector<cv::Vec3d> & vertices();
        const std::vector<cv::Vec3d> & vertices() const;

        std::vector<cv::Vec3d> & normals();
        const std::vector<cv::Vec3d> & normals() const;

        std::vector<cv::Vec4b> & colors();
        const std::vector<cv::Vec4b> & colors() const;

    private:
        struct Private;
        Private *m_privImpl;
};

#endif // MESH_H
