/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RECON_H
#define RECON_H

#include <opencv2/core/core.hpp>
#include "export.h"
#include "mvg.h"
#include "mesh.h"

struct MVG_DECLSPEC VertexFilter
{
    enum { None, MaxEdgeLength, ShadowedTriangle, MinAngle, MaxEdgeLengthMinAngle };
    VertexFilter(int type_) : type(type_) { }
    virtual ~VertexFilter();
    const int type;
};

struct MVG_DECLSPEC NoFilter : public VertexFilter
{
    NoFilter() : VertexFilter(VertexFilter::None) { }
    ~NoFilter();
    bool reject(const cv::Vec3d &point_a,
                const cv::Vec3d &point_b,
                const cv::Vec3d &point_c);
};

struct MVG_DECLSPEC MaxEdgeLengthFilter : public VertexFilter
{
    MaxEdgeLengthFilter(double maximumEdgeLength=1.0) :
        VertexFilter(VertexFilter::MaxEdgeLength),
        m_maxEdgeLength(maximumEdgeLength)
    { }
    ~MaxEdgeLengthFilter();
    bool reject(const cv::Vec3d &point_a,
                const cv::Vec3d &point_b);
    bool reject(const cv::Vec3d &point_a,
                const cv::Vec3d &point_b,
                const cv::Vec3d &point_c);
    double m_maxEdgeLength;
};

struct MVG_DECLSPEC ShadowedTriangleFilter : public VertexFilter
{
    ShadowedTriangleFilter(double    angleTolerance=12.5,
                           cv::Vec3d cameraCenter=cv::Vec3d());
    ShadowedTriangleFilter(const ShadowedTriangleFilter &);
    ShadowedTriangleFilter & operator=(const ShadowedTriangleFilter &);
    ~ShadowedTriangleFilter();

    bool reject(const cv::Vec3d &point_a,
                const cv::Vec3d &point_b);
    bool reject(const cv::Vec3d &point_a,
                const cv::Vec3d &point_b,
                const cv::Vec3d &point_c);

    private:
        struct Private;
        Private *m_privImpl;
};

struct MVG_DECLSPEC MinAngleFilter : public VertexFilter
{
    MinAngleFilter(double minAngle=20.0) :
        VertexFilter(VertexFilter::MinAngle),
        m_minCosAngle(cos(std::min(std::max(minAngle, 0.), 180.) * CV_PI / 180.0)) { }
    ~MinAngleFilter();

    bool reject(const cv::Vec3d *const points,
                int                    id);
    bool reject(const cv::Vec3d &point_a,
                const cv::Vec3d &point_b,
                const cv::Vec3d &point_c);
    double m_minCosAngle;
};

struct MVG_DECLSPEC MaxEdgeLengthMinAngleFilter : public VertexFilter
{
    MaxEdgeLengthMinAngleFilter(double maxEdgeLength=1.0,
                                double minAngle=20.0) :
        VertexFilter(VertexFilter::MaxEdgeLengthMinAngle),
        m_maxEdgeLength(maxEdgeLength),
        m_minCosAngle(cos(std::min(std::max(minAngle, 0.), 180.) * CV_PI / 180.0))
    { }
    ~MaxEdgeLengthMinAngleFilter();

    bool reject(const cv::Vec3d &point_a,
                const cv::Vec3d &point_b,
                const cv::Vec3d &point_c);
    double m_maxEdgeLength;
    double m_minCosAngle;
};

// When partialCorrespondences is true, this is a special case of
// reconstructMesh where only partial correspondences are given with the
// epipolar geometry
//
// the pair of points are computed by removing radial distorsions and
// enforcing the epipolar constraint on the coordinates to recover the missing
// X or Y coordinate
MVG_DECLSPEC cv::Ptr<Mesh> reconstructMesh(const cv::Mat                   &camPoints,
                                           const cv::Mat                   &projPoints,
                                           const CameraGeometricParameters &camParameters,
                                           const CameraGeometricParameters &projParameters,
                                           const cv::Mat                   &texture=cv::Mat(),
                                           const cv::Mat                   &mask=cv::Mat(),
                                           const VertexFilter              &filter=NoFilter(),
                                           bool                             completeCorrespondences=
                                               true);

/*
 *   MVG_DECLSPEC cv::Ptr<Mesh> mergeMeshes(const cv::Ptr<Mesh>& mesh1,
 *                          const cv::Ptr<Mesh>& mesh2);
 */
#endif
