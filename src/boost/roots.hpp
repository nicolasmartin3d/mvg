/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ROOTS_HPP
#define ROOTS_HPP

// Adapted from
//  (C) Copyright John Maddock 2006.
//  Use, modification and distribution are subject to the
//  Boost Software License, Version 1.0. (See accompanying file
//  LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#include <limits>
#include <utility>
#include <stdexcept>

namespace detail
{
    template <typename T>
    int sign(T val)
    {
        return (T(0) < val) - (val < T(0));
    }

    template <class F, class T>
    void handle_zero_derivative(F        f,
                                T       &last_f0,
                                const T &f0,
                                T       &delta,
                                T       &result,
                                T       &guess,
                                const T &min,
                                const T &max)
    {
        if (last_f0 == 0)
        {
            // this must be the first iteration, pretend that we had a
            // previous one at either min or max:
            if (result == min)
            {
                guess = max;
            }
            else
            {
                guess = min;
            }
            // unpack_0(f(guess), last_f0);
            last_f0 = f(guess, 0).first;
            delta   = guess - result;
        }
        if (sign(last_f0) * sign(f0) < 0)
        {
            // we've crossed over so move in opposite direction to last step:
            if (delta < 0)
            {
                delta = (result - min) / 2;
            }
            else
            {
                delta = (result - max) / 2;
            }
        }
        else
        {
            // move in same direction as last step:
            if (delta < 0)
            {
                delta = (result - max) / 2;
            }
            else
            {
                delta = (result - min) / 2;
            }
        }
    }
}

template <class F, class T>
T newton_raphson_iterate(F    f,
                         T    guess,
                         T    min,
                         T    max,
                         int &max_iter,
                         T    tolerance=1e-8)
{
    T               f0(0);
    T               f1;
    T               last_f0(0);
    T               result = guess;
    std::pair<T, T> p;

    T delta  = 1;
    T delta1 = std::numeric_limits<T>::max();
    T delta2 = std::numeric_limits<T>::max();

    int count(max_iter);

    do
    {
        last_f0 = f0;
        delta2  = delta1;
        delta1  = delta;

        // boost::math::tie(f0, f1) = f(result);
        p  = f(result, 0);
        f0 = p.first; f1 = p.second;

        if (0 == f0)
        {
            break;
        }
        if (f1 == 0)
        {
            // Oops zero derivative!!!
            detail::handle_zero_derivative(f, last_f0, f0, delta, result, guess, min,
                                           max);
        }
        else
        {
            delta = f0 / f1;
        }
        if (fabs(delta * 2) > fabs(delta2))
        {
            // last two steps haven't converged, try bisection:
            delta = (delta > 0) ? (result - min) / 2 : (result - max) / 2;
        }
        guess   = result;
        result -= delta;
        if (result <= min)
        {
            delta  = 0.5F * (guess - min);
            result = guess - delta;
            if ((result == min) || (result == max))
            {
                break;
            }
        }
        else if (result >= max)
        {
            delta  = 0.5F * (guess - max);
            result = guess - delta;
            if ((result == min) || (result == max))
            {
                break;
            }
        }
        // update brackets:
        if (delta > 0)
        {
            max = guess;
        }
        else
        {
            min = guess;
        }
    }
    while (--count && (fabs(result * tolerance) < fabs(delta)));

    max_iter -= count;

    return result;
}

template <class F, class T>
inline T newton_raphson_iterate(F f,
                                T guess,
                                T min,
                                T max,
                                T tolerance=1e-8)
{
    int max_iter = 100;

    return newton_raphson_iterate(f, guess, min, max, max_iter, tolerance);
}

#endif // ROOTS_HPP
