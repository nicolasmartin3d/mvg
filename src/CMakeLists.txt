#
# MVG - A library to solve multiple view geometry problems.
# Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
#
# This file is part of MVG.
#
# MVG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# MVG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MVG.  If not, see <http://www.gnu.org/licenses/>.
#

file(GLOB lapack_srcs lapack/*.cpp lapack/*.c)
file(GLOB pcl_hdrs pcl/*.h pcl/*.tpp)
file(GLOB boost_hdrs boost/*.hpp boost/*.h boost/*.tpp)
file(GLOB mvg_srcs *.cpp)
file(GLOB mvg_hdrs *.h *.tpp)

#Adding header files to sources files only for them to appear in Creator, cmake knows it does not have to compile them
set(MVG_SOURCE_FILES ${lapack_srcs} ${pcl_hdrs} ${boost_hdrs} ${mvg_srcs} ${mvg_hdrs} CACHE INTERNAL "Source files of MVG.")
set(MVG_HEADER_FILES ${mvg_hdrs} CACHE INTERNAL "Header files of MVG.")
