/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <set>
#include <map>
#include <opencv2/imgproc/imgproc.hpp>
#include "pcl/organized_fast_mesher.h"
#include "recon.h"
#include "logger.h"

using namespace std;
using namespace cv;

Ptr<Mesh> reconstructMesh(const cv::Mat                   &camPoints,
                          const cv::Mat                   &projPoints,
                          const CameraGeometricParameters &camParameters,
                          const CameraGeometricParameters &projParameters,
                          const Mat                       &texture,
                          const Mat                       &mask,
                          const VertexFilter              &filter,
                          bool                             completeCorrespondences)
{
    Ptr<Mesh> mesh = Ptr<Mesh>(new Mesh);

    Size   camSize = camPoints.size();
    size_t count   = static_cast<size_t>(camSize.width * camSize.height);

    int         n = 0;
    vector<int> ids(count, -1);
    Mat         cpoints1, cpoints2;

    MatConstIterator_<Point2d> it1 = camPoints.begin<Point2d>(), it2 =
        projPoints.begin<Point2d>();
    for (int yc = 0; yc < camSize.height; ++yc)
    {
        const uchar *textPtr  = 0;
        const Vec3b *textPtrC = 0;
        if (texture.type() == CV_8U)
        {
            textPtr =
                (!texture.empty() ? texture.ptr<uchar>(yc) : 0);
        }
        else if (texture.type() == CV_8UC3)
        {
            textPtrC = (!texture.empty() ? texture.ptr<Vec3b>(yc) : 0);
        }

        const uchar *maskPtr = (!mask.empty() ? mask.ptr<uchar>(yc) : 0);

        for (int xc = 0; xc < camSize.width; ++xc, ++it1, ++it2)
        {
            if (maskPtr && (maskPtr[xc] == 0)) { continue; }

            cpoints1.push_back(*it1);
            cpoints2.push_back(*it2);

            ids[static_cast<size_t>(yc * camSize.width + xc)] = n;
            n++;

            if (textPtr)
            {
                uchar val = textPtr[xc];
                mesh->colors().push_back(Vec4b(val, val, val, 255));
            }
            else if (textPtrC)
            {
                Vec3b val = textPtrC[xc];
                mesh->colors().push_back(Vec4b(val[2], val[1], val[0], 255));
            }
            mesh->texCoords().push_back(Vec2d(xc / (camSize.width - 1.0),
                                              yc / (camSize.height - 1.0)));
        }
    }

    mesh->vertices().resize(static_cast<size_t>(cpoints1.rows * cpoints1.cols));

    Mat vertices(mesh->vertices());
    triangulatePoints(cpoints1, cpoints2, camParameters, projParameters, vertices,
                      completeCorrespondences);

    switch (filter.type)
    {
        case VertexFilter::None: {
            OrganizedFastMesher<NoFilter>(static_cast<const NoFilter &>(filter)).
            reconstruct(mesh, camSize, ids);
            break;
        }
        case VertexFilter::MaxEdgeLength: {
            OrganizedFastMesher<MaxEdgeLengthFilter>(
                static_cast<const MaxEdgeLengthFilter &>(filter)).
            reconstruct(mesh, camSize, ids);
            break;
        }
        case VertexFilter::ShadowedTriangle: {
            OrganizedFastMesher<ShadowedTriangleFilter>(
                static_cast<const ShadowedTriangleFilter &>(filter)).
            reconstruct(mesh, camSize, ids);
            break;
        }
        case VertexFilter::MinAngle: {
            OrganizedFastMesher<MinAngleFilter>(
                static_cast<const MinAngleFilter &>(filter)).
            reconstruct(mesh, camSize, ids);
            break;
        }
        case VertexFilter::MaxEdgeLengthMinAngle: {
            OrganizedFastMesher<MaxEdgeLengthMinAngleFilter>(
                static_cast<const MaxEdgeLengthMinAngleFilter &>(filter)).
            reconstruct(mesh, camSize, ids);
            break;
        }
        default:
            logError() << "Wrong filter used :" << filter.type;
    }

    // mesh->reverseNormals();

    return mesh;
}

#if 0
struct FaceComparator
{
    bool operator()(const TriMesh::Face &f1,
                    const TriMesh::Face &f2)
    {
        return lexicographical_compare(f1.v, f1.v + 3, f2.v, f2.v + 3);
    }
};

TriMesh * mergeMeshes(TriMesh *mesh1,
                      TriMesh *mesh2)
{
    TriMesh *mesh = new TriMesh;

    xform transform1 = xform::identity();
    xform transform2 = xform::identity();

    KDtree *tree1 = new KDtree(mesh1->vertices);
    KDtree *tree2 = new KDtree(mesh2->vertices);

    vector<float> weights1, weights2;

    float err = ICP(mesh1, mesh2,
                    transform1, transform2,
                    tree1, tree2,
                    weights1, weights2, 0.,
                    0, false, false);
    if (err >= 0.0f)
    {
        err = ICP(mesh1, mesh2,
                  transform1, transform2,
                  tree1, tree2,
                  weights1, weights2, 0.,
                  0, false, false);
    }

    if (err < 0.0f)
    {
        logError("ICP alignment failed");

        return 0;
    }

    for (vector<point>::iterator it = mesh2->vertices.begin(),
         itE = mesh2->vertices.end();
         it != itE; ++it)
    {
        *it = transform2 * (*it);
    }

    delete tree2;
    tree2 = new KDtree(mesh2->vertices);

    float minEdgeLength = mesh1->stat(TriMesh::STAT_MEAN, TriMesh::STAT_EDGELEN);
    float maxDist       = minEdgeLength;
    maxDist *= maxDist;

    const int   nbPoints1 = mesh1->vertices.size();
    vector<int> closest12(nbPoints1, -1);
    for (int i = 0; i < nbPoints1; ++i)
    {
        const float *p =
            tree2->closest_to_pt(reinterpret_cast<const float *>(&mesh1->vertices[i]),
                                 maxDist);
        closest12[i] = (p - reinterpret_cast<const float *>(&mesh2->vertices[0])) / 3;
    }

    const int   nbPoints2 = mesh2->vertices.size();
    vector<int> closest21(nbPoints2, -1);
    for (int i = 0; i < nbPoints2; ++i)
    {
        const float *p =
            tree1->closest_to_pt(reinterpret_cast<const float *>(&mesh2->vertices[i]),
                                 maxDist);
        closest21[i] = (p - reinterpret_cast<const float *>(&mesh1->vertices[0])) / 3;
    }

    bool hasColors = (!mesh1->colors.empty() && !mesh2->colors.empty());

    const int     nbFaces2 = mesh2->faces.size();
    map<int, int> remap2;
    for (int i = 0; i < nbPoints1; ++i)
    {
        int c = closest12[i];
        if ((c >= 0) && (c < nbPoints2) && (closest21[c] == i))
        {
            point p = 0.5f * (mesh1->vertices[i] + mesh2->vertices[c]);
            mesh->vertices.push_back(p);
            if (hasColors)
            {
                Color l = 0.5f * (mesh1->colors[i] + mesh2->colors[c]);
                mesh->colors.push_back(l);
            }
            closest12[i] = closest21[c] = -2;
            remap2[c]    = i;
        }
        else
        {
            mesh->vertices.push_back(mesh1->vertices[i]);
            if (hasColors) { mesh->colors.push_back(mesh1->colors[i]); }
        }
    }
    for (int i = 0; i < nbPoints2; ++i)
    {
        if (closest21[i] != -2)
        {
            remap2[i] = mesh->vertices.size();
            mesh->vertices.push_back(mesh2->vertices[i]);
            if (hasColors) { mesh->colors.push_back(mesh2->colors[i]); }
        }
    }

    set<TriMesh::Face, FaceComparator> faces;
    faces.insert(mesh1->faces.begin(), mesh1->faces.end());
    for (int i = 0; i < nbFaces2; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            mesh2->faces[i].v[j] = remap2[mesh2->faces[i].v[j]];
        }
    }
    faces.insert(mesh2->faces.begin(), mesh2->faces.end());

    mesh->faces.resize(faces.size());
    copy(faces.begin(), faces.end(), mesh->faces.begin());

    mesh->need_normals();
    invertNormals(mesh);

    delete tree1;
    delete tree2;

    return mesh;
}

void invertNormals(TriMesh *mesh)
{
    if (!mesh) { return; }
    for (int i = 0; i < mesh->normals.size(); ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            mesh->normals[i][j] *= -1;
        }
    }
}
#endif

VertexFilter::~VertexFilter()
{ }

NoFilter::~NoFilter()
{ }

bool NoFilter::reject(const cv::Vec3d & /*point_a*/,
                      const cv::Vec3d & /*point_b*/,
                      const cv::Vec3d & /*point_c*/)
{
    return false;
}

MaxEdgeLengthFilter::~MaxEdgeLengthFilter()
{ }

bool MaxEdgeLengthFilter::reject(const Vec3d &point_a,
                                 const Vec3d &point_b)
{
    cv::Vec3d v = point_a - point_b;
    double    n = norm(v);

    return n > m_maxEdgeLength;
}

bool MaxEdgeLengthFilter::reject(const Vec3d &point_a,
                                 const Vec3d &point_b,
                                 const Vec3d &point_c)
{
    return reject(point_a, point_b) || reject(point_b, point_c) ||
           reject(point_c, point_a);
}

struct ShadowedTriangleFilter::Private
{
    Private(double cosAngleTolerance_,
            Vec3d  cameraCenter_);
    ~Private();

    double cosAngleTolerance;
    Vec3d  cameraCenter;
};

ShadowedTriangleFilter::Private::Private(double cosAngleTolerance_,
                                         Vec3d  cameraCenter_) :
    cosAngleTolerance(cosAngleTolerance_),
    cameraCenter(cameraCenter_)
{ }

ShadowedTriangleFilter::Private::~Private()
{ }

ShadowedTriangleFilter::ShadowedTriangleFilter(double    angleTolerance,
                                               cv::Vec3d cameraCenter) :
    VertexFilter(VertexFilter::ShadowedTriangle)
{
    m_privImpl =
        new Private(cos(std::min(std::max(angleTolerance, 0.), 180.) * CV_PI / 180.0),
                    cameraCenter);
}

ShadowedTriangleFilter::ShadowedTriangleFilter(const ShadowedTriangleFilter &f) :
    VertexFilter(VertexFilter::ShadowedTriangle),
    m_privImpl(new Private(*f.m_privImpl))
{ }

ShadowedTriangleFilter & ShadowedTriangleFilter::operator=(const ShadowedTriangleFilter &f)
{
    if (this != &f)
    {
        delete m_privImpl;
        m_privImpl = new Private(*f.m_privImpl);
    }

    return *this;
}

ShadowedTriangleFilter::~ShadowedTriangleFilter()
{
    delete m_privImpl;
}

bool ShadowedTriangleFilter::reject(const Vec3d &point_a,
                                    const Vec3d &point_b)
{
    cv::Vec3d v1;
    cv::Vec3d v2;
    double    dt, db;
    v1 = m_privImpl->cameraCenter - point_a;
    v2 = point_b - point_a;
    dt = norm(v1);
    db = norm(v2);
    double c = v1.dot(v2) / (dt * db);

    return std::abs(c) > m_privImpl->cosAngleTolerance;
}

bool ShadowedTriangleFilter::reject(const Vec3d &point_a,
                                    const Vec3d &point_b,
                                    const Vec3d &point_c)
{
    return reject(point_a,  point_b) || reject(point_b, point_c) ||
           reject(point_c, point_a);
}

MinAngleFilter::~MinAngleFilter()
{ }

bool MinAngleFilter::reject(const Vec3d *const points,
                            int                id)
{
    cv::Vec3d u;
    cv::Vec3d v;
    double    nu, nv;
    u  = points[(id - 1) % 3] - points[id];
    v  = points[(id + 1) % 3] - points[id];
    nu = norm(u);
    nv = norm(v);
    double c = u.dot(v) / (nu * nv);

    return c > m_minCosAngle;
}

bool MinAngleFilter::reject(const Vec3d &point_a,
                            const Vec3d &point_b,
                            const Vec3d &point_c)
{
    cv::Vec3d points[] = { point_a, point_b, point_c };

    return reject(points, 0) || reject(points, 1) || reject(points, 2);
}

MaxEdgeLengthMinAngleFilter::~MaxEdgeLengthMinAngleFilter()
{ }

bool MaxEdgeLengthMinAngleFilter::reject(const Vec3d &point_a,
                                         const Vec3d &point_b,
                                         const Vec3d &point_c)
{
    cv::Vec3d u, v, w;
    double    nu, nv, nw;
    double    c;

    u  = point_b - point_a;
    nu = norm(u);
    if (nu > m_maxEdgeLength) { return true; }

    v  = point_c - point_a;
    nv = norm(v);
    if (nv > m_maxEdgeLength) { return true; }

    c = u.dot(v) / (nu * nv);
    if (c > m_minCosAngle) { return true; }

    w  = point_b - point_c;
    nw = norm(w);
    if (nw > m_maxEdgeLength) { return true; }

    c = (-w).dot(-u) / (nw * nu);
    if (c > m_minCosAngle) { return true; }

    c = (-v).dot(w) / (nv * nw);
    if (c > m_minCosAngle) { return true; }

    return false;
}

