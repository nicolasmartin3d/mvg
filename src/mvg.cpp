/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <opencv2/core/core.hpp>
#if CV_MAJOR_VERSION >= 3
#include <opencv2/core/utility.hpp>
#endif
#include <limits>
#include <iostream>
#include <stdexcept>

#include <ceres/internal/autodiff.h>
#include "boost/minima.hpp"
#include "boost/roots.hpp"
#include "mvg.h"
#include "lsvd.h"
#include "concurrency.h"
#include "logger.h"

using namespace std;
using namespace cv;

void rank2MatrixSVDCorrection(const Mat &src,
                              Mat       &dst)
{
    assert(src.rows == 3 && src.cols == 3);

    double _U[9], _VT[9], _w[3], _W[9];
    Mat    U(3, 3, CV_64F, _U),
    w(3, 1, CV_64F, _w),
    W(3, 3, CV_64F, _W),
    VT(3, 3, CV_64F, _VT);

    // SVD::compute(src, w, U, VT);
    LSVD::compute(src, w, U, VT);

    W             = Scalar::all(0);
    _W[0 * 3 + 0] = _w[0];
    _W[1 * 3 + 1] = _w[1];

    dst = U * W * VT;
}

/*
 * OpenCV's version for Point2d
 */
bool clipLine(Size     imgSize,
              Point2d &pt1,
              Point2d &pt2)
{
    double x1, y1, x2, y2;
    int    c1, c2;
    int    right = imgSize.width - 1, bottom = imgSize.height - 1;

    if ((imgSize.width <= 0) || (imgSize.height <= 0))
    {
        return false;
    }

    x1 = pt1.x; y1 = pt1.y; x2 = pt2.x; y2 = pt2.y;
    c1 = (x1 < 0) + (x1 > right) * 2 + (y1 < 0) * 4 + (y1 > bottom) * 8;
    c2 = (x2 < 0) + (x2 > right) * 2 + (y2 < 0) * 4 + (y2 > bottom) * 8;

    if (((c1 & c2) == 0) && ((c1 | c2) != 0))
    {
        int a;
        if (c1 & 12)
        {
            a   = c1 < 8 ? 0 : bottom;
            x1 += ((a - y1) * (x2 - x1) / (y2 - y1));
            y1  = a;
            c1  = (x1 < 0) + (x1 > right) * 2;
        }
        if (c2 & 12)
        {
            a   = c2 < 8 ? 0 : bottom;
            x2 += ((a - y2) * (x2 - x1) / (y2 - y1));
            y2  = a;
            c2  = (x2 < 0) + (x2 > right) * 2;
        }
        if (((c1 & c2) == 0) && ((c1 | c2) != 0))
        {
            if (c1)
            {
                a   = c1 == 1 ? 0 : right;
                y1 += ((a - x1) * (y2 - y1) / (x2 - x1));
                x1  = a;
                c1  = 0;
            }
            if (c2)
            {
                a   = c2 == 1 ? 0 : right;
                y2 += ((a - x2) * (y2 - y1) / (x2 - x1));
                x2  = a;
                c2  = 0;
            }
        }

        assert(((c1 & c2) != 0) || ((x1 != 0) | (y1 != 0) | (x2 != 0) | (y2 != 0)) >= 0);

        pt1.x = x1;
        pt1.y = y1;
        pt2.x = x2;
        pt2.y = y2;
    }

    return (c1 | c2) == 0;
}

// {{0,-z,y},{z,0,-x},{-y,x,0}}
void skewMatrix(const Mat &src,
                Mat       &dst)
{
    assert(src.rows * src.cols == 3);
    assert(src.depth() == CV_64F);

    dst.create(3, 3, CV_64F);
    const double *p = src.ptr<double>();
    double       *q = dst.ptr<double>();
    q[0 * 3 + 0] = 0;     q[0 * 3 + 1] = -p[2]; q[0 * 3 + 2] = p[1];
    q[1 * 3 + 0] = p[2];  q[1 * 3 + 1] = 0;     q[1 * 3 + 2] = -p[0];
    q[2 * 3 + 0] = -p[1]; q[2 * 3 + 1] = p[0];  q[2 * 3 + 2] = 0;
}

double distanceToEpipolarLine(const Mat     &F,
                              const Point2d &p,
                              const Point2d &q)
{
    const double *_F = F.ptr<double>();
    double        a, b, c, d, s;

    a = _F[0] * p.x + _F[1] * p.y + _F[2];
    b = _F[3] * p.x + _F[4] * p.y + _F[5];
    c = _F[6] * p.x + _F[7] * p.y + _F[8];

    s = 1. / sqrt(a * a + b * b);
    d = q.x * a + q.y * b + c;

    return d * s;
}

double distanceToEpipolarLine(const Mat     &l,
                              const Point2d &p)
{
    const double *_l = l.ptr<double>();
    double        d, s;

    s = 1. / sqrt(_l[0] * _l[0] + _l[1] * _l[1]);
    d = p.x * _l[0] + p.y * _l[1] + _l[2];

    return d * s;
}

double squaredDistanceToEpipolarLine(const Mat     &F,
                                     const Point2d &p,
                                     const Point2d &q)
{
    assert(F.rows == 3 && F.cols == 3);

    const double *_F = F.ptr<double>();
    double        a, b, c, d, s;

    a = _F[0] * p.x + _F[1] * p.y + _F[2];
    b = _F[3] * p.x + _F[4] * p.y + _F[5];
    c = _F[6] * p.x + _F[7] * p.y + _F[8];

    s = 1. / (a * a + b * b);
    d = q.x * a + q.y * b + c;

    return d * d * s;
}

double squaredDistanceToEpipolarLine(const Mat     &l,
                                     const Point2d &p)
{
    assert(l.rows * l.cols == 3);

    const double *_l = l.ptr<double>();
    double        d, s;

    s = 1. / (_l[0] * _l[0] + _l[1] * _l[1]);
    d = p.x * _l[0] + p.y * _l[1] + _l[2];

    return d * d * s;
}

double distanceToHomographyProjection(const Mat     &H,
                                      const Point2d &p,
                                      const Point2d &q)
{
    assert(H.rows == 3 && H.cols == 3);

    const double *_H = H.ptr<double>();
    double        ww = 1. / (_H[6] * p.x + _H[7] * p.y + _H[8]);
    double        dx = (_H[0] * p.x + _H[1] * p.y + _H[2]) * ww - q.x;
    double        dy = (_H[3] * p.x + _H[4] * p.y + _H[5]) * ww - q.y;

    return sqrt(dx * dx + dy * dy);
}

double squaredDistanceToHomographyProjection(const Mat     &H,
                                             const Point2d &p,
                                             const Point2d &q)
{
    assert(H.rows == 3 && H.cols == 3);

    const double *_H = H.ptr<double>();
    double        ww = 1. / (_H[6] * p.x + _H[7] * p.y + _H[8]);
    double        dx = (_H[0] * p.x + _H[1] * p.y + _H[2]) * ww - q.x;
    double        dy = (_H[3] * p.x + _H[4] * p.y + _H[5]) * ww - q.y;

    return dx * dx + dy * dy;
}

double squaredDistanceToCameraProjection(const Mat     &M,
                                         const Point3d &p,
                                         const
                                         Point2d       &q)
{
    assert(M.rows == 3 && M.cols == 4);

    const double *_M = M.ptr<double>();
    double        ww = 1. / (_M[8] * p.x + _M[9] * p.y + _M[10] * p.z + _M[11]);
    double        dx = (_M[0] * p.x + _M[1] * p.y + _M[2] * p.z + _M[3]) * ww - q.x;
    double        dy = (_M[4] * p.x + _M[5] * p.y + _M[6] * p.z + _M[7]) * ww - q.y;

    return dx * dx + dy * dy;
}

/*
 * Implements the distance between fundamentals by sampling random points through
 * each images and computing the difference between the reprojection error with
 * the actual and computed fundamentals
 * Method by Laveau in Zhang's paper on epipolar geometry estimation
 */
double distanceBetweenFundamentalMatrices(Size       dim,
                                          const Mat &F1,
                                          const Mat &F2)
{
    double _p[3], _q[3], _t[3];
    Mat    p(3, 1, CV_64F, _p);
    Mat    q(3, 1, CV_64F, _q);
    Mat    t(3, 1, CV_64F, _t);
    Mat    tF1 = F1.t(), tF2 = F2.t();
    // vector<double> distances;
    double d      = 0.;
    int    nIters = 5000;
    RNG    rng;

    for (int i = 0, tries = 0; i < nIters; tries++)
    {

        double d1, d2, d3, d4;
        if (tries > 250000) { return -1; }

        Point2d a, b;
        double  l;
        _q[0] = rng.uniform(0., static_cast<double>(dim.width));
        _q[1] = rng.uniform(0., static_cast<double>(dim.height));
        _q[2] = 1;

        t   = F1 * q;
        a.x = 0; a.y = -(_t[0] * a.x + _t[2]) / _t[1];
        b.x = dim.width; b.y = -(_t[0] * b.x + _t[2]) / _t[1];

        // does not intersect the line
        if (!clipLine(dim, a, b)) { continue; }

        // a point on the line joining p1 and p2
        l     = rng.uniform(0., 1.);
        _p[0] = a.x + l * (b.x - a.x);
        _p[1] = a.y + l * (b.y - a.y);
        _p[2] = 1;

        t  = F2 * q;
        d1 = squaredDistanceToEpipolarLine(t, Point2d(_p[0], _p[1]));

        t  = tF1 * p;
        d2 = squaredDistanceToEpipolarLine(t, Point2d(_q[0], _q[1]));

        t   = F2 * q;
        a.x = 0; a.y = -(_t[0] * a.x + _t[2]) / _t[1];
        b.x = dim.width; b.y = -(_t[0] * b.x + _t[2]) / _t[1];

        // does not intersect the line
        if (!clipLine(dim, a, b)) { continue; }

        // a point on the line joining the other points
        l     = rng.uniform(0., 1.);
        _p[0] = a.x + l * (b.x - a.x);
        _p[1] = a.y + l * (b.y - a.y);
        _p[2] = 1;

        t  = F1 * q;
        d3 = squaredDistanceToEpipolarLine(t, Point2d(_p[0], _p[1]));

        t  = tF2 * p;
        d4 = squaredDistanceToEpipolarLine(t, Point2d(_q[0], _q[1]));

        /*distances.push_back(d1);
         *    distances.push_back(d2);
         *    distances.push_back(d3);
         *    distances.push_back(d4);*/
        d += (d1 + d2 + d3 + d4) / 4.;

        ++i;
    }

    /*nth_element(distances.begin(), distances.begin()+distances.size()/2,
     *        distances.end());
     *   return mean(dist)[0];*/

    return sqrt(d / nIters);
}

double sampsonDistanceToEpipolarLine(const Mat     &F,
                                     const Point2d &p,
                                     const Point2d &q)
{
    const double *_F = F.ptr<double>();
    double        a1, b1, c1, a2, b2, /*c2,*/ d, s;
    double        norm, det;

    a1 = _F[0] * p.x + _F[1] * p.y + _F[2];
    b1 = _F[3] * p.x + _F[4] * p.y + _F[5];
    c1 = _F[6] * p.x + _F[7] * p.y + _F[8];

    a2 = _F[0] * q.x + _F[3] * q.y + _F[6];
    b2 = _F[1] * q.x + _F[4] * q.y + _F[7];
    // c2 = _F[2]*q.x + _F[5]*q.y + _F[8];

    d    = q.x * a1 + q.y * b1 + c1;
    norm = d * d;

    if (norm < std::numeric_limits<double>::epsilon())
    {
        return norm;
    }

    det = (a1 * a1 + b1 * b1 + a2 * a2 + b2 * b2);

    s = 1. / det;
    if (cvIsNaN(s))
    {
        if (det < 0) { s = -std::numeric_limits<double>::max(); }
        else { s = std::numeric_limits<double>::max(); }
    }
    /*s = (fabs(det) > std::numeric_limits<double>::epsilon() ? 1./det :
     *        1./std::numeric_limits<double>::epsilon());*/

    return norm * s;
}

/*
 * Automatically generated by Mathematica
 */
double sampsonDistanceToHomographyProjection(const Mat     &H,
                                             const Point2d &p,
                                             const Point2d &q)
{
    double        x0 = p.x, y0 = p.y, x1 = q.x, y1 = q.y;
    const double *x  = H.ptr<double>();

    double tmp2395, tmp2476, tmp2478, tmp2479, tmp2480, tmp2481, tmp2489, \
           tmp2490, tmp2491, tmp2492, tmp2494, tmp2495, tmp2496, tmp2497, tmp2501, \
           tmp2502, tmp2503, tmp2504, tmp2505, tmp2506, tmp2507, tmp2508, tmp2509, \
           tmp2510, tmp2515, tmp2516, tmp2517, tmp2522, tmp2523, tmp2524, tmp2512, \
           tmp2513, tmp2514, tmp2532, tmp2533, tmp2519, tmp2520, tmp2521, tmp2535, \
           tmp2536, tmp2527, tmp2528, tmp2529, tmp2518, tmp2525, tmp2526, tmp2530, \
           tmp2531, tmp2547, tmp2548, tmp2549, tmp2539, tmp2540, tmp2541, tmp2542, \
           tmp2543, tmp2544, tmp2545, tmp2534, tmp2537, tmp2538, tmp2555, tmp2556, \
           tmp2561, tmp2562, tmp2563, tmp2564, tmp2565, tmp2566, tmp2567, tmp2568, \
           tmp2569, tmp2570, tmp2572, tmp2550, tmp2557, tmp2546, tmp2551, tmp2552, \
           tmp2553, tmp2554, tmp2558, tmp2559, tmp2560, tmp2571, tmp2573, tmp2574, \
           tmp2575, tmp2576, tmp2577, tmp2578, tmp2579, tmp2580, tmp2581, tmp2582, \
           tmp2583, tmp2584, tmp2585, tmp2593, tmp2586, tmp2587, tmp2588, tmp2589, \
           tmp2590, tmp2591, tmp2609, tmp2598, tmp2599, tmp2600, tmp2601, tmp2602, \
           tmp2603, tmp2604, tmp2614, tmp2615, tmp2616;

    tmp2395 = x0 * y1 * x[0];
    tmp2476 = y0 * y1 * x[1];
    tmp2478 = y1 * x[2];
    tmp2479 = -(x0 * x1 * x[3]);
    tmp2480 = -(y0 * x1 * x[4]);
    tmp2481 = -(x1 * x[5]);
    tmp2489 = tmp2395 + tmp2476 + tmp2478 + tmp2479 + tmp2480 + tmp2481;
    tmp2490 = tmp2489 * tmp2489;
    tmp2491 = x0 * x[0];
    tmp2492 = y0 * x[1];
    tmp2494 = -(x0 * x1 * x[6]);
    tmp2495 = -(y0 * x1 * x[7]);
    tmp2496 = -(x1 * x[8]);
    tmp2497 = tmp2491 + tmp2492 + x[2] + tmp2494 + tmp2495 + tmp2496;
    tmp2501 = tmp2497 * tmp2497;
    tmp2502 = -(y1 * x[6]);
    tmp2503 = x[3] + tmp2502;
    tmp2504 = x0 * tmp2503;
    tmp2505 = -(y1 * x[7]);
    tmp2506 = x[4] + tmp2505;
    tmp2507 = y0 * tmp2506;
    tmp2508 = -(y1 * x[8]);
    tmp2509 = x[5] + tmp2504 + tmp2507 + tmp2508;
    tmp2510 = tmp2509 * tmp2509;
    tmp2515 = -x[3];
    tmp2516 = y1 * x[6];
    tmp2517 = tmp2515 + tmp2516;
    tmp2522 = -x[4];
    tmp2523 = y1 * x[7];
    tmp2524 = tmp2522 + tmp2523;
    tmp2512 = -(y1 * x[0]);
    tmp2513 = x1 * x[3];
    tmp2514 = tmp2512 + tmp2513;
    tmp2532 = -(x1 * x[6]);
    tmp2533 = x[0] + tmp2532;
    tmp2519 = -(y1 * x[1]);
    tmp2520 = x1 * x[4];
    tmp2521 = tmp2519 + tmp2520;
    tmp2535 = -(x1 * x[7]);
    tmp2536 = x[1] + tmp2535;
    tmp2527 = x0 * x[6];
    tmp2528 = y0 * x[7];
    tmp2529 = tmp2527 + tmp2528 + x[8];
    tmp2518 = tmp2514 * tmp2517;
    tmp2525 = tmp2521 * tmp2524;
    tmp2526 = tmp2491 + tmp2492 + x[2];
    tmp2530 = -(tmp2526 * tmp2529);
    tmp2531 = tmp2518 + tmp2525 + tmp2530;
    tmp2547 = tmp2533 * tmp2533;
    tmp2548 = tmp2536 * tmp2536;
    tmp2549 = tmp2529 * tmp2529;
    tmp2539 = tmp2514 * tmp2533;
    tmp2540 = tmp2521 * tmp2536;
    tmp2541 = x0 * x[3];
    tmp2542 = y0 * x[4];
    tmp2543 = tmp2541 + tmp2542 + x[5];
    tmp2544 = -(tmp2543 * tmp2529);
    tmp2545 = tmp2539 + tmp2540 + tmp2544;
    tmp2534 = tmp2533 * tmp2517;
    tmp2537 = tmp2536 * tmp2524;
    tmp2538 = tmp2534 + tmp2537;
    tmp2555 = tmp2503 * tmp2503;
    tmp2556 = tmp2506 * tmp2506;
    tmp2561 = tmp2526 * tmp2526;
    tmp2562 = y1 * x[0];
    tmp2563 = -(x1 * x[3]);
    tmp2564 = tmp2562 + tmp2563;
    tmp2565 = tmp2564 * tmp2564;
    tmp2566 = y1 * x[1];
    tmp2567 = -(x1 * x[4]);
    tmp2568 = tmp2566 + tmp2567;
    tmp2569 = tmp2568 * tmp2568;
    tmp2570 = tmp2543 * tmp2543;
    tmp2572 = tmp2538 * tmp2538;
    tmp2550 = tmp2547 + tmp2548 + tmp2549;
    tmp2557 = tmp2555 + tmp2556 + tmp2549;
    tmp2546 = tmp2538 * tmp2545;
    tmp2551 = -(tmp2531 * tmp2550);
    tmp2552 = tmp2546 + tmp2551;
    tmp2553 = tmp2531 * tmp2552;
    tmp2554 = -(tmp2538 * tmp2531);
    tmp2558 = tmp2545 * tmp2557;
    tmp2559 = tmp2554 + tmp2558;
    tmp2560 = -(tmp2545 * tmp2559);
    tmp2571 = tmp2561 + tmp2565 + tmp2569 + tmp2570;
    tmp2573 = -tmp2572;
    tmp2574 = tmp2550 * tmp2557;
    tmp2575 = tmp2573 + tmp2574;
    tmp2576 = tmp2571 * tmp2575;
    tmp2577 = tmp2553 + tmp2560 + tmp2576;
    tmp2578 = 1. / tmp2577;
    if (cvIsInf(tmp2578))
    {
        if (tmp2577 < 0) { tmp2578 = -std::numeric_limits<double>::max(); }
        else { tmp2578 = std::numeric_limits<double>::max(); }
    }
    /*tmp2578 = (fabs(tmp2577) > std::numeric_limits<double>::epsilon() ?
     *        1./tmp2577 : 1./std::numeric_limits<double>::epsilon());*/

    tmp2579 = -(x0 * x[3]);
    tmp2580 = -(y0 * x[4]);
    tmp2581 = -x[5];
    tmp2582 = x0 * y1 * x[6];
    tmp2583 = y0 * y1 * x[7];
    tmp2584 = y1 * x[8];
    tmp2585 = tmp2579 + tmp2580 + tmp2581 + tmp2582 + tmp2583 + tmp2584;
    tmp2593 = tmp2545 * tmp2545;
    tmp2586 = x0 * tmp2533;
    tmp2587 = y0 * tmp2536;
    tmp2588 = x[2] + tmp2586 + tmp2587 + tmp2496;
    tmp2589 = -(tmp2571 * tmp2538);
    tmp2590 = tmp2531 * tmp2545;
    tmp2591 = tmp2589 + tmp2590;
    tmp2609 = tmp2531 * tmp2531;
    tmp2598 = -(x0 * y1 * x[0]);
    tmp2599 = -(y0 * y1 * x[1]);
    tmp2600 = -(y1 * x[2]);
    tmp2601 = x0 * x1 * x[3];
    tmp2602 = y0 * x1 * x[4];
    tmp2603 = x1 * x[5];
    tmp2604 = tmp2598 + tmp2599 + tmp2600 + tmp2601 + tmp2602 + tmp2603;
    tmp2614 = tmp2538 * tmp2531;
    tmp2615 = -(tmp2545 * tmp2557);
    tmp2616 = tmp2614 + tmp2615;

    double norm = fabs(tmp2490 + tmp2501 + tmp2510);
    if (norm < std::numeric_limits<double>::epsilon())
    {
        return norm;
    }

    // *INDENT-OFF*
    //double det = tmp2577;
    double val = fabs(tmp2578*(tmp2585*(tmp2588*tmp2591 + tmp2585*(-tmp2593 +
                    tmp2571*tmp2550) + tmp2604*tmp2552) +
            tmp2588*(tmp2585*tmp2591 + tmp2588*(-tmp2609 + tmp2571*tmp2557) +
                tmp2604*tmp2616) + tmp2604*(tmp2585*tmp2552 + tmp2588*tmp2616
                    + tmp2604*tmp2575)));
    // *INDENT-ON*
    /*
     *   printf("norm = %16.30f (%d) %16.30f (%d) (%d) %16.30f %16.30f\n", norm, fabs(norm) <
     *        std::numeric_limits<double>::epsilon(), det,
     *        fabs(det-std::numeric_limits<double>::min()) <
     *        std::numeric_limits<double>::epsilon()*
     *            fabs(det), isinf(tmp2578), tmp2578, val );
     *   printf("%16.30f %16.30f\n", tmp2577, 1./tmp2577);
     *   if (isinf(1./tmp2577)) assert(tmp2577>=0);*/
    assert(!cvIsNaN(val));

    /*printf("norm = %16.30f (%d) %16.30f %16.30f\n", norm, fabs(norm) <
     *        std::numeric_limits<double>::epsilon(), det, val );*/

    /*printf("%15.30f %15.30f %15.30f %15.30f %15.30f %15.30f %15.30f %15.30f "
     *       "%15.30f %15.30f %15.30f %15.30f %15.30f\n",
     *   tmp1000, tmp1003, tmp1005, tmp1016, tmp1021, tmp1028,
     *   tmp962, tmp964, tmp969, tmp983, tmp987, tmp990, tmp997);
     *
     *   printf("%15.30f %15.30f %15.30f\n",(-tmp1005 + \
     *                tmp983*tmp962 + tmp1016*tmp964),(-tmp1021 + tmp983*tmp969),
     *        (tmp997*tmp964 + tmp1000*tmp1028 + tmp1016*tmp987));
     *
     *   printf("%15.30f\n",(tmp1000*tmp1003 + tmp997*(-tmp1005 + \
     *                tmp983*tmp962 + tmp1016*tmp964) + tmp1000*(tmp997*tmp1003 + \
     *                    tmp1000*(-tmp1021 + tmp983*tmp969) + tmp1016*tmp1028) + \
     *            tmp1016*(tmp997*tmp964 + tmp1000*tmp1028 + tmp1016*tmp987)));
     *
     *   printf("%15.30f\n",(tmp997*(tmp1000*tmp1003 + tmp997*(-tmp1005 + \
     *                tmp983*tmp962 + tmp1016*tmp964) + tmp1000*(tmp997*tmp1003 + \
     *                    tmp1000*(-tmp1021 + tmp983*tmp969) + tmp1016*tmp1028) + \
     *            tmp1016*(tmp997*tmp964 + tmp1000*tmp1028 + tmp1016*tmp987))));
     *
     *   printf("%15.30f\n",tmp990*(tmp997*(tmp1000*tmp1003 + tmp997*(-tmp1005 + \
     *                tmp983*tmp962 + tmp1016*tmp964) + tmp1000*(tmp997*tmp1003 + \
     *                    tmp1000*(-tmp1021 + tmp983*tmp969) + tmp1016*tmp1028) + \
     *            tmp1016*(tmp997*tmp964 + tmp1000*tmp1028 + tmp1016*tmp987))));*/

    return val;
}

/*
 * Automatically generated by Mathematica
 */
double sampsonDistanceToCameraProjection(const Mat     &M,
                                         const Point3d &p,
                                         const Point2d &q)
{
    double        x0 = p.x, y0 = p.y, z0 = p.z, x1 = q.x, y1 = q.y;
    const double *x  = M.ptr<double>();

    double tmp2663, tmp2766, tmp2768, tmp2769, tmp2770, tmp2771, tmp2781, \
           tmp2782, tmp2783, tmp2784, tmp2786, tmp2787, tmp2788, tmp2789, tmp2793, \
           tmp2794, tmp2795, tmp2796, tmp2797, tmp2798, tmp2799, tmp2800, tmp2801, \
           tmp2802, tmp2803, tmp2804, tmp2805, tmp2806, tmp2807, tmp2808, tmp2813, \
           tmp2814, tmp2815, tmp2820, tmp2821, tmp2822, tmp2827, tmp2828, tmp2829, \
           tmp2810, tmp2811, tmp2812, tmp2838, tmp2839, tmp2817, tmp2818, tmp2819, \
           tmp2841, tmp2842, tmp2824, tmp2825, tmp2826, tmp2844, tmp2845, tmp2832, \
           tmp2833, tmp2834, tmp2835, tmp2816, tmp2823, tmp2830, tmp2831, tmp2836, \
           tmp2837, tmp2857, tmp2858, tmp2859, tmp2860, tmp2848, tmp2849, tmp2850, \
           tmp2851, tmp2852, tmp2853, tmp2854, tmp2855, tmp2840, tmp2843, tmp2846, \
           tmp2847, tmp2866, tmp2867, tmp2868, tmp2869, tmp2870, tmp2875, tmp2876, \
           tmp2877, tmp2878, tmp2879, tmp2880, tmp2881, tmp2882, tmp2883, tmp2884, \
           tmp2885, tmp2886, tmp2887, tmp2888, tmp2890, tmp2861, tmp2871, tmp2856, \
           tmp2862, tmp2863, tmp2864, tmp2865, tmp2872, tmp2873, tmp2874, tmp2889, \
           tmp2891, tmp2892, tmp2893, tmp2894, tmp2895, tmp2896, tmp2897, tmp2898, \
           tmp2899, tmp2900, tmp2901, tmp2902, tmp2903, tmp2904, tmp2905, tmp2913, \
           tmp2906, tmp2907, tmp2908, tmp2909, tmp2910, tmp2911, tmp2931, tmp2918, \
           tmp2919, tmp2920, tmp2921, tmp2922, tmp2923, tmp2924, tmp2925, tmp2926, \
           tmp2936, tmp2937, tmp2938;

    tmp2663 = x0 * y1 * x[0];
    tmp2766 = y0 * y1 * x[1];
    tmp2768 = z0 * y1 * x[2];
    tmp2769 = y1 * x[3];
    tmp2770 = -(x0 * x1 * x[4]);
    tmp2771 = -(y0 * x1 * x[5]);
    tmp2781 = -(z0 * x1 * x[6]);
    tmp2782 = -(x1 * x[7]);
    tmp2783 = tmp2663 + tmp2766 + tmp2768 + tmp2769 + tmp2770 + tmp2771 + \
              tmp2781 + tmp2782;
    tmp2784 = tmp2783 * tmp2783;
    tmp2786 = x0 * x[0];
    tmp2787 = y0 * x[1];
    tmp2788 = z0 * x[2];
    tmp2789 = -(x0 * x1 * x[8]);
    tmp2793 = -(y0 * x1 * x[9]);
    tmp2794 = -(z0 * x1 * x[10]);
    tmp2795 = -(x1 * x[11]);
    tmp2796 = tmp2786 + tmp2787 + tmp2788 + x[3] + tmp2789 + tmp2793 + \
              tmp2794 + tmp2795;
    tmp2797 = tmp2796 * tmp2796;
    tmp2798 = z0 * x[6];
    tmp2799 = -(y1 * x[8]);
    tmp2800 = x[4] + tmp2799;
    tmp2801 = x0 * tmp2800;
    tmp2802 = -(y1 * x[9]);
    tmp2803 = x[5] + tmp2802;
    tmp2804 = y0 * tmp2803;
    tmp2805 = -(z0 * y1 * x[10]);
    tmp2806 = -(y1 * x[11]);
    tmp2807 = tmp2798 + x[7] + tmp2801 + tmp2804 + tmp2805 + tmp2806;
    tmp2808 = tmp2807 * tmp2807;
    tmp2813 = -x[4];
    tmp2814 = y1 * x[8];
    tmp2815 = tmp2813 + tmp2814;
    tmp2820 = -x[5];
    tmp2821 = y1 * x[9];
    tmp2822 = tmp2820 + tmp2821;
    tmp2827 = -x[6];
    tmp2828 = y1 * x[10];
    tmp2829 = tmp2827 + tmp2828;
    tmp2810 = -(y1 * x[0]);
    tmp2811 = x1 * x[4];
    tmp2812 = tmp2810 + tmp2811;
    tmp2838 = -(x1 * x[8]);
    tmp2839 = x[0] + tmp2838;
    tmp2817 = -(y1 * x[1]);
    tmp2818 = x1 * x[5];
    tmp2819 = tmp2817 + tmp2818;
    tmp2841 = -(x1 * x[9]);
    tmp2842 = x[1] + tmp2841;
    tmp2824 = -(y1 * x[2]);
    tmp2825 = x1 * x[6];
    tmp2826 = tmp2824 + tmp2825;
    tmp2844 = -(x1 * x[10]);
    tmp2845 = x[2] + tmp2844;
    tmp2832 = x0 * x[8];
    tmp2833 = y0 * x[9];
    tmp2834 = z0 * x[10];
    tmp2835 = tmp2832 + tmp2833 + tmp2834 + x[11];
    tmp2816 = tmp2812 * tmp2815;
    tmp2823 = tmp2819 * tmp2822;
    tmp2830 = tmp2826 * tmp2829;
    tmp2831 = tmp2786 + tmp2787 + tmp2788 + x[3];
    tmp2836 = -(tmp2831 * tmp2835);
    tmp2837 = tmp2816 + tmp2823 + tmp2830 + tmp2836;
    tmp2857 = tmp2839 * tmp2839;
    tmp2858 = tmp2842 * tmp2842;
    tmp2859 = tmp2845 * tmp2845;
    tmp2860 = tmp2835 * tmp2835;
    tmp2848 = tmp2812 * tmp2839;
    tmp2849 = tmp2819 * tmp2842;
    tmp2850 = tmp2826 * tmp2845;
    tmp2851 = x0 * x[4];
    tmp2852 = y0 * x[5];
    tmp2853 = tmp2851 + tmp2852 + tmp2798 + x[7];
    tmp2854 = -(tmp2853 * tmp2835);
    tmp2855 = tmp2848 + tmp2849 + tmp2850 + tmp2854;
    tmp2840 = tmp2839 * tmp2815;
    tmp2843 = tmp2842 * tmp2822;
    tmp2846 = tmp2845 * tmp2829;
    tmp2847 = tmp2840 + tmp2843 + tmp2846;
    tmp2866 = tmp2800 * tmp2800;
    tmp2867 = tmp2803 * tmp2803;
    tmp2868 = -(y1 * x[10]);
    tmp2869 = x[6] + tmp2868;
    tmp2870 = tmp2869 * tmp2869;
    tmp2875 = tmp2831 * tmp2831;
    tmp2876 = y1 * x[0];
    tmp2877 = -(x1 * x[4]);
    tmp2878 = tmp2876 + tmp2877;
    tmp2879 = tmp2878 * tmp2878;
    tmp2880 = y1 * x[1];
    tmp2881 = -(x1 * x[5]);
    tmp2882 = tmp2880 + tmp2881;
    tmp2883 = tmp2882 * tmp2882;
    tmp2884 = y1 * x[2];
    tmp2885 = -(x1 * x[6]);
    tmp2886 = tmp2884 + tmp2885;
    tmp2887 = tmp2886 * tmp2886;
    tmp2888 = tmp2853 * tmp2853;
    tmp2890 = tmp2847 * tmp2847;
    tmp2861 = tmp2857 + tmp2858 + tmp2859 + tmp2860;
    tmp2871 = tmp2866 + tmp2867 + tmp2870 + tmp2860;
    tmp2856 = tmp2847 * tmp2855;
    tmp2862 = -(tmp2837 * tmp2861);
    tmp2863 = tmp2856 + tmp2862;
    tmp2864 = tmp2837 * tmp2863;
    tmp2865 = -(tmp2847 * tmp2837);
    tmp2872 = tmp2855 * tmp2871;
    tmp2873 = tmp2865 + tmp2872;
    tmp2874 = -(tmp2855 * tmp2873);
    tmp2889 = tmp2875 + tmp2879 + tmp2883 + tmp2887 + tmp2888;
    tmp2891 = -tmp2890;
    tmp2892 = tmp2861 * tmp2871;
    tmp2893 = tmp2891 + tmp2892;
    tmp2894 = tmp2889 * tmp2893;
    tmp2895 = tmp2864 + tmp2874 + tmp2894;
    tmp2896 = 1. / tmp2895;
    if (cvIsInf(tmp2896))
    {
        if (tmp2895 < 0) { tmp2896 = -std::numeric_limits<double>::max(); }
        else { tmp2896 = std::numeric_limits<double>::max(); }
    }
    /*tmp2896 = (fabs(tmp2895) > std::numeric_limits<double>::epsilon() ?
     *        1./tmp2895 : 1./std::numeric_limits<double>::epsilon());*/
    tmp2897 = -(x0 * x[4]);
    tmp2898 = -(y0 * x[5]);
    tmp2899 = -(z0 * x[6]);
    tmp2900 = -x[7];
    tmp2901 = x0 * y1 * x[8];
    tmp2902 = y0 * y1 * x[9];
    tmp2903 = z0 * y1 * x[10];
    tmp2904 = y1 * x[11];
    tmp2905 = tmp2897 + tmp2898 + tmp2899 + tmp2900 + tmp2901 + tmp2902 + \
              tmp2903 + tmp2904;
    tmp2913 = tmp2855 * tmp2855;
    tmp2906 = x0 * tmp2839;
    tmp2907 = y0 * tmp2842;
    tmp2908 = tmp2788 + x[3] + tmp2906 + tmp2907 + tmp2794 + tmp2795;
    tmp2909 = -(tmp2889 * tmp2847);
    tmp2910 = tmp2837 * tmp2855;
    tmp2911 = tmp2909 + tmp2910;
    tmp2931 = tmp2837 * tmp2837;
    tmp2918 = -(x0 * y1 * x[0]);
    tmp2919 = -(y0 * y1 * x[1]);
    tmp2920 = -(z0 * y1 * x[2]);
    tmp2921 = -(y1 * x[3]);
    tmp2922 = x0 * x1 * x[4];
    tmp2923 = y0 * x1 * x[5];
    tmp2924 = z0 * x1 * x[6];
    tmp2925 = x1 * x[7];
    tmp2926 = tmp2918 + tmp2919 + tmp2920 + tmp2921 + tmp2922 + tmp2923 + \
              tmp2924 + tmp2925;
    tmp2936 = tmp2847 * tmp2837;
    tmp2937 = -(tmp2855 * tmp2871);
    tmp2938 = tmp2936 + tmp2937;
    double norm = tmp2784 + tmp2797 + tmp2808;
    if (norm < std::numeric_limits<double>::epsilon())
    {
        return norm;
    }
    // *INDENT-OFF*
    //double det = tmp2895;
    double val = fabs(tmp2896*(tmp2905*(tmp2908*tmp2911 + tmp2905*(-tmp2913 \
                 + tmp2889*tmp2861) + tmp2926*tmp2863) + tmp2908*(tmp2905*tmp2911 + \
                 tmp2908*(-tmp2931 + tmp2889*tmp2871) + tmp2926*tmp2938) + \
                 tmp2926*(tmp2905*tmp2863 + tmp2908*tmp2938 + tmp2926*tmp2893)));
    // *INDENT-ON*
    /*
     *   if (fabs(x[0]-(0.000129818696061601)) < 10E-3 &&
     *   fabs(x[4]-(-0.0001478773592789584)) < 10E-3 &&
     *    fabs(x[8]-(1.232641881400605E-05)) < 10E-3) {
     *    printf(">>>%16.30f %16.30f %16.30f\n",x0, y0, z0);
     *
     *   printf("%16.30f %16.30f %16.30f %16.30f %16.30f %16.30f %16.30f %16.30f\n",
     *    tmp2786 , tmp2787 , tmp2788 , x[3] , tmp2789 , tmp2793 , tmp2794 ,
     *    tmp2795);
     *   printf("norm = %16.30f (%d) %16.30f %16.30f\n", norm, fabs(norm) <
     *        std::numeric_limits<double>::epsilon(), det, val );
     *   }*/

    return val;
}

void projectiveCamerasFromFundamentalMatrix(const Mat &F,
                                            Mat       &M1,
                                            Mat       &M2)
{
    double _U[9], _VT[9], _w[3], _W[9], _e[3];
    Mat    U(3, 3, CV_64F, _U),
    w(3, 1, CV_64F, _w),
    W(3, 3, CV_64F, _W),
    VT(3, 3, CV_64F, _VT);
    Mat eX, e(3, 1, CV_64F, _e);
    // SVD::compute(F, w, U, VT);
    LSVD::compute(F, w, U, VT);
    U.col(2).copyTo(e);

    skewMatrix(e, eX);

    M1 = Mat::eye(3, 4, CV_64F);
    M2.create(3, 4, CV_64F);
    Mat M2s3x3 = M2(Range::all(), Range(0, 3)),
        M2s3x1 = M2(Range::all(), Range(3, 4));
    M2s3x3 = eX * F;
    e.copyTo(M2s3x1);
}

struct PointsTriangulater : public ParallelForBody
{
    PointsTriangulater(const uchar       *mask,
                       size_t             count,
                       const vector<Mat> &nCameras,
                       const vector<Mat> &undImagePoints,
                       double             scalex,
                       double             scaley,
                       Point3d           *out
                       ) :
        m_mask(mask),
        m_count(count),
        m_nCameras(nCameras),
        m_undImagePoints(undImagePoints),
        m_scalex(scalex),
        m_scaley(scaley),
        m_out(out)
    {
        m_ncams = nCameras.size();
    }
    ~PointsTriangulater();

    virtual void operator()(size_t begin,
                            size_t end) const
    {
        Mat    A;
        double _X[4];
        Mat    X(4, 1, CV_64F, _X);

        for (size_t i = begin; i < end; ++i)
        {
            int nb = 0;
            for (size_t j = 0; j < m_ncams; ++j)
            {
                nb += m_mask[j * m_count + i];
            }

            assert(nb >= 2);

            A.create(nb * 3, 4, CV_64F);
            double *_A = A.ptr<double>();

            for (size_t k = 0, j = 0; k < m_ncams; ++k)
            {
                if (!m_mask[k * m_count + i]) { continue; }

                const Point2d *p  = m_undImagePoints[k].ptr<Point2d>();
                const double  *_M = m_nCameras[k].ptr<double>();

                double x = (p[i].x * m_scalex) - 1,
                       y = (p[i].y * m_scaley) - 1;

                _A[(j * 3 + 0) * 4 + 0] = -_M[1 * 4 + 0] + _M[2 * 4 + 0] * y;
                _A[(j * 3 + 0) * 4 + 1] = -_M[1 * 4 + 1] + _M[2 * 4 + 1] * y;
                _A[(j * 3 + 0) * 4 + 2] = -_M[1 * 4 + 2] + _M[2 * 4 + 2] * y;
                _A[(j * 3 + 0) * 4 + 3] = -_M[1 * 4 + 3] + _M[2 * 4 + 3] * y;

                _A[(j * 3 + 1) * 4 + 0] = _M[0 * 4 + 0] - _M[2 * 4 + 0] * x;
                _A[(j * 3 + 1) * 4 + 1] = _M[0 * 4 + 1] - _M[2 * 4 + 1] * x;
                _A[(j * 3 + 1) * 4 + 2] = _M[0 * 4 + 2] - _M[2 * 4 + 2] * x;
                _A[(j * 3 + 1) * 4 + 3] = _M[0 * 4 + 3] - _M[2 * 4 + 3] * x;

                _A[(j * 3 + 2) * 4 + 0] = _M[1 * 4 + 0] * x - _M[0 * 4 + 0] * y;
                _A[(j * 3 + 2) * 4 + 1] = _M[1 * 4 + 1] * x - _M[0 * 4 + 1] * y;
                _A[(j * 3 + 2) * 4 + 2] = _M[1 * 4 + 2] * x - _M[0 * 4 + 2] * y;
                _A[(j * 3 + 2) * 4 + 3] = _M[1 * 4 + 3] * x - _M[0 * 4 + 3] * y;

                ++j;
            }

            SVD::solveZ(A, X);

            for (int j = 0; j < 3; ++j) { _X[j] /= _X[3]; }
            m_out[i] = Point3d(_X[0], _X[1], _X[2]);
        }
    }

    size_t             m_ncams;
    const uchar       *m_mask;
    size_t             m_count;
    const vector<Mat> &m_nCameras;
    const vector<Mat> &m_undImagePoints;
    double             m_scalex;
    double             m_scaley;
    Point3d           *m_out;
};

// out of line destructor
PointsTriangulater::~PointsTriangulater()
{ }

void triangulatePoints(const vector<Mat> &imagePoints,
                       const vector<Mat> &cameras,
                       const Mat         &visibilityMask,
                       Size               camSize,
                       Mat               &scenePoints)
{
    assert(imagePoints.size() >= 2);
    assert(imagePoints.size() == cameras.size());
    assert(imagePoints[0].cols == 1 || imagePoints[0].rows == 1);

    size_t ncams = imagePoints.size();
    size_t count = static_cast<size_t>(imagePoints[0].rows * imagePoints[0].cols);

    scenePoints.create(static_cast<int>(count), 1, CV_64FC3);

    assert(visibilityMask.rows == static_cast<int>(ncams) &&
           visibilityMask.cols == static_cast<int>(count));
    assert(visibilityMask.type() == CV_8U);
    for (size_t i = 0; i < imagePoints.size(); ++i)
    {
        assert(imagePoints[i].rows * imagePoints[i].cols == count);
    }

    const uchar *mask = visibilityMask.ptr<uchar>();
    Point3d     *P    = scenePoints.ptr<Point3d>();
    vector<Mat>  nCameras(ncams); // normalized cameras

    // !!!!do something if camSize = Size()
    double scalex = 2.0 / camSize.width, scaley = 2.0 / camSize.height;
    double _T[]   = { scalex, 0, -1, 0, scaley, -1, 0, 0, 1 };
    Mat    T(3, 3, CV_64F, _T);

    for (size_t j = 0; j < ncams; ++j)
    {
        nCameras[j] = T * cameras[j];
    }

    PointsTriangulater triangulater(mask, count, nCameras, imagePoints, scalex,
                                    scaley,
                                    P);
    parallel_for(0, count, triangulater);
}

void triangulatePoints(const vector<Mat>                       &imagePoints,
                       const vector<CameraGeometricParameters> &cameras,
                       const Mat                               &visibilityMask,
                       Size                                     camSize,
                       Mat                                     &scenePoints)
{
    assert(imagePoints.size() >= 2);
    assert(imagePoints.size() == cameras.size());
    assert(imagePoints[0].cols == 1 || imagePoints[0].rows == 1);

    size_t ncams = imagePoints.size();
    size_t count = static_cast<size_t>(imagePoints[0].rows * imagePoints[0].cols);

    scenePoints.create(static_cast<int>(count), 1, CV_64FC3);

    assert(visibilityMask.rows == static_cast<int>(ncams) &&
           visibilityMask.cols == static_cast<int>(count));
    assert(visibilityMask.type() == CV_8U);
    for (size_t i = 0; i < imagePoints.size(); ++i)
    {
        assert(imagePoints[i].rows * imagePoints[i].cols == count);
    }

    const uchar *mask = visibilityMask.ptr<uchar>();
    Point3d     *P    = scenePoints.ptr<Point3d>();
    vector<Mat>  nCameras(ncams); // normalized cameras

    double scalex = 2.0 / camSize.width, scaley = 2.0 / camSize.height;
    double _T[]   = { scalex, 0, -1, 0, scaley, -1, 0, 0, 1 };
    Mat    T(3, 3, CV_64F, _T);

    vector<Mat> undImagePoints(ncams);
    for (size_t j = 0; j < ncams; ++j)
    {
        nCameras[j] = T * cameras[j].M();
        undistortPoints(imagePoints[j], undImagePoints[j],
                        cameras[j].K(), cameras[j].distorsionCoefficients(),
                        Mat(), cameras[j].K());
    }

    PointsTriangulater triangulater(mask, count, nCameras, undImagePoints, scalex,
                                    scaley,
                                    P);
    parallel_for(0, count, triangulater);
}

struct TwoViewPointsTriangulater : public ParallelForBody
{
    TwoViewPointsTriangulater(const CameraGeometricParameters &cam1,
                              const CameraGeometricParameters &cam2,
                              const Mat                       &T1,
                              const Mat                       &T2,
                              const Mat                       &p1,
                              const Mat                       &p2,
                              Mat                             &p3ds) :
        m_cam1(cam1),
        m_T1(T1),
        m_p1(p1),
        m_cam2(cam2),
        m_T2(T2),
        m_p2(p2),
        m_p3ds(p3ds)
    { }
    ~TwoViewPointsTriangulater();

    virtual void operator()(size_t begin,
                            size_t end) const
    {
        Mat ups[2], Ms[2];
        Mat p1, p2;

        if (m_p1.rows > 1)
        {
            p1 = m_p1.rowRange(static_cast<int>(begin), static_cast<int>(end));
        }
        else
        {
            p1 = m_p1.colRange(static_cast<int>(begin), static_cast<int>(end));
        }
        Ms[0] = m_T1 * m_cam1.M();
        undistortPoints(p1, ups[0], m_cam1.K(), m_cam1.distorsionCoefficients(),
                        Mat(), m_T1 * m_cam1.K());

        if (m_p2.rows > 1)
        {
            p2 = m_p2.rowRange(static_cast<int>(begin), static_cast<int>(end));
        }
        else
        {
            p2 = m_p2.colRange(static_cast<int>(begin), static_cast<int>(end));
        }
        Ms[1] = m_T2 * m_cam2.M();
        undistortPoints(p2, ups[1], m_cam2.K(), m_cam2.distorsionCoefficients(),
                        Mat(), m_T2 * m_cam2.K());

        Mat    p3ds = m_p3ds.rowRange(static_cast<int>(begin), static_cast<int>(end));
        Vec3d *_P   = p3ds.ptr<Vec3d>();

        double _A[6 * 4], _X[4];
        Mat    A(6, 4, CV_64F, _A);
        Mat    X(4, 1, CV_64F, _X);

        size_t count = end - begin;
        for (size_t i = 0; i < count; ++i)
        {
            for (int k = 0; k < 2; ++k)
            {
                const Point2d *_p = ups[k].ptr<Point2d>();
                const double  *_M = Ms[k].ptr<double>();
                double         x  = _p[i].x, y = _p[i].y;

                _A[(k * 3 + 0) * 4 + 0] = -_M[1 * 4 + 0] + _M[2 * 4 + 0] * y;
                _A[(k * 3 + 0) * 4 + 1] = -_M[1 * 4 + 1] + _M[2 * 4 + 1] * y;
                _A[(k * 3 + 0) * 4 + 2] = -_M[1 * 4 + 2] + _M[2 * 4 + 2] * y;
                _A[(k * 3 + 0) * 4 + 3] = -_M[1 * 4 + 3] + _M[2 * 4 + 3] * y;

                _A[(k * 3 + 1) * 4 + 0] = _M[0 * 4 + 0] - _M[2 * 4 + 0] * x;
                _A[(k * 3 + 1) * 4 + 1] = _M[0 * 4 + 1] - _M[2 * 4 + 1] * x;
                _A[(k * 3 + 1) * 4 + 2] = _M[0 * 4 + 2] - _M[2 * 4 + 2] * x;
                _A[(k * 3 + 1) * 4 + 3] = _M[0 * 4 + 3] - _M[2 * 4 + 3] * x;

                _A[(k * 3 + 2) * 4 + 0] = _M[1 * 4 + 0] * x - _M[0 * 4 + 0] * y;
                _A[(k * 3 + 2) * 4 + 1] = _M[1 * 4 + 1] * x - _M[0 * 4 + 1] * y;
                _A[(k * 3 + 2) * 4 + 2] = _M[1 * 4 + 2] * x - _M[0 * 4 + 2] * y;
                _A[(k * 3 + 2) * 4 + 3] = _M[1 * 4 + 3] * x - _M[0 * 4 + 3] * y;
            }

            SVD::solveZ(A, X);

            for (int j = 0; j < 3; ++j)
            {
                _P[i][j] = _X[j] / _X[3];
            }
        }
    }

    const CameraGeometricParameters &m_cam1;
    const Mat                       &m_T1;
    const Mat                       &m_p1;
    const CameraGeometricParameters &m_cam2;
    const Mat                       &m_T2;
    const Mat                       &m_p2;
    Mat                             &m_p3ds;
};

TwoViewPointsTriangulater::~TwoViewPointsTriangulater()
{ }

template <bool alignHorizontal>
struct TwoViewDisparityCostFunction;

template <>
struct TwoViewDisparityCostFunction<true>
{
    TwoViewDisparityCostFunction(const CameraGeometricParameters &cam2,
                                 const Mat                       &K2,
                                 const Mat                       &R2)
    {
        const double *p = cam2.distorsionCoefficients().ptr<double>();
        k1 = p[0];
        k2 = p[1];
        p1 = p[2];
        p2 = p[3];

        _K2 = cam2.K().ptr<double>();
        iT  = R2.t() * K2.inv();
        _iT = iT.ptr<double>();
    }

    void setPoints(const Point2d &p1r,
                   const Point2d &p2d)
    {
        p1rx = p1r.x;
        p1ry = p1r.y;
        p2dx = p2d.x;
    }

    // Ceres driver! even though 1D minimization with ceres is not really efficient
    template <typename T>
    bool operator()(const T *const d,
                    T             *residuals) const
    {
        // p2r is (p1r.x+d, p1r.y)
        // p2u = (rK2.R2.K2^{-1})^{-1}.(p2r, 1)
        // {x,y} = (K2^{-1}.(p2u, 1)).xy = (R2^T.rK2^-1.(p2r,1)).xy
        T p2rx = T(p1rx) + d[0];
        T p2ry = T(p1ry);

        T t = T(_iT[2 * 3 + 0]) * p2rx + T(_iT[2 * 3 + 1]) * p2ry + T(_iT[2 * 3 + 2]);
        T x =
            (T(_iT[0 * 3 + 0]) * p2rx + T(_iT[0 * 3 + 1]) * p2ry +
             T(_iT[0 * 3 + 2])) / t;
        T y =
            (T(_iT[1 * 3 + 0]) * p2rx + T(_iT[1 * 3 + 1]) * p2ry +
             T(_iT[1 * 3 + 2])) / t;

        // p2d = D(K2, coeffs2, {x,y})
        T r2 = x * x + y * y;
        T r4 = r2 * r2;
        T u  = x * (T(1.0) + T(k1) * r2 + T(k2) * r4) +
               T(2.0) * T(p1) * x * y +
               T(p2) * (r2 + T(2.0) * x * x);

        T p2dx_ = T(_K2[0 * 3 + 0]) * u + T(_K2[0 * 3 + 2]);
        residuals[0] = p2dx - p2dx_;

        return true;
    }

    // Boost brent minimization driver
    template <typename T>
    T operator()(T x) const
    {
        T res;
        this->operator()( &x,
                          &res);

        return res * res;
    }

    // Boost newton minimization driver
    template <typename T>
    pair<T, T> operator()(T x,
                          int) const
    {
        const double *params[] = { &x };
        T             res;
        T             jacobian;
        T            *jacobianArray[] = { &jacobian };

        typedef ceres::internal::AutoDiff<TwoViewDisparityCostFunction<true>, double,
                                          1> AutoDiff;
        AutoDiff::Differentiate(*this, params, 1, &res, jacobianArray);

        return make_pair(res * res, 2 * jacobian * res);
    }

    Mat           iT;
    const double *_K2, *_iT;
    double        k1, k2, p1, p2;
    double        p1rx, p1ry, p2dx;
};

template <>
struct TwoViewDisparityCostFunction<false>
{
    TwoViewDisparityCostFunction(const CameraGeometricParameters &cam2,
                                 const Mat                       &K2,
                                 const Mat                       &R2)
    {
        const double *p = cam2.distorsionCoefficients().ptr<double>();
        k1 = p[0];
        k2 = p[1];
        p1 = p[2];
        p2 = p[3];

        _K2 = cam2.K().ptr<double>();
        iT  = R2.t() * K2.inv();
        _iT = iT.ptr<double>();

    }

    void setPoints(const Point2d &p1r,
                   const Point2d &p2d)
    {
        p1rx = p1r.x;
        p1ry = p1r.y;
        p2dy = p2d.y;
    }

    // Ceres driver! even though 1D minimization with ceres is not really efficient
    template <typename T>
    bool operator()(const T *const d,
                    T             *residuals) const
    {
        // p2r is (p1r.x, p1r.y+d)
        // p2u = (rK2.R2.K2^{-1})^{-1}.(p2r, 1)
        // {x,y} = (K2^{-1}.(p2u, 1)).xy = (R2^T.rK2^-1.(p2r,1)).xy
        T p2rx = T(p1rx);
        T p2ry = T(p1ry) + d[0];

        T t =
            T(_iT[2 * 3 + 0]) * p2rx + T(_iT[2 * 3 + 1]) * p2ry + T(_iT[2 * 3 + 2]);
        T x =
            (T(_iT[0 * 3 + 0]) * p2rx + T(_iT[0 * 3 + 1]) * p2ry +
             T(_iT[0 * 3 + 2])) / t;
        T y =
            (T(_iT[1 * 3 + 0]) * p2rx + T(_iT[1 * 3 + 1]) * p2ry +
             T(_iT[1 * 3 + 2])) / t;

        // p2d = D(K2, coeffs2, {x,y})
        T r2 = x * x + y * y;
        T r4 = r2 * r2;
        T v  = y * (T(1.0) + T(k1) * r2 + T(k2) * r4) +
               T(p1) * (r2 + T(2.0) * y * y) +
               T(2.0) * T(p2) * x * y;

        T p2dy_ = T(_K2[1 * 3 + 1]) * v + T(_K2[1 * 3 + 2]);
        residuals[0] = p2dy - p2dy_;

        return true;
    }

    // Boost brent minimization driver
    template <typename T>
    T operator()(T x) const
    {
        T res;
        this->operator()( &x,
                          &res);

        return res * res;
    }

    // Boost newton minimization driver
    template <typename T>
    pair<T, T> operator()(T x,
                          int) const
    {
        T res;
        T jacobian;

        typedef ceres::internal::AutoDiff<TwoViewDisparityCostFunction<false>, double,
                                          1,
                                          1> AutoDiff;
        AutoDiff::Differentiate(*this, &x, 1, &res, &jacobian);

        return res * res;
    }

    Mat           iT;
    const double *_K2, *_iT;
    double        k1, k2, p1, p2;
    double        p1rx, p1ry, p2dy;
};

// #define NO_REFINE_BRACKET

struct TwoViewWithGeometryPointsTriangulater : public ParallelForBody
{
    TwoViewWithGeometryPointsTriangulater(const CameraGeometricParameters &cam1,
                                          const CameraGeometricParameters &cam2,
                                          const Mat                       &dp1s,
                                          const Mat                       &dp2s,
                                          bool                             alignHorizontal,
                                          const Size                      &s2,
                                          const Mat                       &K1,
                                          const Mat                       &R1,
                                          const Mat                       &H1,
                                          const Mat                       &K2,
                                          const Mat                       &R2,
                                          const Mat                       &H2,
                                          const Mat                       &Q,
                                          Mat                             &Ps) :
        m_cam1(cam1),
        m_cam2(cam2),
        m_dp1s(dp1s),
        m_dp2s(dp2s),
        m_alignHorizontal(alignHorizontal),
        m_s2(s2),
        m_K1(K1),
        m_R1(R1),
        m_H1(H1),
        m_K2(K2),
        m_R2(R2),
        m_H2(H2),
        m_Q(Q),
        m_Ps(Ps)
    { }
    ~TwoViewWithGeometryPointsTriangulater();

    virtual void operator()(size_t begin,
                            size_t end) const
    {
        Mat    dp1s, dp2s, rp1s, Ps;
        double _P[4];
        Mat    P(4, 1, CV_64F, _P);

        if (m_dp1s.rows > 1)
        {
            dp1s = m_dp1s.rowRange(static_cast<int>(begin),
                                   static_cast<int>(end));
        }
        else
        {
            dp1s = m_dp1s.colRange(static_cast<int>(begin), static_cast<int>(end));
        }
        // const Point2d* _dp1s = dp1s.ptr<Point2d>();

        if (m_dp2s.rows > 1)
        {
            dp2s = m_dp2s.rowRange(static_cast<int>(begin),
                                   static_cast<int>(end));
        }
        else
        {
            dp2s = m_dp2s.colRange(static_cast<int>(begin), static_cast<int>(end));
        }
        const Point2d *_dp2s = dp2s.ptr<Point2d>();

        // 3) remove radial distorsions and applies rectification to points1
        undistortPoints(dp1s, rp1s, m_cam1.K(),
                        m_cam1.distorsionCoefficients(), m_R1, m_K1);
        Point2d *_rp1s = rp1s.ptr<Point2d>();

        const double *_H2 = m_H2.ptr<double>(); (void)_H2;
        Ps = m_Ps.rowRange(static_cast<int>(begin), static_cast<int>(end));
        Vec3d *_Ps = Ps.ptr<Vec3d>();

        size_t count = end - begin;
        // condition on outer loop for efficiency ...
        if (m_alignHorizontal)
        {
            TwoViewDisparityCostFunction<true> f(m_cam2, m_K2, m_R2);

            for (size_t i = 0; i < count; ++i)
            {
                f.setPoints(_rp1s[i], _dp2s[i]);

                // 4) compute d as
                // d = arg min || D(K2,coeffs2,H2^{-1}.{p1r.x+d, p1r.y, 1}).x - p2d.x ||^2
                // disparities can be negative if cameras look toward each other
                // d = x2-x1 and x2 >= 0 and x2 <w2 donc -x1 <= d < w2-x1
#ifdef NO_REFINE_BRACKET
                std::pair<double, double> pd = brent_find_minima(f, -_rp1s[i].x,
                                                                 m_s2.width -
                                                                 _rp1s[i].x);
                double d = pd.first;
#else
                double a, b, c, d;
                // bracketing de base
                // on résoud pour up2y approximé par dp2y dans (H2.{dp2.x,dp2.y,1}).y == rp1.y
                a = _dp2s[i].x * _H2[1 * 3 + 0] + _H2[1 * 3 + 2];
                b = _rp1s[i].y * (_dp2s[i].x * _H2[2 * 3 + 0] + _H2[2 * 3 + 2]);
                c = (_H2[1 * 3 + 1] - _rp1s[i].y * _H2[2 * 3 + 1]);
                double dp2y = -(a - b) / c;

                // ensuite on approxime d = (H2.{dp2.x, dp2.y, 1}).x - rp1.x
                a = _dp2s[i].x * _H2[0 * 3 + 0] + dp2y * _H2[0 * 3 + 1] +
                    _H2[0 * 3 + 2];
                b = _dp2s[i].x * _H2[2 * 3 + 0] + dp2y * _H2[2 * 3 + 1] +
                    _H2[2 * 3 + 2];
                d = (a / b) - _rp1s[i].x;

                // avec une borne conservatrice de 150 pixels autour ?
                std::pair<double, double> pd = brent_find_minima(f, d - 150, d + 150);
                d = pd.first;

                // avec Newton .. mais ça converge pas nécessairement plus vite :(
                // d = newton_raphson_iterate(f, d, -_rp1s[i].x, m_s2.width-_rp1s[i].x, 1e-6);
#endif

                // 5) solve Q.{rx1,ry,1,d} x= {X,Y,Z,W}
                _P[0] = _rp1s[i].x;
                _P[1] = _rp1s[i].y;
                _P[2] = 1;
                _P[3] = d;
                P     = m_Q * P;

                for (int j = 0; j < 3; ++j)
                {
                    _Ps[i][j] = _P[j] / _P[3];
                }
            }
        }
        else
        {
            TwoViewDisparityCostFunction<false> f(m_cam2, m_K2, m_R2);

            for (size_t i = 0; i < count; ++i)
            {
                f.setPoints(_rp1s[i], _dp2s[i]);

                // 4) compute d as
                // d = arg min || D(K2,coeffs2,H2^{-1}.{p1r.x, p1r.y+d, 1}).y - p2d.y ||^2
                // disparities can be negative if cameras look toward each other
                // d = y2-y1 and y2 >= 0 and y2 <h2 donc -y1 <= d < h2-y1
#ifdef NO_REFINE_BRACKET
                std::pair<double, double> pd = brent_find_minima(f, -_rp1s[i].y,
                                                                 m_s2.height -
                                                                 _rp1s[i].y);
                double d = pd.first;
#else
                double a, b, c, d;
                // bracketing de base
                // on résoud pour up2x approximé par dp2x dans (H2.{dp2.x,dp2.y,1}).x == rp1.x
                a = _dp2s[i].y * _H2[0 * 3 + 1] + _H2[0 * 3 + 2];
                b = _rp1s[i].x * (_dp2s[i].y * _H2[2 * 3 + 1] + _H2[2 * 3 + 2]);
                c = (_H2[0 * 3 + 0] - _rp1s[i].x * _H2[2 * 3 + 0]);
                double dp2x = -(a - b) / c;

                // ensuite on approxime d = (H2.{dp2.x, dp2.y, 1}).y - rp1.y
                a = dp2x * _H2[1 * 3 + 0] + _dp2s[i].y * _H2[1 * 3 + 1] +
                    _H2[1 * 3 + 2];
                b = dp2x * _H2[2 * 3 + 0] + _dp2s[i].y * _H2[2 * 3 + 1] +
                    _H2[2 * 3 + 2];
                d = (a / b) - _rp1s[i].y;

                // avec une borne conservatrice de 150 pixels autour ?
                std::pair<double, double> pd = brent_find_minima(f, d - 150, d + 150);
                d = pd.first;

                // avec Newton .. mais ça converge pas nécessairement plus vite :(
                // d = newton_raphson_iterate(f, d, -_rp1s[i].y, m_s2.height-_rp1s[i].y, 1e-6);
#endif

                // 5) solve Q.{rx1,ry,1,d} x= {X,Y,Z,W}
                _P[0] = _rp1s[i].x;
                _P[1] = _rp1s[i].y;
                _P[2] = 1;
                _P[3] = d;
                P     = m_Q * P;

                for (int j = 0; j < 3; ++j)
                {
                    _Ps[i][j] = _P[j] / _P[3];
                }
            }
        }
    }

    const CameraGeometricParameters &m_cam1;
    const CameraGeometricParameters &m_cam2;
    const Mat                       &m_dp1s;
    const Mat                       &m_dp2s;
    bool                             m_alignHorizontal;
    const Size                      &m_s2;
    const Mat                       &m_K1;
    const Mat                       &m_R1;
    const Mat                       &m_H1;
    const Mat                       &m_K2;
    const Mat                       &m_R2;
    const Mat                       &m_H2;
    const Mat                       &m_Q;
    Mat                             &m_Ps;
};

// out of line destructor
TwoViewWithGeometryPointsTriangulater::~TwoViewWithGeometryPointsTriangulater()
{ }

void triangulatePoints(const Mat                       &points1,
                       const Mat                       &points2,
                       const CameraGeometricParameters &cam1,
                       const CameraGeometricParameters &cam2,
                       Mat                             &scenePoints,
                       bool                             completeCorrespondences)
{
    size_t count = static_cast<size_t>(points1.rows * points1.cols);
    scenePoints.create(static_cast<int>(count), 1, CV_64FC3);

    if (completeCorrespondences)
    {
        double scalex1 = 2.0 / cam1.imageSize().width, scaley1 = 2.0 /
                                                                 cam1.imageSize().
                                                                 height;
        Mat T1 = (Mat_<double>(3, 3) << scalex1, 0, -1, 0, scaley1, -1, 0, 0, 1);

        double scalex2 = 2.0 / cam2.imageSize().width, scaley2 = 2.0 /
                                                                 cam2.imageSize().
                                                                 height;
        Mat T2 = (Mat_<double>(3, 3) << scalex2, 0, -1, 0, scaley2, -1, 0, 0, 1);

        TwoViewPointsTriangulater triangulater(cam1, cam2, T1, T2, points1, points2,
                                               scenePoints);
        parallel_for(0, count, triangulater);
    }
    else
    {
        Mat     c1              = -cam1.R().t() * cam1.t();
        Mat     c2              = -cam2.R().t() * cam2.t();
        Mat     u               = c2 - c1;
        double *_u              = u.ptr<double>();
        bool    alignHorizontal = std::abs(_u[0]) > std::abs(_u[1]);

        // 1) find the rectification homography and rectified projection matrices
        Mat  H1, H2, R1, R2, K1, K2;
        Size s1, s2, s;
        findRectificationTransform(cam1, cam2, true, K1, K2, R1, R2, H1, H2, s1, s2,
                                   s,
                                   alignHorizontal);

        // 2) create Q_4x4 from K1, K2, and R
        double *_K1 = K1.ptr<double>();
        double *_K2 = K2.ptr<double>();

        double _Q[4 * 4];
        Mat    Q(4, 4, CV_64F, _Q);
        Q             = Scalar::all(0);
        _Q[0 * 4 + 0] = 1;
        _Q[1 * 4 + 1] = _K1[0 * 3 + 0] / _K1[1 * 3 + 1];
        _Q[0 * 4 + 2] = -_K1[0 * 3 + 2];
        _Q[1 * 4 + 2] = -_K1[1 * 3 + 2] * _Q[1 * 4 + 1];
        _Q[2 * 4 + 2] = _K1[0 * 3 + 0];
        if (alignHorizontal)
        {
            _Q[3 * 4 + 2] = (_K2[0 * 3 + 2] - _K1[0 * 3 + 2]) *
                            _Q[0 * 4 + 0] / norm(c2, c1);
            _Q[3 * 4 + 3] = -_Q[0 * 4 + 0] / norm(c2, c1);
        }
        else
        {
            _Q[3 * 4 + 2] = (_K2[1 * 3 + 2] - _K1[1 * 3 + 2]) *
                            _Q[1 * 4 + 1] / norm(c2, c1);
            _Q[3 * 4 + 3] = -_Q[1 * 4 + 1] / norm(c2, c1);
        }

        Mat R = (R1 * cam1.R()).t();
        Mat A = Mat::eye(4, 4, CV_64F);
        R.copyTo(A(Range(0, 3), Range(0, 3)));
        c1.copyTo(A(Range(0, 3), Range(3, 4)));
        Q = A * Q;

        // 3) Compute P as Q.{rp1.x, rp1.y, 1, d} by finding d first
        TwoViewWithGeometryPointsTriangulater triangulater(cam1, cam2, points1,
                                                           points2,
                                                           alignHorizontal, s2,
                                                           K1, R1, H1,
                                                           K2, R2, H2,
                                                           Q, scenePoints);
        parallel_for(0, count, triangulater);
    }
}

Mat normalizationMatrix(const Mat &pts)
{
    int    count = pts.rows * pts.cols;
    Scalar m     = mean(pts);
    Mat    ln    = pts.clone() - m;

    double md = 0.;
    for (int i = 0; i < count; ++i)
    {
        if (ln.rows == 1) { md += norm(ln.col(i)); }
        else { md += norm(ln.row(i)); }
    }
    md /= count;

    int    n = pts.channels();
    double s = sqrt(static_cast<double>(n)) / md;

    Mat T = Mat::eye(n + 1, n + 1, CV_64F);
    for (int i = 0; i < n; ++i)
    {
        T.at<double>(i, i) = s;
        T.at<double>(i, n) = -s * m[i];
    }

    return T;
}

double pointDepth(const Mat     &M,
                  const Point3d &X)
{

    double _P[4], _p[3];
    Mat    P(4, 1, CV_64F, _P);
    Mat    p(3, 1, CV_64F, _p);
    Mat    S = M(Range(0, 3), Range(0, 3));
    double d = determinant(S);
    double n = norm(S.row(2));
    _P[0] = X.x; _P[1] = X.y; _P[2] = X.z; _P[3] = 1;
    p     = M * P;
    int sign = (d > 0 ? 1 : d < 0 ? -1 : 0);

    return (sign * _p[2]) / (_P[3] * n);
}

void poseFromEssentialMatrix(const Mat &E,
                             const Mat &K1,
                             const Mat &K2,
                             const Mat &imagePoints1,
                             const Mat &imagePoints2,
                             Size       camSize1,
                             Size       camSize2,
                             Mat       &R,
                             Mat       &t)
{
    double _W[] = { 0, -1, 0, 1, 0, 0, 0, 0, 1 };
    double _S[3 * 3], _V[3], _D[3 * 3];
    Mat    W(3, 3, CV_64F, _W);
    Mat    S(3, 3, CV_64F, _S);
    Mat    V(3, 1, CV_64F, _V);
    Mat    D(3, 3, CV_64F, _D);
    LSVD::compute(E, V, S, D);
    D = D.t();
    if ((determinant(S) < 0) && (determinant(D) < 0))
    {
        S = -S; D = -D;
    }
    if ((determinant(S) < 0) && (determinant(D) > 0))
    {
        S = -S *W.t(); D = D * W;
    }
    if ((determinant(S) > 0) && (determinant(D) < 0))
    {
        S = S * W.t(); D = -D * W;
    }

    double _RTs[4 * 3 * 4];
    Mat    RTs[4] = {
        Mat(3, 4, CV_64F, _RTs),
        Mat(3, 4, CV_64F, _RTs + 1 * 3 * 4),
        Mat(3, 4, CV_64F, _RTs + 2 * 3 * 4),
        Mat(3, 4, CV_64F, _RTs + 3 * 3 * 4)
    };
    Mat    RTs3x3[4], RTs3x1[4];
    for (int i = 0; i < 4; ++i)
    {
        RTs3x3[i] = RTs[i](Range::all(), Range(0, 3));
        RTs3x1[i] = RTs[i](Range::all(), Range(3, 4));
    }
    Mat S3x1 = S(Range::all(), Range(2, 3));
    RTs3x3[0] = S * W * D.t();     S3x1.copyTo(RTs3x1[0]);
    RTs3x3[1] = S * W * D.t();     RTs3x1[1] = -S3x1;
    RTs3x3[2] = S * W.t() * D.t(); S3x1.copyTo(RTs3x1[2]);
    RTs3x3[3] = S * W.t() * D.t(); RTs3x1[3] = -S3x1;

    size_t      nbpts = min(static_cast<size_t>(5000), static_cast<size_t>(imagePoints1.rows));
    vector<Mat> imagePoints(2);
    imagePoints[0] = Mat(static_cast<int>(nbpts), 1, CV_64FC2);
    imagePoints[1] = Mat(static_cast<int>(nbpts), 1, CV_64FC2);
    Point2d *pp1 = imagePoints[0].ptr<Point2d>();
    Point2d *pp2 = imagePoints[1].ptr<Point2d>();

    vector<size_t> pos(nbpts);
    for (size_t i = 0; i < nbpts; ++i)
    {
        pos[i] = i;
    }
    random_shuffle(pos.begin(), pos.end());

    for (size_t i = 0; i < nbpts; ++i)
    {
        pp1[i] = imagePoints1.at<Point2d>(static_cast<int>(pos[i]));
        pp2[i] = imagePoints2.at<Point2d>(static_cast<int>(pos[i]));
    }

    double _P1[12];
    double _P2[12];
    Mat    P1(3, 4, CV_64F, _P1),
    P2(3, 4, CV_64F, _P2);
    P1 = K1 * Mat::eye(3, 4, CV_64F);

    Mat                       scenePoints;
    CameraGeometricParameters cam1, cam2;
    cam1.setMatrix(P1);
    cam2.setMatrix(P2);
    cam1.setImageSize(camSize1);
    cam2.setImageSize(camSize2);

    int pair     = 0;
    int maxCount = 0;
    int count;

    for (int k = 0; k < 4; ++k)
    {
        P2 = K2 * RTs[k];
        // triangulatePoints(imagePoints, cameras, vMask, camSize1, scenePoints);
        triangulatePoints(imagePoints[0], imagePoints[1], cam1, cam2, scenePoints);

        count = 0;
        MatIterator_<Point3d> itP3d = scenePoints.begin<Point3d>();
        for (size_t i = 0; i < nbpts; ++i, ++itP3d)
        {
            if ((pointDepth(P2, *itP3d) > 0) && (pointDepth(P1, *itP3d) > 0))
            {
                count++;
            }
        }

        if (count > maxCount)
        {
            maxCount = count;
            pair     = k;
        }
    }

    Mat &RT = RTs[pair];
    R.create(3, 3, CV_64F);
    t.create(3, 1, CV_64F);
    Mat RT3x3 = RT(Range::all(), Range(0, 3));
    Mat RT1x3 = RT(Range::all(), Range(3, 4));
    RT3x3.copyTo(R);
    RT1x3.copyTo(t);
}

void essentialFromFundamentalMatrix(const Mat &F,
                                    const Mat &K1,
                                    const Mat &K2,
                                    Mat       &E)
{
    E.create(3, 3, CV_64F);
    E = K2.t() * F * K1;

    // normalization by SVD ?
    // double _S[]={1, 0, 0, 0, 1, 0, 0, 0, 0};
    // Mat S(3, 3, CV_64F, _S);
    // LSVD lsvd(E);
    // E = lsvd.u*S*lsvd.vt;
}

bool decompositionKRT(const Mat &M,
                      Mat       &K,
                      Mat       &R,
                      Mat       &t)
{
    const Mat M3x3 = M(Range(0, 3), Range(0, 3));
    const Mat M3x1 = M(Range(0, 3), Range(3, 4));
    decompositionRQ(M3x3, K, R);

    if (K.at<double>(2, 2) < 0)
    {
        K *= -1;
        R *= -1;
    }
    if (K.at<double>(0, 0) < 0)
    {
        K.col(0) *= -1;
        R.row(0) *= -1;
    }
    if (K.at<double>(1, 1) < 0)
    {
        K.col(1) *= -1;
        R.row(1) *= -1;
    }
    if (determinant(R) < 0)
    {
        R *= -1;
        K *= -1;
    }

    assert(norm(K * R - M3x3) < 1E-10);
    assert(fabs(determinant(R) - 1) < 1E-10);

    t = K.inv() * M3x1;

    if (fabs(K.at<double>(2, 2)) < std::numeric_limits<double>::epsilon())
    {
        return false;
    }

    K /= K.at<double>(2, 2);

    if ((K.at<double>(0, 0) < 0) || (K.at<double>(1, 1) < 0))
    {
        return false;
    }

    return true;
}

void findExactHomographyMatrix(const Mat &points1,
                               const Mat &points2,
                               Mat       &hmatrix)
{
    assert((points1.cols == 1 || points1.rows == 1) &&
           (points1.cols * points1.rows == points2.rows * points2.cols));
    assert(points1.rows * points1.cols == 4);

    const Point2d *p1 = points1.ptr<Point2d>(), *p2 = points2.ptr<Point2d>();
    Mat            A(2 * 4, 9, CV_64F), Ai;
    A = Scalar::all(0);

    for (int i = 0; i < 4; i++)
    {
        double x0 = p1[i].x, y0 = p1[i].y;
        double x1 = p2[i].x, y1 = p2[i].y;

        double _r1[] = { 0, 0, 0, -x0, -y0, -1, x0 * y1, y0 * y1, y1 };
        Mat    r1(1, 9, CV_64F, _r1);
        Ai = A.row(i * 2);
        r1.copyTo(Ai);
        double _r2[] = { x0, y0, 1, 0, 0, 0, -x0 * x1, -y0 * x1, -x1 };
        Mat    r2(1, 9, CV_64F, _r2);
        Ai = A.row(i * 2 + 1);
        r2.copyTo(Ai);
    }

    LSVD lsvd(A, LSVD::MODIFY_A | LSVD::FULL_UV);

    Mat H = Mat(3, 3, CV_64F, lsvd.vt().ptr<double>(8));
    hmatrix.create(3, 3, CV_64F);
    H.copyTo(hmatrix);
}

void findExactFundamentalMatrix(const Mat &points1,
                                const Mat &points2,
                                Mat       &fmatrix)
{
    assert((points1.cols == 1 || points1.rows == 1) &&
           (points1.cols * points1.rows == points2.rows * points2.cols));

    double         _A[7 * 9], _coeffs[4], _roots[3];
    Mat            A(7, 9, CV_64F, _A);
    Mat            coeffs(1, 4, CV_64F, _coeffs);
    Mat            roots(1, 3, CV_64F, _roots);
    const Point2d *p1 = points1.ptr<Point2d>(), *p2 = points2.ptr<Point2d>();
    LSVD           lsvd;

    // form a linear system: i-th row of A(=a) represents
    // the equation: (m2[i], 1)'*F*(m1[i], 1) = 0
    for (int i = 0; i < 7; i++)
    {
        double x0 = p1[i].x, y0 = p1[i].y;
        double x1 = p2[i].x, y1 = p2[i].y;

        _A[i * 9 + 0] = x1 * x0;
        _A[i * 9 + 1] = x1 * y0;
        _A[i * 9 + 2] = x1;
        _A[i * 9 + 3] = y1 * x0;
        _A[i * 9 + 4] = y1 * y0;
        _A[i * 9 + 5] = y1;
        _A[i * 9 + 6] = x0;
        _A[i * 9 + 7] = y0;
        _A[i * 9 + 8] = 1;
    }

    // A*(f11 f12 ... f33)' = 0 is singular (7 equations for 9 variables), so
    // the solution is linear subspace of dimensionality 2.
    // => use the last two singular vectors as a basis of the space
    // (according to SVD properties)
    /*svd(A, SVD::MODIFY_A | SVD::FULL_UV);
     *   double *f1 = svd.vt.ptr<double>(7), *f2 = svd.vt.ptr<double>(8);*/
    lsvd(A, SVD::MODIFY_A | SVD::FULL_UV);
    double *f1 = lsvd.vt().ptr<double>(7), *f2 = lsvd.vt().ptr<double>(8);

    // f1, f2 is a basis => lambda*f1 + mu*f2 is an arbitrary f. matrix.
    // as it is determined up to a scale, normalize lambda & mu (lambda + mu =
    // 1), so f ~ lambda*f1 + (1 - lambda)*f2.
    // use the additional constraint det(f) = det(lambda*f1 + (1-lambda)*f2) to
    // find lambda.
    // it will be a cubic equation.
    // find c - polynomial coefficients.
    for (int i = 0; i < 9; i++)
    {
        f1[i] -= f2[i];
    }

    double t0, t1, t2;
    t0 = f2[4] * f2[8] - f2[5] * f2[7];
    t1 = f2[3] * f2[8] - f2[5] * f2[6];
    t2 = f2[3] * f2[7] - f2[4] * f2[6];

    _coeffs[3] = f2[0] * t0 - f2[1] * t1 + f2[2] * t2;

    _coeffs[2] = f1[0] * t0 - f1[1] * t1 + f1[2] * t2 -
                 f1[3] * (f2[1] * f2[8] - f2[2] * f2[7]) +
                 f1[4] * (f2[0] * f2[8] - f2[2] * f2[6]) -
                 f1[5] * (f2[0] * f2[7] - f2[1] * f2[6]) +
                 f1[6] * (f2[1] * f2[5] - f2[2] * f2[4]) -
                 f1[7] * (f2[0] * f2[5] - f2[2] * f2[3]) +
                 f1[8] * (f2[0] * f2[4] - f2[1] * f2[3]);

    t0 = f1[4] * f1[8] - f1[5] * f1[7];
    t1 = f1[3] * f1[8] - f1[5] * f1[6];
    t2 = f1[3] * f1[7] - f1[4] * f1[6];

    _coeffs[1] = f2[0] * t0 - f2[1] * t1 + f2[2] * t2 -
                 f2[3] * (f1[1] * f1[8] - f1[2] * f1[7]) +
                 f2[4] * (f1[0] * f1[8] - f1[2] * f1[6]) -
                 f2[5] * (f1[0] * f1[7] - f1[1] * f1[6]) +
                 f2[6] * (f1[1] * f1[5] - f1[2] * f1[4]) -
                 f2[7] * (f1[0] * f1[5] - f1[2] * f1[3]) +
                 f2[8] * (f1[0] * f1[4] - f1[1] * f1[3]);

    _coeffs[0] = f1[0] * t0 - f1[1] * t1 + f1[2] * t2;

    // solve the cubic equation; there can be 1 to 3 roots ...
    solveCubic(coeffs, roots);

    fmatrix.create(3, 3, CV_64F);
    double *ptr = fmatrix.ptr<double>();

    // for each root form the fundamental matrix
    double lambda = _roots[0], mu = 1.;
    double s      = f1[8] * _roots[0] + f2[8];

    // normalize each matrix, so that F(3,3) (~fmatrix[8]) == 1
    if (fabs(s) > numeric_limits<double>::epsilon())
    {
        mu      = 1. / s;
        lambda *= mu;
        ptr[8]  = 1.;
    }
    else
    {
        ptr[8] = 0.;
    }

    for (int i = 0; i < 8; i++)
    {
        ptr[i] = f1[i] * lambda + f2[i] * mu;
    }
}

void findExactResectionMatrix(const Mat &points1,
                              const Mat &points2,
                              Mat       &mmatrix)
{
    assert((points1.cols == 1 || points1.rows == 1) &&
           (points1.cols * points1.rows == points2.rows * points2.cols));
    assert(points1.rows * points1.cols == 6);

    const Point3d *p1 = points1.ptr<Point3d>();
    const Point2d *p2 = points2.ptr<Point2d>();
    Mat            A(11, 12, CV_64F), Ai;
    A = Scalar::all(0);

    for (int i = 0; i < 6; i++)
    {
        double x0 = p1[i].x, y0 = p1[i].y, z0 = p1[i].z;
        double x1 = p2[i].x, y1 = p2[i].y;

        double _r1[] =
        { 0, 0, 0, 0, -x0, -y0, -z0, -1, x0 * y1, y1 * y0, y1 * z0, y1 };
        Mat r1(1, 12, CV_64F, _r1);
        Ai = A.row(i * 2);
        r1.copyTo(Ai);
        if (i == 5)
        {
            break; // only 5.5 points needed to estimate M

        }
        double _r2[] =
        { x0, y0, z0, 1, 0, 0, 0, 0, -x1 * x0, -x1 * y0, -x1 * z0, -x1 };
        Mat r2(1, 12, CV_64F, _r2);
        Ai = A.row(i * 2 + 1);
        r2.copyTo(Ai);
    }

    LSVD lsvd(A, LSVD::MODIFY_A | LSVD::FULL_UV);

    Mat M = Mat(3, 4, CV_64F, lsvd.vt().ptr<double>(11));
    mmatrix.create(3, 4, CV_64F);
    M.copyTo(mmatrix);
}

static Rect boundingBox(Size       size,
                        const Mat &K1,
                        const Mat &coeffs,
                        const Mat &R,
                        const Mat &K2,
                        Mat       &box)
{
    float _ps[8] =
    { 0.0f, 0.0f, size.width - 1.0f, 0.0f, size.width - 1.0f, size.height - 1.0f, 0.f,
      size.height - 1.0f };
    box.create(4, 1, CV_32FC2);
    undistortPoints(Mat(4, 1, CV_32FC2, _ps), box, K1, coeffs, R, K2);

    return boundingRect(box);
}

// Implementation of Fusiello
void findRectificationTransform(const CameraGeometricParameters &cam1,
                                const CameraGeometricParameters &cam2,
                                bool                             useK1,
                                cv::Mat                         &K1,
                                cv::Mat                         &K2,
                                cv::Mat                         &R1,
                                cv::Mat                         &R2,
                                cv::Mat                         &H1,
                                cv::Mat                         &H2,
                                Size                            &s1,
                                Size                            &s2,
                                cv::Size                        &commonSize,
                                bool                             alignHorizontalEpipolarLines)
{
    // camera centers
    double _c1[3], _c2[3];
    Mat    c1(3, 1, CV_64F, _c1);
    Mat    c2(3, 1, CV_64F, _c2);
    c1 = -cam1.R().t() * cam1.t();
    c2 = -cam2.R().t() * cam2.t();

    // axes of the rotation
    double _vs[3 * 3];
    Mat    v1(3, 1, CV_64F, _vs + 0);
    Mat    v2(3, 1, CV_64F, _vs + 3);
    Mat    v3(3, 1, CV_64F, _vs + 6);
    if (alignHorizontalEpipolarLines)
    {
        v1  = (c2 - c1);
        v1 /= norm(v1);
        cam1.R() (Range(2, 3), Range::all()).t().cross(v1).copyTo(v2);
        v2 /= norm(v2);
    }
    else
    {
        v2  = (c2 - c1);
        v2 /= norm(v2);
        v2.cross(cam1.R() (Range(2, 3), Range::all()).t()).copyTo(v1);
        v1 /= norm(v1);
    }
    v1.cross(v2).copyTo(v3);
    v3 /= norm(v3);

    // the rotation
    Mat R = Mat(3, 3, CV_64F, _vs);
    R1 = R * cam1.R().t();
    R2 = R * cam2.R().t();

    if (useK1)
    {
        cam1.K().copyTo(K1);
        cam1.K().copyTo(K2);
    }
    else
    {
        cam2.K().copyTo(K1);
        cam2.K().copyTo(K2);
    }

    Mat  b1, b2;
    Rect r1 = boundingBox(cam1.imageSize(), cam1.K(),
                          cam1.distorsionCoefficients(), R1, K1, b1);
    Rect r2 = boundingBox(cam2.imageSize(), cam2.K(),
                          cam2.distorsionCoefficients(), R2, K2, b2);
    Mat bs(8, 1, CV_32FC2);
    b1.copyTo(bs.rowRange(0, 4));
    b2.copyTo(bs.rowRange(4, 8));
    Rect r = boundingRect(bs);
    commonSize = r.size();
    if (alignHorizontalEpipolarLines)
    {
        s1 = Size(r1.width, r1.height - (r.y - r1.y));
        s2 = Size(r2.width, r2.height - (r.y - r2.y));
    }
    else
    {
        s1 = Size(r1.width - (r.x - r1.x), r1.height);
        s2 = Size(r2.width - (r.x - r2.x), r2.height);
    }

    double *_K1 = K1.ptr<double>();
    double *_K2 = K2.ptr<double>();
    // adjusting for the top (resp. leftmost) epipolar line
    if (alignHorizontalEpipolarLines)
    {
        _K1[0 * 3 + 2] -= r1.x;
        _K1[1 * 3 + 2] -= r.y;
        _K2[0 * 3 + 2] -= r2.x;
        _K2[1 * 3 + 2] -= r.y;
    }
    else
    {
        _K1[0 * 3 + 2] -= r.x;
        _K1[1 * 3 + 2] -= r1.y;
        _K2[0 * 3 + 2] -= r.x;
        _K2[1 * 3 + 2] -= r2.y;
    }

    // homography rectifying each frames
    H1 = K1 * R1 * cam1.K().inv();
    H2 = K2 * R2 * cam2.K().inv();
}
