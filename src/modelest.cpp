/*
 * MVG - A library to solve multiple view geometry problems.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of MVG.
 *
 * MVG is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MVG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MVG.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This file comes from OpenCV's implementation
 */

#include "modelest.h"
#include <iostream>
#include "mvg.h"

using namespace std;
using namespace cv;

struct ModelEstimator::Private
{
    Private(int  modelPoints_,
            Size modelSize_,
            int  maBasicSolutions_);
    ~Private();

    cv::RNG  rng;
    size_t   modelPoints;
    cv::Size modelSize;
    int      maxBasicSolutions;
    bool     optimize;
};

ModelEstimator::Private::Private(int  modelPoints_,
                                 Size modelSize_,
                                 int  maxBasicSolutions_) :
    rng(RNG()),
    modelPoints(static_cast<size_t>(modelPoints_)),
    modelSize(modelSize_),
    maxBasicSolutions(maxBasicSolutions_),
    optimize(true)
{ }

ModelEstimator::Private::~Private()
{ }

ModelEstimator::ModelEstimator(int  modelPoints,
                               Size modelSize,
                               int  maxBasicSolutions)
{
    m_privImpl = new Private(modelPoints, modelSize, maxBasicSolutions);
}

ModelEstimator::~ModelEstimator()
{
    delete m_privImpl;
}

void ModelEstimator::setSeed(int64 seed)
{
    m_privImpl->rng = RNG(static_cast<uint64>(seed));
}

int ModelEstimator::findInliers(const Mat      &m1,
                                const Mat      &m2,
                                const Mat      &model,
                                vector<double> &errors,
                                vector<bool>   &mask,
                                double          threshold)
{
    size_t count     = errors.size();
    int    goodCount = 0;

    computeReprojError(m1, m2, model, errors);

    threshold *= threshold;
    for (size_t i = 0; i < count; i++)
    {
        bool inlier = (errors[i] <= threshold);
        mask[i]    = inlier;
        goodCount += inlier;
    }

    return goodCount;
}

static int RANSACUpdateNumIters(double p,
                                double ep,
                                int    modelPoints,
                                int    maxIters)
{
    /*
     *   if( modelPoints <= 0 )
     *   CV_Error( CV_StsOutOfRange, "the number of model points should be positive" );
     */

    p  = max(p, 0.);
    p  = min(p, 1.);
    ep = max(ep, 0.);
    ep = min(ep, 1.);

    // avoid inf's & nan's
    double num   = max(1. - p, numeric_limits<double>::min());
    double denom = 1. - pow(1. - ep, modelPoints);
    if (denom < numeric_limits<double>::min())
    {
        return 0;
    }

    num   = log(num);
    denom = log(denom);

    return denom >= 0 || -num >= maxIters * (-denom)
           ? maxIters : cvRound(num / denom);
}

bool ModelEstimator::runRANSAC(const Mat    &m1,
                               const Mat    &m2,
                               Mat          &model,
                               vector<bool> &mask,
                               double        threshold,
                               double        confidence,
                               int           maxIters)
{
    int    iter, niters = maxIters;
    size_t count        = static_cast<size_t>(m1.rows * m1.cols);
    size_t maxGoodCount = 0;
    // CV_Assert( CV_ARE_SIZES_EQ(m1, m2) && CV_ARE_SIZES_EQ(m1, mask) );

    if (count < m_privImpl->modelPoints)
    {
        return false;
    }

    Mat models = Mat(m_privImpl->modelSize.height * m_privImpl->maxBasicSolutions,
                     m_privImpl->modelSize.width, CV_64F);
    Mat            ms1, ms2;
    vector<double> errors(count);
    vector<bool>   tmask(count);

    if (count > m_privImpl->modelPoints)
    {
        ms1 = Mat(1, static_cast<int>(m_privImpl->modelPoints), m1.type());
        ms2 = Mat(1, static_cast<int>(m_privImpl->modelPoints), m2.type());
    }
    else
    {
        niters = 1;
        ms1    = m1.clone();
        ms2    = m2.clone();
    }

    for (iter = 0; iter < niters; iter++)
    {
        size_t goodCount;
        int    nmodels;
        if (count > m_privImpl->modelPoints)
        {
            bool found = getSubset(m1, m2, ms1, ms2, 300);
            if (!found)
            {
                if (iter == 0)
                {
                    return false;
                }
                break;
            }
        }

        nmodels = runKernel(ms1, ms2, models);
        if (nmodels <= 0)
        {
            continue;
        }

        for (int i = 0; i < nmodels; i++)
        {
            Mat modelI = models.rowRange(i * m_privImpl->modelSize.height,
                                         (i + 1) * m_privImpl->modelSize.height);
            goodCount = static_cast<size_t>(findInliers(m1, m2, modelI, errors,
                                                        tmask, threshold));

            if (goodCount > max(maxGoodCount, m_privImpl->modelPoints - 1))
            {
                swap(tmask, mask);
                modelI.copyTo(model);
                maxGoodCount = goodCount;
                niters       =
                    RANSACUpdateNumIters(confidence,
                                         double(count - goodCount) / count,
                                         static_cast<int>(m_privImpl->modelPoints),
                                         niters);
            }
        }
    }

    // stabilizes the solution by a last run
    Mat cm1 = m1.clone(), cm2 = m2.clone();
    compressPoints(cm1, cm2, mask);

    /*cout << model << endl;
     *   cout << "count = " << maxGoodCount << endl;*/
    runLSKernel(cm1, cm2, model);
    /*maxGoodCount = findInliers(m1, m2, model, errors, mask, threshold);
     *   cout << model << endl;
     *   cout << "count = " << maxGoodCount << endl;*/
    /*cout << m1 << endl;
     *   cout << m2 << endl;
     *   cout << model << endl;
     *   for (int z = 0; z < mask.size(); ++z) {
     *    cout << mask[z] << ",";
     *   }
     *   cout << endl;*/

    if (m_privImpl->optimize)
    {
        size_t goodCount;
        do
        {
            goodCount = maxGoodCount;

            if (!runOptimization(m1, m2, model, mask))
            {
                return 0;
            }
            maxGoodCount =
                static_cast<size_t>(findInliers(m1, m2, model, errors, mask, threshold));
            // cout << model << endl;
            /*cout << m1 << endl;
             *    cout << m2 << endl;*/
            // cout << maxGoodCount << endl;

            /*m1.copyTo(cm1); m2.copyTo(cm2);
             *   int n = compressPoints(cm1, cm2, mask);
             *   runLSKernel(cm1, cm2, camSize, model);*/
        }
        while (goodCount != maxGoodCount);
    }
    // cout << model << endl;

    return maxGoodCount >= m_privImpl->modelPoints;
}

bool ModelEstimator::runLMeDS(const Mat    &m1,
                              const Mat    &m2,
                              Mat          &model,
                              vector<bool> &mask,
                              double        confidence,
                              int           maxIters)
{
    const double outlierRatio = 0.45;

    int    iter, niters = maxIters;
    size_t count     = static_cast<size_t>(m1.rows * m1.cols);
    double minMedian = numeric_limits<double>::max(), sigma = 0.001;

    //    CV_Assert( CV_ARE_SIZES_EQ(m1, m2) && CV_ARE_SIZES_EQ(m1, mask) );

    if (count < m_privImpl->modelPoints)
    {
        return false;
    }

    Mat models = Mat(m_privImpl->modelSize.height * m_privImpl->maxBasicSolutions,
                     m_privImpl->modelSize.width, CV_64F);
    Mat            ms1, ms2;
    vector<double> errors(count);
    vector<bool>   tmask(count);

    if (count > m_privImpl->modelPoints)
    {
        ms1 = Mat(1, static_cast<int>(m_privImpl->modelPoints), m1.type());
        ms2 = Mat(1, static_cast<int>(m_privImpl->modelPoints), m2.type());
    }
    else
    {
        niters = 1;
        ms1    = m1.clone();
        ms2    = m2.clone();
    }

    niters =
        cvRound(log(1 - confidence) /
                log(1 - pow(1 - outlierRatio, double(m_privImpl->modelPoints))));
    niters = min(max(niters, 3), maxIters);

    for (iter = 0; iter < niters; iter++)
    {
        int nmodels;
        if (count > m_privImpl->modelPoints)
        {
            bool found = getSubset(m1, m2, ms1, ms2, 300);
            if (!found)
            {
                if (iter == 0)
                {
                    return false;
                }
                break;
            }
        }

        nmodels = runKernel(ms1, ms2, models);
        if (nmodels <= 0)
        {
            continue;
        }

        for (int i = 0; i < nmodels; i++)
        {
            Mat modelI = models.rowRange(i * m_privImpl->modelSize.height,
                                         (i + 1) * m_privImpl->modelSize.height);
            computeReprojError(m1, m2, modelI, errors);

            // this is middle for odd-length, and "upper-middle" for even length
            vector<double>::iterator middle = errors.begin() +
                                              (errors.end() - errors.begin()) / 2;

            // This function runs in O(n) on average, according to the standard
            nth_element(errors.begin(), middle, errors.end());

            double median;
            if ((errors.end() - errors.begin()) % 2 != 0)   // odd length
            {
                median = *middle;
            }
            else   // even length
            { // the "lower middle" is the max of the lower half
                vector<double>::iterator lower_middle =
                    max_element(errors.begin(), middle);
                median = (*middle + *lower_middle) / 2.0;
            }

            if (median < minMedian)
            {
                minMedian = median;
                modelI.copyTo(model);
            }
        }
    }

    if (minMedian < numeric_limits<double>::max())
    {
        sigma = 2.5 * 1.4826 * (1 + 5. / (count - m_privImpl->modelPoints)) *
                sqrt(minMedian);
        sigma = max(sigma, 0.001);

        count = static_cast<size_t>(findInliers(m1, m2, model, errors, mask, sigma));
    }

    // stabilizes the solution by a last run
    Mat cm1 = m1.clone(), cm2 = m2.clone();
    compressPoints(cm1, cm2, mask);
    runLSKernel(cm1, cm2, model);

    if (m_privImpl->optimize)
    {
        size_t goodCount;
        do
        {
            goodCount = count;

            if (!runOptimization(m1, m2, model, mask))
            {
                return 0;
            }
            count = static_cast<size_t>(findInliers(m1, m2, model, errors, mask, sigma));

            /*m1.copyTo(cm1); m2.copyTo(cm2);
             *   int n = compressPoints(cm1, cm2, mask);
             *   runLSKernel(cm1, cm2, camSize, model);*/
        }
        while (goodCount != count);
    }

    return count >= m_privImpl->modelPoints;
}

int ModelEstimator::compressPoints(Mat                &m1,
                                   Mat                &m2,
                                   const vector<bool> &mask)
{
    assert((m1.cols == 1 || m1.rows == 1) &&
           (m1.cols * m1.rows == m2.rows * m2.cols));

    Point2d *m1ptr = m1.ptr<Point2d>(), *m2ptr = m2.ptr<Point2d>();
    size_t   count = static_cast<size_t>(m1.cols * m1.rows);

    int j = 0;
    for (size_t i = 0; i < count; ++i)
    {
        if (mask[i])
        {
            m1ptr[j] = m1ptr[i];
            m2ptr[j] = m2ptr[i];
            ++j;
        }
    }

    if (m1.cols == 1)
    {
        m1.rows = m2.rows = j;
    }
    else
    {
        m1.cols = m2.cols = j;
    }

    return j;
}

bool ModelEstimator::optimize() const
{
    return m_privImpl->optimize;
}

void ModelEstimator::setOptimize(bool optimize)
{
    m_privImpl->optimize = optimize;
}

cv::RNG& ModelEstimator::rng()
{
    return m_privImpl->rng;
}

int ModelEstimator::modelPoints() const
{
    return static_cast<int>(m_privImpl->modelPoints);
}

int ModelEstimator::maxBasicSolutions() const
{
    return m_privImpl->maxBasicSolutions;
}

cv::Size ModelEstimator::modelSize() const
{
    return m_privImpl->modelSize;
}

bool ModelEstimator::getSubset(const Mat &m1,
                               const Mat &m2,
                               Mat       &ms1,
                               Mat       &ms2,
                               int        maxAttempts)
{
    vector<int>    idx(m_privImpl->modelPoints);
    const Point2d *m1ptr  = m1.ptr<Point2d>(), *m2ptr = m2.ptr<Point2d>();
    Point2d       *ms1ptr = ms1.ptr<Point2d>(), *ms2ptr = ms2.ptr<Point2d>();
    int            count  = m1.cols * m1.rows;

    size_t i;
    int    iters;
    // assert( CV_IS_MAT_CONT(m1->type & m2->type) && (elemSize % sizeof(int) == 0) );
    for (iters = 0, i = 0; i < m_privImpl->modelPoints && iters < maxAttempts;)
    {
        idx[i] = static_cast<int>(m_privImpl->rng(static_cast<unsigned int>(count)));

        vector<int>::iterator idxE = idx.begin() + static_cast<int>(i);
        if (find(idx.begin(), idxE, idx[i]) != idxE) { continue; }

        ms1ptr[i] = m1ptr[idx[i]];
        ms2ptr[i] = m2ptr[idx[i]];

        if (!checkSubset(ms1, static_cast<int>(i + 1)) ||
            !checkSubset(ms2, static_cast<int>(i + 1)))
        {
            iters++;
            continue;
        }

        i++;
    }

    return i == m_privImpl->modelPoints && iters < maxAttempts;
}

bool ModelEstimator::checkSubset(const Mat &ms,
                                 int        count)
{
    const Point2d *p = ms.ptr<Point2d>();

    // assert( CV_MAT_TYPE(m->type) == CV_64FC2 );
    int id = count - 1;
    // check that the id-th selected point does not belong
    // to a line connecting some previously selected points
    int j, k;
    for (j = 0; j < id; j++)
    {
        double dx1 = p[j].x - p[id].x;
        double dy1 = p[j].y - p[id].y;

        for (k = 0; k < j; k++)
        {
            double dx2 = p[k].x - p[id].x;
            double dy2 = p[k].y - p[id].y;

            if (fabs(dx2 * dy1 - dy2 * dx1) <=
                numeric_limits<double>::epsilon() * (fabs(dx1) + fabs(dy1) +
                                                     fabs(dx2) + fabs(dy2)))
            {
                break;
            }
        }
        if (k < j)
        {
            break;
        }
    }

    return j == id;
}

