#
# MVG - A library to solve multiple view geometry problems.
# Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
#
# This file is part of MVG.
#
# MVG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# MVG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MVG.  If not, see <http://www.gnu.org/licenses/>.
#

# Config file for MVG - Find MVG & dependencies.
#
# This file is used by CMake when FIND_PACKAGE( MVG ) is invoked (and
# the directory containing this file is present in CMAKE_MODULE_PATH).
#
# This module defines the following variables:
#
# MVG_FOUND : True iff MVG has been successfully found.
# MVG_VERSION: Version of MVG found.
# MVG_INCLUDE_DIRS: Include directories for MVG and the dependencies which
#                     appear in the MVG public API and are thus required to
#                     use MVG.
# MVG_LIBRARIES: Libraries for MVG and all dependencies against which MVG
#                  was compiled. This will not include any optional dependencies
#                  that were disabled when MVG was compiled.
#

#
# File inspired by Ceres Solver (http://code.google.com/p/ceres-solver)
#

#
# Called if we failed to find MVG or any of it's required dependencies,
# unsets all public (designed to be used externally) variables and reports
# error message at priority depending upon [REQUIRED/QUIET/<NONE>] argument.
#
MACRO(MVG_REPORT_NOT_FOUND REASON_MSG)
  # FindPackage() only references MVG_FOUND, and requires it to be explicitly
  # set FALSE to denote not found (not merely undefined).
  SET(MVG_FOUND FALSE)
  UNSET(MVG_INCLUDE_DIRS)
  UNSET(MVG_LIBRARIES)

  # Reset the CMake module path to its state when this script was called.
  SET(CMAKE_MODULE_PATH ${MVG_CMAKE_MODULE_PATH})

  # Note <package>_FIND_[REQUIRED/QUIETLY] variables defined by FindPackage()
  # use the camelcase library name, not uppercase.
  IF (MVG_FIND_QUIETLY)
    MESSAGE(STATUS "Failed to find MVG - " ${REASON_MSG} ${ARGN})
  ELSE (MVG_FIND_REQUIRED)
    MESSAGE(FATAL_ERROR "Failed to find MVG - " ${REASON_MSG} ${ARGN})
  ELSE()
    # Neither QUIETLY nor REQUIRED, use SEND_ERROR which emits an error
    # that prevents generation, but continues configuration.
    MESSAGE(SEND_ERROR "Failed to find MVG - " ${REASON_MSG} ${ARGN})
  ENDIF ()
  RETURN()
ENDMACRO(MVG_REPORT_NOT_FOUND)

# Get the (current, i.e. installed) directory containing this file.
GET_FILENAME_COMPONENT(MVG_CONFIG_INSTALL_DIR
  "${CMAKE_CURRENT_LIST_FILE}" PATH)

# Record the state of the CMake module path when this script was called so
# that we can ensure that we leave it in the same state on exit as it was
# on entry, but modify it locally.
SET(MVG_CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH})

# Reset CMake module path to the installation directory of this script,
# thus we will use the FindPackage() scripts shipped with MVG to find
# MVG' dependencies, even if the user has equivalently named FindPackage()
# scripts in their project.
SET(CMAKE_MODULE_PATH ${MVG_CONFIG_INSTALL_DIR})

# Build the absolute root install directory as a relative path (determined when
# MVG was configured & built) from the current install directory for this file.
# This allows for the install tree to be relocated, after MVG was
# built, outside of CMake.
GET_FILENAME_COMPONENT(CURRENT_ROOT_INSTALL_DIR
  ${MVG_CONFIG_INSTALL_DIR}/@INSTALL_ROOT_REL_CONFIG_INSTALL_DIR@ ABSOLUTE)
IF (NOT EXISTS ${CURRENT_ROOT_INSTALL_DIR})
  MVG_REPORT_NOT_FOUND(
    "MVG install root: ${CURRENT_ROOT_INSTALL_DIR}, "
    "determined from relative path from MVGConfg.cmake install location: "
    "${MVG_CONFIG_INSTALL_DIR}, does not exist. Either the install "
    "directory was deleted, or the install tree was only partially relocated "
    "outside of CMake after MVG was built.")
ENDIF (NOT EXISTS ${CURRENT_ROOT_INSTALL_DIR})

# Set the version.
SET(MVG_VERSION @MVG_VERSION@ )

# Set the include directories for MVG (itself).
SET(MVG_INCLUDE_DIR "${CURRENT_ROOT_INSTALL_DIR}/include")
IF (NOT EXISTS ${MVG_INCLUDE_DIR}/mvg/mvg.h)
  MVG_REPORT_NOT_FOUND(
    "MVG install root: ${CURRENT_ROOT_INSTALL_DIR}, "
    "determined from relative path from MVGConfg.cmake install location: "
    "${MVG_CONFIG_INSTALL_DIR}, does not contain MVG headers. "
    "Either the install directory was deleted, or the install tree was only "
    "partially relocated outside of CMake after MVG was built.")
ENDIF (NOT EXISTS ${MVG_INCLUDE_DIR}/mvg/mvg.h)

# Append the include directories for all (potentially optional) dependencies
# with which MVG was compiled, the libraries themselves come in via
# MVGTargets-<release/debug>.cmake as link libraries for MVG target.
SET(MVG_INCLUDE_DIRS ${MVG_INCLUDE_DIR})

# OpenCV.
# Flag set during configuration and build of MVG.
SET(MVG_OpenCV_VERSION @OpenCV_VERSION@)
# Append the locations of OpenCV when MVG was built to the search path hints.
LIST(APPEND OpenCV_INCLUDE_DIR_HINTS @OpenCV_INCLUDE_DIR@)
# Set the DIR variable to the one we were given when building in case it is not passed again
# when looking for MVG
IF (NOT OpenCV_DIR)
    SET (OpenCV_DIR @OpenCV_DIR@)
ENDIF (NOT OpenCV_DIR)
# Search quietly s/t we control the timing of the error message if not found.
IF ("@OpenCV_VERSION_MAJOR@" STREQUAL "3")
    FIND_PACKAGE(OpenCV ${MVG_OpenCV_VERSION} EXACT QUIET COMPONENTS core imgproc imgcodecs)
ELSE ("@OpenCV_VERSION_MAJOR@" STREQUAL "3")
    FIND_PACKAGE(OpenCV ${MVG_OpenCV_VERSION} EXACT QUIET COMPONENTS core imgproc highgui)
ENDIF ("@OpenCV_VERSION_MAJOR@" STREQUAL "3")
IF (OpenCV_FOUND)
  MESSAGE(STATUS "Found required MVG dependency: "
      "OpenCV version ${MVG_OpenCV_VERSION} in ${OpenCV_INCLUDE_DIRS}")
ELSE (OpenCV_FOUND)
  MVG_REPORT_NOT_FOUND("Missing required MVG "
      "dependency: OpenCV version ${MVG_OpenCV_VERSION}, please set "
      "OpenCV_INCLUDE_DIR.")
ENDIF (OpenCV_FOUND)
LIST(APPEND MVG_INCLUDE_DIRS ${OpenCV_INCLUDE_DIRS})

# Because OpenCV does not explicitly call them even thought it links with its imported target ...
FIND_PACKAGE(Qt5Core QUIET)
FIND_PACKAGE(Qt5Gui QUIET)
FIND_PACKAGE(Qt5Widgets QUIET)
FIND_PACKAGE(Qt5Concurrent QUIET)
FIND_PACKAGE(Qt5Test QUIET)

# Ceres.
# Flag set during configuration and build of MVG.
SET(MVG_CERES_VERSION @CERES_VERSION@)
# Append the locations of Ceres when MVG was built to the search path hints.
LIST(APPEND CERES_INCLUDE_DIR_HINTS @CERES_INCLUDE_DIR@)
# Set the DIR variable to the one we were given when building in case it is not passed again
# when looking for MVG
IF (NOT Ceres_DIR)
    SET (Ceres_DIR @Ceres_DIR@)
ENDIF (NOT Ceres_DIR)
# Search quietly s/t we control the timing of the error message if not found.
FIND_PACKAGE(Ceres ${MVG_CERES_VERSION} EXACT QUIET)
IF (Ceres_FOUND)
  MESSAGE(STATUS "Found required MVG dependency: "
      "Ceres version ${MVG_CERES_VERSION} in ${CERES_INCLUDE_DIRS}")
ELSE (Ceres_FOUND)
  MVG_REPORT_NOT_FOUND("Missing required MVG "
      "dependency: Ceres version ${MVG_CERES_VERSION}, please set "
      "CERES_INCLUDE_DIR.")
ENDIF (Ceres_FOUND)
LIST(APPEND MVG_INCLUDE_DIRS ${CERES_INCLUDE_DIRS})


# OpenThreads.
# Flag set during configuration and build of OpenThreads.
SET(MVG_OpenThreads_VERSION @OPENTHREADS_VERSION@)
# Append the locations of OpenThreads when MVG was built to the search path hints.
LIST(APPEND OpenThreads_INCLUDE_DIR_HINTS @OPENTHREADS_INCLUDE_DIR@)
# Set the DIR variable to the one we were given when building in case it is not passed again
# when looking for MVG
IF (NOT OPENTHREADS_DIR)
    SET (OPENTHREADS_DIR @OPENTHREADS_DIR@)
ENDIF (NOT OPENTHREADS_DIR)
IF (NOT OSG_DIR)
    SET (OSG_DIR @OSG_DIR@)
ENDIF (NOT OSG_DIR)
# Search quietly s/t we control the timing of the error message if not found.
FIND_PACKAGE(OpenThreads QUIET)
IF (OPENTHREADS_FOUND)
  MESSAGE(STATUS "Found required MVG dependency: "
      "OpenThreads version ${MVG_OpenThreads_VERSION} in ${OPENTHREADS_INCLUDE_DIR}")
ELSE ()
  MVG_REPORT_NOT_FOUND("Missing required MVG "
      "dependency: OpenThreads version ${MVG_OpenThreads_VERSION}, please set "
      "OPENTHREADS_DIR.")
ENDIF ()
LIST(APPEND MVG_INCLUDE_DIRS ${OPENTHREADS_INCLUDE_DIR})

# Import exported MVG targets.
IF (NOT MVG_TARGETS_INCLUDED)
  SET (MVG_TARGETS_INCLUDED 1)
  INCLUDE(${MVG_CONFIG_INSTALL_DIR}/MVGTargets.cmake)
ENDIF ()
# Set the expected XX_LIBRARIES variable for FindPackage().
SET(MVG_LIBRARIES mvg)

# Reset CMake module path to its state when this script was called.
SET(CMAKE_MODULE_PATH ${MVG_CMAKE_MODULE_PATH})

# As we use MVG_REPORT_NOT_FOUND() to abort, if we reach this point we have
# found MVG and all required dependencies.
MESSAGE(STATUS "Found MVG version: ${MVG_VERSION} "
    "installed in: ${CURRENT_ROOT_INSTALL_DIR}")

set(MVG_FOUND true)
