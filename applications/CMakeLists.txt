#
# MVG - A library to solve multiple view geometry problems.
# Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
#
# This file is part of MVG.
#
# MVG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# MVG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MVG.  If not, see <http://www.gnu.org/licenses/>.
#

# Now we are not building anymore, and symbols should be imported !
REMOVE_DEFINITIONS(-DMVG_BUILDING)

add_executable(rectify rectify.cpp)
target_link_libraries(rectify mvg)

install(TARGETS rectify RUNTIME DESTINATION bin)
