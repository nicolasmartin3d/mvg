/*
 * License Agreement for MVG
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "../src/mvg.h"

using namespace cv;
using namespace std;

int main(int   argc,
         char *argv[])
{
    string camPath1, camPath2;
    string imPath1, imPath2;
    string outPath1, outPath2;

    for (int i = 1; i < argc; ++i)
    {
        string arg(argv[i]);
        if ((arg == "-c1") && (i + 1 < argc))
        {
            camPath1 = argv[i + 1];
            ++i;
        }
        else if ((arg == "-c2") && (i + 1 < argc))
        {
            camPath2 = argv[i + 1];
            ++i;
        }
        else if ((arg == "-i1") && (i + 1 < argc))
        {
            imPath1 = argv[i + 1];
            ++i;
        }
        else if ((arg == "-i2") && (i + 1 < argc))
        {
            imPath2 = argv[i + 1];
            ++i;
        }
        else if ((arg == "-o1") && (i + 1 < argc))
        {
            outPath1 = argv[i + 1];
            ++i;
        }
        else if ((arg == "-o2") && (i + 1 < argc))
        {
            outPath2 = argv[i + 1];
            ++i;
        }
    }

    if (camPath1.empty() || camPath2.empty() ||
        imPath1.empty() || imPath2.empty() ||
        outPath1.empty() || outPath2.empty())
    {
        cerr << "USAGE:\n\trectify -i1 im1 -i2 im2 -o1 im1 -o2 im2 -c1 cam1 -c2 cam2" <<
            endl;

        return 1;
    }

    CameraGeometricParameters cam1, cam2;
    cam1.read(camPath1);
    cam2.read(camPath2);

    Mat im1, im2;
    im1 = imread(imPath1, -1);
    im2 = imread(imPath2, -1);

    Mat  H1, H2, R1, R2, K1, K2, Q;
    Size s1, s2, s;
#if 1
    findRectificationTransform(cam1, cam2,
                               false, // use K2 arbitrarily rather than K1
                               K1, K2,
                               R1, R2,
                               H1, H2,
                               s1, s2, s);
#else
    Rect r1, r2;
    stereoRectify(cam1.K, cam1.coeffs, cam2.K, cam2.coeffs,
                  cam2.imageSize,
                  cam2.R * cam1.R.t(), cam2.t - cam2.R * cam1.R.t() * cam1.t,
                  R1, R2, K1, K2, Q,
                  0, 1, Size(800, 600), &r1, &r2);
    cout << r1 << " " << r2 << endl;
    s = cam2.imageSize;
#endif

    cout << R1 << endl << K1 << endl << s1 << endl;
    cout << R2 << endl << K2 << endl << s2 << endl;
    cout << s << endl;

    Mat m1, m2;
    Mat out1;
    initUndistortRectifyMap(cam1.K(), cam1.distorsionCoefficients(),
                            R1, K1, s1, CV_16SC2, m1, m2);
    remap(im1, out1, m1, m2, INTER_LINEAR);

    Mat out2;
    initUndistortRectifyMap(cam2.K(), cam2.distorsionCoefficients(),
                            R2, K2, s2, CV_16SC2, m1, m2);
    remap(im2, out2, m1, m2, INTER_LINEAR);

    imwrite(outPath1, out1);
    imwrite(outPath2, out2);

    return 0;
}
